<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'Pabellón Digital',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-logo
    |
    */

    // RESPALDO/PRUEBA
    // DB_HOST3=10.4.237.23
    // DB_PORT3=3306
    // DB_DATABASE3=pabellon
    // DB_USERNAME3=sqldbx
    // DB_PASSWORD3=h5jdh1nf0rm4t1k4!

    // PRODUCCION
    // DB_HOST3=10.6.3.43
    // DB_PORT3=3306
    // DB_DATABASE3=pabellon
    // DB_USERNAME3=hospitalizacion
    // DB_PASSWORD3=OhXCYFrBIe46Qagq

    'logo' => 'Pabellón Digital',
    'logo_img' => 'vendor/adminlte/dist/img/logoSJD.png',
    'logo_img_class' => 'brand-image-xl',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Extra Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-info elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#66-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'http://10.6.3.43/pabellon/',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-menu
    |
    */

    'menu' => [
        [
            'text' => 'Buscar Paciente',
            'search' => true,
            'topnav' => true,
        ],
        [
            'text' => 'blog',
            'url'  => 'admin/blog',
            'can'  => 'manage-blog',
        ],
        [
            'text'    => 'Buscar Paciente',
            'icon'    => 'fas fa-search',
            'url'  => 'http://10.6.3.43/pabellon/buscar_ficha',
        ],
        [
            'text'    => 'Insumos',
            'can'  => 'insumos',
            'icon'    => 'fas fa-cubes',
            'submenu' => [
                [
                    'text' => 'Solicitud Insumo',
                    'can'  => 'solicitudInsumo',
                    'url'  => 'http://10.6.3.43/pabellon/solicitud',
                ],
                [
                    'text' => 'Mis Solicitudes',
                    'can'  => 'misSolicitudes',
                    'url'  => 'http://10.6.3.43/pabellon/misolicitud',
                ],
                [
                    'text' => 'Solicitud Recibida',
                    'can'  => 'solicitudRecibida',
                    'url'  => 'http://10.6.3.43/pabellon/recepcion',
                ],
                [
                    'text' => 'Confirmación Insumos Utilizados',
                    'can'  => 'confirmacionInsumosUtilizados',
                    'url'  => 'http://10.6.3.43/pabellon/devolucion',
                ],
                [
                    'text' => 'Lista Pedidos',
                    'can'  => 'listaPedidos',
                    'url'  => 'http://10.6.3.43/pabellon/historial',
                ],
                [
                    'text' => 'Stock',
                    'can'  => 'stock',
                    'url'  => 'http://10.6.3.43/pabellon/stock',
                ],
                [
                    'text' => 'Visar Insumos',
                    'can'  => 'visarInsumos',
                    'url'  => 'http://10.6.3.43/pabellon/visarinsumo',
                ],
                [
                    'text' => 'Visar Devolucion',
                    'can'  => 'visarDevolucion',
                    'url'  => 'http://10.6.3.43/pabellon/visardevolucion',
                ],
                [
                    'text' => 'Uso Insumo s/solicitud',
                    'can'  => 'usoInsumoSinSolicitud',
                    'url'  => 'http://10.6.3.43/pabellon/registrosalida',
                ],
                [
                    'text' => 'Salidas',
                    'can'  => 'salidas',
                    'url'  => 'http://10.6.3.43/pabellon/salida',
                ],
                [
                    'text' => 'Solicitud Externa',
                    'can'  => 'solicitudExterna',
                    'url'  => 'http://10.6.3.43/pabellon/solicitudbodega',
                ],
                [
                    'text' => 'Carga Hermes',
                    'can'  => 'cargaHermes',
                    'url'  => 'http://10.6.3.43/pabellon/cargamasiva',
                ],
                [
                    'text' => 'Vaciado Stock Excel',
                    'can'  => 'vaciadoStockExcel',
                    'url'  => 'http://10.6.3.43/pabellon/vaciado_stock',
                ],
                [
                    'text' => 'Solicitud Post Operacion	',
                    'can'  => 'solicitudPostOperacion',
                    'url'  => 'http://10.6.3.43/pabellon/solicitud_post	',
                ],
            ],
        ],
        [
            'text'    => 'Solicitud de Pabellón',
            'can'  => 'solicitudDePabellon',
            'icon'    => 'fas fa-clipboard',
            'submenu' => [
                [
                    'text' => 'Ingreso Solicitud',
                    'can'  => 'ingresoSolicitud',
                    'url'  => 'http://10.6.3.43/pabellon/solicitudpab',
                ],
                [
                    'text' => 'Mis Solicitudes Quirúrgicas',
                    'can'  => 'misSolicitudesQuirurgicas',
                    'url'  => 'http://10.6.3.43/pabellon/misolicitudpabellon',
                ],
                [
                    'text' => 'Asignación',
                    'can'  => 'asignacion',
                    'url'  => 'http://10.6.3.43/pabellon/confirmacion',
                ],
                [
                    'text' => 'Agenda Pabellones',
                    'can'  => 'agendaPabellones',
                    'url'  => 'http://10.6.3.43/pabellon/agenda',
                ],
                [
                    'text' => 'Ingreso Solicitud Antigua (Temporal)',
                    'can'  => 'ingresoSolicitudAntigua',
                    'url'  => 'http://10.6.3.43/pabellon/solicitudpabold',
                ],
                [
                    'text' => 'Estado Solicitud (Equipo)',
                    'can'  => 'estadoSolicitud',
                    'url'  => 'http://10.6.3.43/pabellon/solicitudpabpro',
                ],
                [
                    'text' => 'Suspender IQ',
                    'can'  => 'suspenderIQ',
                    'url'  => 'http://10.6.3.43/pabellon/suspender',
                ],
            ],
        ],
        [
            'text'    => 'Lista Espera',
            'can'  => 'listaEspera',
            'icon'    => 'fas fa-clock',
            'submenu' => [
                [
                    'text' => 'Gestion Lista Espera',
                    'can'  => 'gestionListaEspera',
                    'url'  => 'http://10.6.3.43/pabellon/listaespera',
                ],
                [
                    'text' => 'RNLE',
                    'can'  => 'rnle',
                    'url'  => 'http://10.6.3.43/pabellon/listaesperahospital',
                ],
                [
                    'text' => 'Baja Lista Espera',
                    'can'  => 'bajaListaEspera',
                    'url'  => 'http://10.6.3.43/pabellon/listaesperasalida',
                ],
                [
                    'text' => 'Desvinculación',
                    'can'  => 'desvinculacion',
                    'url'  => 'http://10.6.3.43/pabellon/desvincular_le',
                ],
                [
                    'text' => 'Intervenciones editadas',
                    'can'  => 'intervencionesEditadas',
                    'url'  => 'http://10.6.3.43/pabellon/listaespera_editados',
                ],
            ],
        ],
        [
            'text'    => 'Enfermeria',
            'can'  => 'enfermeria',
            'icon'    => 'fas fa-user-nurse',
            'submenu' => [
                [
                    'text' => 'Proceso Enfermeria',
                    'can'  => 'procesoEnfermeria',
                    'url'  => 'http://10.6.3.43/pabellon/pausa',
                ],
                [
                    'text' => 'Visar Pausa',
                    'can'  => 'visarPausa',
                    'url'  => 'http://10.6.3.43/pabellon/pausa_visar',
                ],
                [
                    'text' => 'Historial Enfermeria',
                    'can'  => 'historialEnfermeria',
                    'url'  => 'http://10.6.3.43/pabellon/historiapausa',
                ],
                [
                    'text' => 'Recuperación',
                    'can'  => 'recuperacion',
                    'url'  => 'http://10.6.3.43/pabellon/recuperacion',
                ],
                [
                    'text' => 'Hoja de Traslado',
                    'can'  => 'hojaDeTraslado',
                    'url'  => 'http://10.6.3.43/pabellon/solicitud_traslado',
                ],
                [
                    'text' => 'Traslados Realizados',
                    'can'  => 'trasladosRealizados',
                    'url'  => 'http://10.6.3.43/pabellon/listatraslado',
                ],
            ],
        ],
        [
            'text'    => 'Pre-Operatorio s/Solicitud',
            'can'  => 'preOperatorioSinSolicitud',
            'icon'    => 'fas fa-clipboard',
            'submenu' => [
                [
                    'text' => 'Ingresar Pre-Operatorio',
                    'can'  => 'ingresarPreOperatorio',
                    'url'  => 'http://10.6.3.43/pabellon/solicitudpre',
                ],
                [
                    'text' => 'Gestión Pre-Operatorio',
                    'can'  => 'gestionPreOperatorio',
                    'url'  => 'http://10.6.3.43/pabellon/listapreoperatorio',
                ],
            ],
        ],
        [
            'text'    => 'Procolo',
            'can'  => 'protocolo',
            'icon'    => 'fas fa-user-md',
            'submenu' => [
                [
                    'text' => 'Ingresar Protocolo',
                    'can'  => 'ingresarProtocolo',
                    'url'  => 'http://10.6.3.43/pabellon/protocolo',
                ],
                [
                    'text' => 'Lista Protocolo',
                    'can'  => 'listaProtocolo',
                    'url'  => 'http://10.6.3.43/pabellon/miprotocolo',
                ],
                [
                    'text' => 'Historial',
                    'can'  => 'historial',
                    'url'  => 'http://10.6.3.43/pabellon/historialprotocolo',
                ],
                [
                    'text' => 'Historial Ver',
                    'can'  => 'historialVer',
                    'url'  => 'http://10.6.3.43/pabellon/histosolover',
                ],
                [
                    'text' => 'Mis Protocolos',
                    'can'  => 'misProtocolos',
                    'url'  => 'http://10.6.3.43/pabellon/misingresosprotocolo',
                ],
                [
                    'text' => 'Estadisticas',
                    'can'  => 'estadisticas',
                    'url'  => 'http://10.6.3.43/pabellon/estadistico',
                ],
                [
                    'text' => 'Mis Participaciones',
                    'can'  => 'misParticipaciones',
                    'url'  => 'http://10.6.3.43/pabellon/miparticipacion',
                ],
                [
                    'text' => 'Protocolo Procuramiento',
                    'can'  => 'protocoloProcuramiento',
                    'url'  => 'http://10.6.3.43/pabellon/misingresosprotocoloprocuramiento',
                ],
                [
                    'text' => 'Lista de Analisis Reoperaciones',
                    'can'  => 'listaDeAnalisisReoperaciones',
                    'url'  => 'http://10.6.3.43/pabellon/analisis_reoperaciones',
                ],
            ],
        ],
        [
            'text'    => 'Pabellon',
            'can'  => 'pabellon',
            'icon'    => 'fas fa-hospital',
            'submenu' => [
                [
                    'text' => '5° Piso',
                    'can'  => 'quintoPiso',
                    'url'  => 'http://10.6.3.43/pabellon/pabellon',
                ],
                [
                    'text' => 'Estadística Ingreso',
                    'can'  => 'estadisticasIngreso',
                    'url'  => 'http://10.6.3.43/pabellon/estadistica',
                ],
            ],
        ],
        [
            'text'    => 'Informes',
            'can'  => 'informes',
            'icon'    => 'fas fa-file',
            'submenu' => [
                [
                    'text' => 'Vaciado Intervenciones',
                    'can'  => 'vaciadoIntervenciones',
                    'url'  => 'http://10.6.3.43/pabellon/vaciado',
                ],
                [
                    'text' => 'Revisar Diagnostico',
                    'can'  => 'revisarDiagnostico',
                    'url'  => 'http://10.6.3.43/pabellon/rev_diag',
                ],
                [
                    'text' => 'Producción Pabellon',
                    'can'  => 'produccionPabellon',
                    'url'  => 'http://10.6.3.43/pabellon/produccionpab',
                ],
                [
                    'text' => 'UGCQ Minsal',
                    'can'  => 'ugsqMinsal',
                    'url'  => 'http://10.6.3.43/pabellon/ugcq',
                ],
                [
                    'text' => 'Informe Prestaciones',
                    'can'  => 'informePrestaciones',
                    'url'  => 'http://10.6.3.43/pabellon/informe_protocolo',
                ],
                [
                    'text' => 'Informe Cardio Cirugia',
                    'can'  => 'informeCardioCirugia',
                    'url'  => 'http://10.6.3.43/pabellon/informe_cardio',
                ],
                [
                    'text' => 'Solicitud Intervención sin protocolo',
                    'can'  => 'solicitudIntervencionSinProtocolo',
                    'url'  => 'http://10.6.3.43/pabellon/solicitud_protocolo',
                ],
                [
                    'text' => 'Estado Lista Espera Historica',
                    'can'  => 'estadoListaEsperaHistorica',
                    'url'  => 'http://10.6.3.43/pabellon/metricalistaespera',
                ],
                [
                    'text' => 'Estado Insumos',
                    'can'  => 'estadoInsumos',
                    'url'  => 'http://10.6.3.43/pabellon/estado_insumo',
                ],
                [
                    'text' => 'Uso de Pabellon',
                    'can'  => 'usoDePabellon',
                    'url'  => 'http://10.6.3.43/pabellon/uso_pabellon',
                ],
                [
                    'text' => 'Tiempo Operatorio',
                    'can'  => 'tiempoOperatorio',
                    'url'  => 'http://10.6.3.43/pabellon/tiempo_operatorio',
                ],
                [
                    'text' => 'Informe Prestaciones SIGGES',
                    'can'  => 'informePrestacionesSIGGES',
                    'url'  => 'http://10.6.3.43/pabellon/informe_protocolo_sigges',
                ],
                [
                    'text' => 'Vaciado Completo',
                    'can'  => 'vaciadoCompleto',
                    'url'  => 'http://10.6.3.43/pabellon/vaciado_completo',
                ],
                [
                    'text' => 'Medida Ministerial',
                    'can'  => 'medidaMinisterial',
                    'url'  => 'http://10.6.3.43/pabellon/vaciado_ugcq',
                ],
                [
                    'text' => 'CMDB UGCQ 2da Etapa',
                    'can'  => 'cmdbUGCQsegundaEtapa',
                    'url'  => 'http://10.6.3.43/pabellon/cmdb_ugcq',
                ],
                [
                    'text' => 'Estudio de Eficiencia',
                    'can'  => 'estudioDeEficiencia',
                    'url'  => 'http://10.6.3.43/pabellon/estudio_eficiencia',
                ],
                [
                    'text' => 'Indicadores Calidad',
                    'can'  => 'indicadoresCalidad',
                    'url'  => 'http://10.6.3.43/pabellon/indicadores',
                ],
                [
                    'text' => 'Producción Equipo Quirúrgico',
                    'can'  => 'produccionEquipoQuirurgico',
                    'url'  => 'http://10.6.3.43/pabellon/indicadores_unidad',
                ],
                [
                    'text' => 'Producción Cirujanos',
                    'can'  => 'produccionCirujanos',
                    'url'  => 'http://10.6.3.43/pabellon/indicador_produccion_cirujanos',
                ],
                [
                    'text' => 'Pausa Seguridad - Protocolo',
                    'can'  => 'pausaSeguridadProtocolo',
                    'url'  => 'http://10.6.3.43/pabellon/indicador_pausa_seguridad',
                ],
                [
                    'text' => 'Gastos Insumos',
                    'can'  => 'gastosInsumos',
                    'url'  => 'http://10.6.3.43/pabellon/indicador_gasto',
                ],
                [
                    'text' => 'Cantidad Pacientes Intervenidos',
                    'can'  => 'cantidadPacientesIntervenidos',
                    'url'  => 'http://10.6.3.43/pabellon/indicador_pacientes_intervenidos',
                ],
                [
                    'text' => 'Informe Oncologia',
                    'can'  => 'informeOncologia',
                    'url'  => 'http://10.6.3.43/pabellon/informe_oncologia',
                ],
                [
                    'text' => 'Pacientes ingresados con anticipacion',
                    'can'  => 'pacientesIngresadosConAnticipacion',
                    'url'  => 'http://10.6.3.43/pabellon/ingresos_anticipacion',
                ],
            ],
        ],
        [
            'text'    => 'Mantenedor',
            'can'  => 'mantenedor',
            'icon'    => 'fas fa-wrench',
            'submenu' => [
                [
                    'text' => 'Equipo Clinico',
                    'can'  => 'equipoClinico',
                    'url'  => 'http://10.6.3.43/pabellon/profesional',
                ],
                [
                    'text' => 'Equipo Medico',
                    'can'  => 'equipoMedico',
                    'url'  => 'http://10.6.3.43/pabellon/equipomedico',
                ],
                [
                    'text' => 'Pack Insumos',
                    'can'  => 'packInsumos',
                    'url'  => 'http://10.6.3.43/pabellon/packinsumo',
                ],
                [
                    'text' => 'Jerarquía Equipo Médico',
                    'can'  => 'jerarquiaEquipoMedico',
                    'url'  => 'http://10.6.3.43/pabellon/jerarquia_equipo',
                ],
                [
                    'text' => 'Causa Desvincular',
                    'can'  => 'causaDesvincular',
                    'url'  => 'http://10.6.3.43/pabellon/causa_desvincular',
                ],
                [
                    'text' => 'Pabellón',
                    'can'  => 'pabellonMantenedor',
                    'url'  => 'http://10.6.3.43/pabellon/lista_pabellon',
                ],
                [
                    'text' => 'Sala Recuperacion',
                    'can'  => 'salaRecuperacion',
                    'url'  => 'http://10.6.3.43/pabellon/sala_recuperacion',
                ],
                [
                    'text' => 'Cama Recuperacion',
                    'can'  => 'camaRecuperacion',
                    'url'  => 'http://10.6.3.43/pabellon/cama_recuperacion',
                ],
                [
                    'text' => 'Estado Cama/Sala',
                    'can'  => 'estadoCamaSala',
                    'url'  => 'http://10.6.3.43/pabellon/estado_sala_cama',
                ],
            ],
        ],
        [
            'text'    => 'Administración',
            'can'  => 'administracion',
            'icon'    => 'fas fa-cog',
            'submenu' => [
                [
                    'text' => 'Usuario',
                    'icon'    => 'fas fa-user-cog',
                    'url'  => 'user',
                ],
                [
                    'text' => 'Perfil',
                    'url'  => 'http://10.6.3.43/pabellon/perfil',
                ],
                [
                    'text' => 'Opción',
                    'url'  => 'http://10.6.3.43/pabellon/opcion',
                ],
                [
                    'text' => 'Bodega',
                    'url'  => 'http://10.6.3.43/pabellon/bodega',
                ],
                [
                    'text' => 'Insumo',
                    'url'  => 'http://10.6.3.43/pabellon/insumo',
                ],
                [
                    'text' => 'Especialidad Medica	',
                    'url'  => 'http://10.6.3.43/pabellon/especialidad',
                ],
                [
                    'text' => 'Reparar Registro	',
                    'url'  => 'http://10.6.3.43/pabellon/reparar',
                ],
                [
                    'text' => 'Paciente',
                    'icon'    => 'fas fa-user-cog',
                    'url'  => 'paciente',
                ],
                [
                    'text' => 'Sol. de Pab. sin paciente',
                    'url'  => '/solicitudPabellonSinPaciente',
                ],
            ],
        ],
        [
            'text'    => 'Modulos de Prueba',
            'can'  => 'administracion',
            'icon'    => 'fas fa-code',
            'submenu' => [
                [
                    'text' => 'Ingreso Solicitud',
                    'icon'    => 'fas fa-clipboard',
                    'url'  => '/solicitudPabellon/create',
                ],
                [
                    'text' => 'Proceso Enfermería',
                    'icon'    => 'fas fa-clipboard',
                    'url'  => '/indexPausas',
                ],
                [
                    'text' => 'Vaciado Intervenciones',
                    'icon'    => 'fas fa-file',
                    'url'  => '/vaciadoIntervenciones',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-plugins
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
