@extends('adminlte::page')

@section('title', 'Lista de Usuarios')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Lista de Usuarios</h3>
		</div>
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                                <div class="col-sm-2">
                                    <a href="user/create" class="btn btn-success" type="button" title="Agregar Usuario"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th style="width: 5% !important">Email</th>
                                    <th>Activo</th>
                                    <th>Perfil</th>
                                    <th>Bodega</th>
                                    <th>Especialidad</th>
                                    <th>Equipo Médico</th>
                                    <th>Pabellón</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        @php
                                            $urlDeleteUsuario = url('deleteUsuario/'.$user->id);
                                            $urlResetUsuario = url('resetUsuario/'.$user->id);
                                        @endphp 
                                        <tr style="font-size:12px">
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->nombre}}</td> 
                                            <td>{{$user->gl_rut}}</td>
                                            <td><span title="{{$user->gl_mail}}">{{mb_substr($user->gl_mail,0,5)}}{{isset($user->gl_mail) && strlen($user->gl_mail) > 2 ? '...' : ''}}</span></td>
                                            <td>{{$user->bo_suspendido == 1 ? 'No' : 'Si'}}</td>
                                            <td>{{$user->Perfiles->pluck('gl_nombre')->implode(', ')}}</td>
                                            <td>{{$user->Bodegas->pluck('gl_nombre')->implode(', ')}}</td>
                                            <td>{{$user->Especialidades->pluck('gl_nombre')->implode(', ')}}</td>
                                            <td>{{$user->EquiposMedicos->pluck('gl_descripcion')->implode(', ')}}</td>
                                            <td>{{$user->Pabellones->pluck('gl_seudonimo')->implode(', ')}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="user/{{$user->id}}/edit" title="Editar Usuario" class="btn btn-warning btn-xs"><i class="fa fa-edit" style="color:white"></i></a>
                                                    <a onclick="reset('{{$urlResetUsuario}}');" title="Reiniciar Contraseña" type="button" class="btn btn-primary btn-xs"><i class="fa fa-lock-open" style="color:white"></i></a>
                                                    <a onclick="eliminar('{{$urlDeleteUsuario}}');" title="Eliminar Usuario" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $users->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        function reset(url)
        {
            var opcion = confirm("Estas seguro de reiniciar la contraseña de este usuario?");
            if(opcion == true) {
                location.href = url;
            }else{
                return false;
            }
        }

        function eliminar(url)
        {
            var opcion = confirm("Estas seguro de eliminar este usuario?");
            if(opcion == true) {
                location.href = url;
            }else{
                return false;
            }
        }

        $(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
            $(".alert-success").slideUp(1000);
        });

        $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
            $(".alert-danger").slideUp(1000);
        });
    </script>
@stop