@extends('adminlte::page')

@section('title', 'Registro Usuario')

@section('content')
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">{{ isset($user) ? 'Editar cuenta' : 'Crear cuenta'}}</h3>
		</div>
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UserController@store')}}">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="gl_rut">Rut: </label>
							<input class="form-control" id="gl_rut" name="gl_rut" value="{{isset($user) ? $user->gl_rut : ''}}" {{isset($user) ? 'readonly' : ''}} onblur="getPerson($(this).val(), 1);" required>
						</div>
						<div class="col-sm-4">
							<label for="gl_nombre">Nombre: </label>
							<input class="form-control" id="gl_nombre" name="gl_nombre" value="{{isset($user) ? $user->gl_nombre : ''}}" required>
						</div>
						<div class="col-sm-4">
							<label for="gl_mail">Email</label>
							<input type="email" class="form-control" id="gl_mail" name="gl_mail" value="{{isset($user) ? $user->gl_mail : ''}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="perfil">Perfil:</label>
							<select class="select2 select2-hidden-accessible" id="perfiles" name="perfiles[]" multiple="" data-placeholder="Seleccione perfil(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($perfiles as $perfil)
									<option value={{$perfil->id}} >{{$perfil->gl_nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="bodega">Bodega:</label>
							<select class="select2 select2-hidden-accessible" id="bodegas" name="bodegas[]" multiple="" data-placeholder="Seleccione bodega(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($bodegas as $bodega)
									<option value={{$bodega->id}} >{{$bodega->gl_nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="especialidad">Especialidad:</label>
							<select class="select2 select2-hidden-accessible" id="especialidades" name="especialidades[]" multiple="" data-placeholder="Seleccione especialidad(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($especialidades as $especialidad)
									<option value={{$especialidad->id}} >{{$especialidad->gl_nombre}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="equipoMedico">Equipo Medico:</label>
							<select class="select2 select2-hidden-accessible" id="equiposMedicos" name="equiposMedicos[]" multiple="" data-placeholder="Seleccione Equipo(s) Medico(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($equiposMedicos as $equipoMedico)
									<option value={{$equipoMedico->id}} >{{$equipoMedico->gl_descripcion}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="pabellon">Pabellon:</label>
							<select class="select2 select2-hidden-accessible" id="pabellones" name="pabellones[]" multiple="" data-placeholder="Seleccione pabellon(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($pabellones as $pabellon)
									<option value={{$pabellon->id}} >{{$pabellon->gl_nombre}} ({{$pabellon->gl_seudonimo}})</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" id="equipoClinico" name="equipoClinico" {{isset($medico) ? 'checked' : ''}}>
								<label class="custom-control-label" for="equipoClinico">Añadir Equipo Clinico</label>
							</div>						
						</div>
						<div class="col-sm-4">
							<label for="labores">Labor</label>
							<select class="select2 select2-hidden-accessible" id="labores" name="labores[]" multiple="" data-placeholder="Seleccione labor(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" {{!isset($medico) ? 'disabled' : ''}}>
								@foreach ($labores as $labor)
									<option value={{$labor->id}} >{{$labor->gl_nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="tipoProfesional">Tipo Profesional</label>
							<select class="select2 select2-hidden-accessible" id="tipoProfesional" name="tipoProfesional" data-placeholder="Seleccione tipo profesional" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" {{!isset($medico) ? 'disabled' : ''}}>
								@foreach ($tiposProfesionales as $tipoProfesional)
									<option value={{$tipoProfesional->gl_codigo}} {{isset($medico->pro_tipo) && $medico->pro_tipo == $tipoProfesional->gl_codigo ? "selected" : ""}}>{{$tipoProfesional->gl_nombre}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="tx_nombre">Nombre: </label>
							<input class="form-control" id="tx_nombre" name="tx_nombre" value="{{isset($medico) ? $medico->pro_nombres : ''}}" {{!isset($medico) ? 'disabled' : ''}}>
						</div>
						<div class="col-sm-4">
							<label for="tx_paterno">A. Paterno: </label>
							<input class="form-control" id="tx_paterno" name="tx_paterno" value="{{isset($medico) ? $medico->pro_apepat : ''}}" {{!isset($medico) ? 'disabled' : ''}}>
						</div>
						<div class="col-sm-4">
							<label for="tx_materno">A. Materno</label>
							<input class="form-control" id="tx_materno" name="tx_materno" value="{{isset($medico) ? $medico->pro_apemat : ''}}" {{!isset($medico) ? 'disabled' : ''}}>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success">Guardar</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
	<script>
		function getPerson(rut, id_tipo_identificacion_paciente){
			@if(!isset($user))
				$.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut+"&id_tipo_identificacion="+id_tipo_identificacion_paciente,
				function(data){
					if(data.user){
						var opcion = confirm("Este Usuario ya existe desea editarlo?");
						if(opcion == true){
							location.href = data.id+"/edit";
						}else{
							$("#gl_rut").val("");
						}
					}
					else{
						if(data.encontrado == true){
							$("#gl_nombre").val(data.gl_nombre);
							$("#tx_nombre").val(data.tx_nombre);
							$("#tx_paterno").val(data.tx_apellido_paterno);
							$("#tx_materno").val(data.tx_apellido_materno);
						}else{
							alert("Rut no encontrado");
							$("#gl_rut").val("");
							$("#gl_nombre").val("");
							$("#gl_nombre").css('display','').attr('readonly', false);
						}
					}
				})
			@else
				$.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut+"&id_tipo_identificacion="+id_tipo_identificacion_paciente,
					function(data){
						if(data.encontrado == true){
							$("#gl_nombre").val(data.gl_nombre);
							$("#tx_nombre").val(data.tx_nombre);
							$("#tx_paterno").val(data.tx_apellido_paterno);
							$("#tx_materno").val(data.tx_apellido_materno);
						}
					})
			@endif
		};

		$('#equipoClinico').change(function() {
			if(this.checked){
				$("#labores").css('display','').attr('disabled', false);   
				$("#tipoProfesional").css('display','').attr('disabled', false);   
				$("#tx_nombre").css('display','').attr('disabled', false); 
				$("#tx_nombre").css('display','').attr('required', true);     
				$("#tx_paterno").css('display','').attr('disabled', false);   
				$("#tx_paterno").css('display','').attr('required', true);   
				$("#tx_materno").css('display','').attr('disabled', false);   
				$("#tx_materno").css('display','').attr('required', true);   
			}else{
				$("#labores").css('display','').attr('disabled', true);
				$("#tipoProfesional").css('display','').attr('disabled', true);   
				$("#tx_nombre").css('display','').attr('disabled', true);   
				$("#tx_nombre").css('display','').attr('required', false);     
				$("#tx_paterno").css('display','').attr('disabled', true);   
				$("#tx_materno").css('display','').attr('disabled', true);   
			}
		});
		
		@if (isset($user))
			var user = @json($user);
			var perfiles = user.perfiles.map(perfiles => perfiles['id']);
			var bodegas = user.bodegas.map(bodegas => bodegas['id']);
			var especialidades = user.especialidades.map(especialidades => especialidades['id']);
			var equiposMedicos = user.equipos_medicos.map(equiposMedicos => equiposMedicos['id']);
			var pabellones = user.pabellones.map(pabellones => pabellones['id']);

			$("#perfiles").val(perfiles).trigger('change');
			$("#bodegas").val(bodegas).trigger('change');
			$("#especialidades").val(especialidades).trigger('change');
			$("#equiposMedicos").val(equiposMedicos).trigger('change');
			$("#pabellones").val(pabellones).trigger('change');
			
			getPerson(user.gl_rut, 1);
		@endif

		@if (isset($medico))
			var user = @json($medico);
			var labores = user.labores.map(labores => labores['id']);

			$("#labores").val(labores).trigger('change');
		@endif

		$('.select2').select2();
	</script>
@stop