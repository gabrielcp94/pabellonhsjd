@extends('adminlte::page')

@section('title', 'Ingresar Protocolo')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i> Ingresar Protocolo</h1>
@stop

@section('content')
    <div class="card">
        <!-- form start -->
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ProtocoloController@store')}}">
        {{ csrf_field() }}
        <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->protocolo->id ?? ''}}"/>
        <input type="hidden" id="id_solicitud_pabellon" name="id_solicitud_pabellon" value="{{isset($solicitudPabellon) ? $solicitudPabellon->id : ''}}"/>
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
        </div>
        <div class="card-body">
            @include('paciente.datos')
        </div>
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Solicitud</strong></h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="id_equipo">Equipo Quirúrgico<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_equipo" name="id_equipo" required>
                            <option value="">Seleccione Equipo Quirúrgico</option>
                            @foreach ($equiposMedicos as $equipoMedico)
                                <option value={{$equipoMedico->id}} {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->id_equipo == $equipoMedico->id ? "selected" : ""}}>{{$equipoMedico->gl_descripcion}}</option>								
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_auge">Ges<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_auge_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_auge_1" name="bo_auge" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_auge == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_auge_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_auge_2" name="bo_auge" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_auge == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_cancer">Cáncer<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_cancer_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cancer_1" name="bo_cancer" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_cancer == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_cancer_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cancer_2" name="bo_cancer" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_cancer == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_procuramiento">Procuramiento<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_procuramiento_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_procuramiento_1" name="bo_procuramiento" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_procuramiento == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_procuramiento_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_procuramiento_2" name="bo_procuramiento" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_procuramiento == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="bo_re_intervencion">Re-intervención no programada<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_re_intervencion_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_re_intervencion_1" name="bo_re_intervencion" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_re_intervencion == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_re_intervencion_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_re_intervencion_2" name="bo_re_intervencion" value="0" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->bo_re_intervencion == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="id_pabellon">Pabellón<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_pabellon" name="id_pabellon" onchange="llenarNrPabellon((this.value))" required>
                            <option value="">Seleccione Pabellón</option>
                            @foreach ($pabellones as $pabellon)
                                <option value={{$pabellon->id}} {{isset($solicitudPabellon) && $solicitudPabellon->id_pabellon == $pabellon->id ? "selected" : ""}}>{{$pabellon->nombre}}</option>								
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label for="id_nr_pabellon">N° Pabellón<span style="color:#FF0000";>*</span></label>
                        <select class="form-control select_nr_pabellones" id="id_nr_pabellon" name="id_nr_pabellon" disabled>
                        </select>
                    </div>
                    <div class="col-sm-2 offset-sm-1">
                        <label for="bo_lista_espera">Lista Espera<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_lista_espera_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_lista_espera_1" name="bo_lista_espera" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_lista_espera == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_lista_espera_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_lista_espera_2" name="bo_lista_espera" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_lista_espera == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="fc_intervencion">Fecha Intervención<span style="color:#FF0000";>*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="date" class="form-control" id="fc_intervencion" name="fc_intervencion" value="{{isset($solicitudPabellon) ? $solicitudPabellon->fc_intervencion : ''}}" disabled>
                        </div>	
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Uso de Pabellón</strong></h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2 offset-sm-1">
                        <br>
                        <label>Entrada/Inicio</label>
                        <br>
                        <label>Salida/Término</label>
                    </div>
                    <div class="col-sm-3">
                        <label>Pabellón<span style="color:#FF0000";>*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_entrada_pabellon" name="fc_entrada_pabellon" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_entrada_pabellon)) : ""}}">
                        </div>	
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_salida_pabellon" name="fc_salida_pabellon" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_salida_pabellon)) : ""}}">
                        </div>	
                    </div>
                    <div class="col-sm-3">
                        <label>Anestesia<span style="color:#FF0000";>*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_inicio_anestesia" name="fc_inicio_anestesia" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_inicio_anestesia)) : ""}}">
                        </div>	
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_fin_anestesia" name="fc_fin_anestesia" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_fin_anestesia)) : ""}}">
                        </div>	
                    </div>
                    <div class="col-sm-3">
                        <label>Intervención<span style="color:#FF0000";>*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_inicio_intervencion" name="fc_inicio_intervencion" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_inicio_intervencion)) : ""}}">
                        </div>	
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="datetime-local" class="form-control" id="fc_fin_intervencion" name="fc_fin_intervencion" value="{{isset($solicitudPabellon->protocolo) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->protocolo->fc_fin_intervencion)) : ""}}">
                        </div>	
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Intervención</strong></h3>
        </div>
        <div class="card-body">
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="intervencion">Intervencion(es)<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_intervenciones" id="intervencion" name="intervencion[]" multiple required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="id_diagnostico_pre">Diag. Pre-Operatorio (CIE10)<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_diagnosticos" id="id_diagnostico_pre" name="id_diagnostico_pre" required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="gl_diagnostico_pre">Diag. Pre-Operatorio (Texto)</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="gl_diagnostico_pre" name="gl_diagnostico_pre" value="{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_diagnostico_pre : ''}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="id_diagnostico_pos">Diag. Post-Operatorio (CIE10)<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_diagnosticos" id="id_diagnostico_pos" name="id_diagnostico_pos" required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="gl_diagnostico_pos">Diag. Post-Operatorio (Texto)</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="gl_diagnostico_pos" name="gl_diagnostico_pos" value="{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_diagnostico_pos : ''}}" required>
                </div>
            </div>
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Personal de Apoyo</strong></h3>
        </div>
        <div class="card-body">
            <div class="row form-group equipo" id="equipo">
                <div class="col-sm-1">
                    <label for="personal">Personal<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-11">
                    <select class="form-control select_equipos" id="equipos" name="equipos[]" multiple required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label for="gl_cirugia_practicada">Cirugía Practicada<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="gl_cirugia_practicada" name="gl_cirugia_practicada" required>{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_cirugia_practicada : ''}}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="gl_riesgo_operatorio">Riesgo Operatorio<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_1">Grave</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_1" name="gl_riesgo_operatorio" value="Grave" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_riesgo_operatorio == "Grave" ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_2">Mediano</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_2" name="gl_riesgo_operatorio" value="Mediano" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_riesgo_operatorio == "Mediano" ? "checked" : ""}}>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_3">Leve</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_3" name="gl_riesgo_operatorio" value="Leve" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_riesgo_operatorio == "Leve" ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 offset-sm-2">
                    <label for="gl_tipo_anestesia">Tipo Anestesia<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_1">General</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_1" name="gl_tipo_anestesia" value="General" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_tipo_anestesia == "General" ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_2">Local</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_2" name="gl_tipo_anestesia" value="Local" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_tipo_anestesia == "Local" ? "checked" : ""}}>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_3">Regional</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_3" name="gl_tipo_anestesia" value="Regional" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_tipo_anestesia == "Regional" ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label for="gl_hallazgo">Hallazgos IntraOp.<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="gl_hallazgo" name="gl_hallazgo" required>{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_hallazgo : ''}}</textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label for="gl_descripcion">Descripción Op.<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="gl_descripcion" name="gl_descripcion" required>{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_descripcion : ''}}</textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label for="gl_incidencia_qui">Incidencias Quir.<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-4">
                    <textarea type="text" class="form-control" id="gl_incidencia_qui" name="gl_incidencia_qui" required>{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_incidencia_qui : ''}}</textarea>
                </div>
                <div class="col-sm-2">
                    <label for="gl_incidencia_ane">Incidencias Anest.<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-4">
                    <textarea type="text" class="form-control" id="gl_incidencia_ane" name="gl_incidencia_ane" required>{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->gl_incidencia_ane : ''}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="bo_drenaje">Drenajes<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_drenaje_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_drenaje_1" name="bo_drenaje" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_drenaje == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_drenaje_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_drenaje_2" name="bo_drenaje" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_drenaje == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_biopsia">Biopsia<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_biopsia_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_biopsia_1" name="bo_biopsia" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_biopsia == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_biopsia_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_biopsia_2" name="bo_biopsia" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_biopsia == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_cultivo">Cultivos<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_cultivo_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cultivo_1" name="bo_cultivo" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_cultivo == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_cultivo_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cultivo_2" name="bo_cultivo" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_cultivo == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="gl_tipo_herida">Tipo Herida<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="gl_tipo_herida_1">Limpia</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_herida_1" name="gl_tipo_herida" value="Limpia" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_tipo_herida == "Limpia" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tipo_herida_2">Limpia-Contaminada</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_herida_2" name="gl_tipo_herida" value="Limpia-Contaminada" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tipo_herida == "Limpia-Contaminada" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tipo_herida_3">Contaminada</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_herida_3" name="gl_tipo_herida" value="Contaminada" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tipo_herida == "Contaminada" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tipo_herida_4">Sucia</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_herida_4" name="gl_tipo_herida" value="Sucia" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tipo_herida == "Sucia" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 offset-sm-1">
                        <label for="nr_perdida_sanguinea">Perdida Sanguínea (cc)<span style="color:#FF0000";>*</span></label>
                        <input type="number" class="form-control" id="nr_perdida_sanguinea" name="nr_perdida_sanguinea" value="{{isset($solicitudPabellon->protocolo) ? $solicitudPabellon->protocolo->nr_perdida_sanguinea : ''}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5">
                        <label for="gl_tromboembolica">Enfermedad Tramboembólica<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="gl_tromboembolica_1">Baja</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tromboembolica_1" name="gl_tromboembolica" value="Baja" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->gl_tromboembolica == "Baja" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tromboembolica_2">Media</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tromboembolica_2" name="gl_tromboembolica" value="Media" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tromboembolica == "Media" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tromboembolica_3">Alta</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tromboembolica_3" name="gl_tromboembolica" value="Alta" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tromboembolica == "Alta" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-2">
                                <label for="gl_tromboembolica_4">Muy Alta</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tromboembolica_4" name="gl_tromboembolica" value="Muy Alta" {{!isset($solicitudPabellon->protocolo) || $solicitudPabellon->protocolo->gl_tromboembolica == "Muy Alta" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 offset-sm-2">
                        <label for="bo_tratamiento_antibiotico">Tratamiento de antibióticos<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_tratamiento_antibiotico_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_tratamiento_antibiotico_1" name="bo_tratamiento_antibiotico" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_tratamiento_antibiotico == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_tratamiento_antibiotico_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_tratamiento_antibiotico_2" name="bo_tratamiento_antibiotico" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_tratamiento_antibiotico == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_profilosis">Profilaxis de antibióticos<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_profilosis_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_profilosis_1" name="bo_profilosis" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_profilosis == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_profilosis_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_profilosis_2" name="bo_profilosis" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_profilosis == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Recuentos Conforme</strong></h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="bo_gasa">Gasas<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_gasa_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_gasa_1" name="bo_gasa" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_gasa == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_gasa_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_gasa_2" name="bo_gasa" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_gasa == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_material">Materiales<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_material_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_material_1" name="bo_material" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_material == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_material_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_material_2" name="bo_material" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_material == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_compresa">Compresas<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_compresa_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_compresa_1" name="bo_compresa" value="1" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_compresa == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_compresa_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_compresa_2" name="bo_compresa" value="0" {{isset($solicitudPabellon->protocolo) && $solicitudPabellon->protocolo->bo_compresa == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer text-right">
            <button type="submit" class="btn btn-info">Ingresar Protocolo</button>
        </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    var url_diagnosticos = @json(url('getDiagnostico'));
    var url_medicos = @json(url('getMedico'));
    var url_intervenciones = @json(url('getPrestacion'));
    var url_equipos = @json(url('getEquipo'));
    var url_nr_pabellones = @json(url('getNrPabellon/'));

    function llenarNrPabellon(id_pabellon){
        $("#id_nr_pabellon").css('display','').attr('disabled', false);
        $("#id_nr_pabellon").css('display','').attr('required', true);
        $('.select_nr_pabellones').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 0,
            placeholder: "Seleccione N° Pabellon",
            ajax: {
                url: url_nr_pabellones+'/'+id_pabellon,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                }
                
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    }

    $('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    $('.select_intervenciones').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Código",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_intervenciones,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    $(".select_equipos").select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Rut",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_equipos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    @if (isset($solicitudPabellon->protocolo))
        var solicitudPabellon = @json($solicitudPabellon);
        var protocolo = @json($solicitudPabellon->protocolo);
        llenarNrPabellon(solicitudPabellon.id_pabellon);

        var id_nr_pabellon = [];
		var newOption1 = new Option('{{ $solicitudPabellon->asignacion->nrPabellon->gl_nombre}}', {{ $solicitudPabellon->asignacion->nrPabellon->id }}, true, true);
		id_nr_pabellon.push(newOption1);
        $('#id_nr_pabellon').append(id_nr_pabellon).trigger('change');

        var id_prestacion = [];
        protocolo.prestaciones.forEach(element => {
            var newOption1 = new Option(element.prs_cod+' - '+element.prs_desc, element.prs_corr, true, true);
            id_prestacion.push(newOption1);
        });
        $('#intervencion').append(id_prestacion).trigger('change');

        var id_cie10 = [];
		var newOption1 = new Option('{{ $solicitudPabellon->protocolo->cie10pre->gl_glosa}}', {{ $solicitudPabellon->protocolo->cie10pre->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_diagnostico_pre').append(id_cie10).trigger('change');

        var id_cie10_pos = [];
		var newOption1 = new Option('{{ $solicitudPabellon->protocolo->cie10post->gl_glosa}}', {{ $solicitudPabellon->protocolo->cie10post->id }}, true, true);
		id_cie10_pos.push(newOption1);
        $('#id_diagnostico_pos').append(id_cie10_pos).trigger('change');

        var id_equipo = [];
        protocolo.equipo.forEach(element => {
            var newOption1 = new Option(element.medico.pro_nombres+' '+element.medico.pro_apepat+' '+element.medico.pro_apemat+', '+element.labor.cod_des, element.id_new, true, true);
            id_equipo.push(newOption1);
        });
        $('#equipos').append(id_equipo).trigger('change');
	@endif
</script>
@stop