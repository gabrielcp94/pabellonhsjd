<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Eliminación de Protocolo</title>
</head>
<body>
    <h2>Eliminacion de Protocolo de solicitud N° {{$request->protocolo->id_solicitud_pabellon}}</h2>
    <p>Motivo: {{($request->mensaje)}}</p>
    <p>Solicitado Por: {{$request->usuario_solicitante}}.</p>
    <br>
    <br>
    {{$request->protocolo}}
    {{-- <p>Favor no responder este correo, si tienes dudas comunicarte directamente al correo del emisor del mensaje.</p> --}}
</body>
</html>