<div class="card-body">
    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            <strong>Cirujano: </strong>{{$solicitudPabellon->protocolo->medico->nombre}}<br>
            <strong>Pabellón: </strong>{{$solicitudPabellon->asignacion->pabellon->nombre}}<br>
        </div>
        <div class="col-sm-6 invoice-col">
            <strong>Fecha (Entrada Pabellón): </strong>{{date('d/m/Y H:i:s', strtotime(str_replace("/",".",$solicitudPabellon->protocolo->fc_entrada_pabellon)))}}<br>
            <strong>Sala: </strong>{{$solicitudPabellon->asignacion->nrPabellon->gl_nombre}}<br>
        </div>
    </div>
    <strong>Diagnostico Pre-Operatorio Cie10: </strong>{{$solicitudPabellon->protocolo->cie10pre->gl_glosa}}<br>
    <strong>Diagnostico Pre-Operatorio: </strong>{{$solicitudPabellon->protocolo->gl_diagnostico_pre}}<br>
    <strong>Diagnostico Post-Operatorio Cie10: </strong>{{$solicitudPabellon->protocolo->cie10post->gl_glosa}}<br>
    <strong>Diagnostico Post-Operatorio: </strong>{{$solicitudPabellon->protocolo->gl_diagnostico_post}}<br>
</div>