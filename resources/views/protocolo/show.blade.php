<html>
    <head>
        @include('PDF.style')
        <title>Protocolo Operatorio</title>
    </head>
    <body>
        @include('PDF.encabezado')
        <h2 align="center">Protocolo Operatorio</h2>
        @include('paciente.datosPDF')
        <table id="tabla">
            <tr>
                <th colspan="4" id="tituloTabla">Información Medica</th>
            </tr>
            <tr>
                <th>Re-intervención</th>
                <th>Lista Espera</th>
                <th>AUGE</th>
                <th>Procuramiento</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->bo_re_intervencion == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_lista_espera == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_auge == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_procuramiento == 1 ? 'Si' : 'No'}}</td>
            </tr>
        </table>
        <table id="tabla">
            <tr>
                <th colspan="4" id="subtituloTabla">Datos de Pabellón</th>
            </tr>
            <tr>
                <th>Solicitud</th>
                <th>Fecha Intervención</th>
                <th>Servicio</th>
                <th>Nº Pabellón</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->id}}</td>
                <td>{{$solicitudPabellon->protocolo->fecha_intervencion}}</td>
                <td>{{$solicitudPabellon->pabellon->gl_seudonimo}}</td>
                <td>{{$solicitudPabellon->asignacion->nrPabellon->gl_nombre}}</td>
            </tr>
        </table>
        <table id="tabla">
            <tr>
                <th colspan="4" id="subtituloTabla">Uso de Pabellón</th>
            </tr>
            <tr>
                <th>Inicio Intervención</th>
                <th>Inicio Anestesia</th>
                <th>Inicio Intervención</th>
                <th>Duración Intervención</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->id}}</td>
                <td>{{$solicitudPabellon->protocolo->fecha_intervencion}}</td>
                <td>{{$solicitudPabellon->pabellon->gl_seudonimo}}</td>
                <td>{{$solicitudPabellon->protocolo->duracion}}</td>
            </tr>
            <tr>
                <th>Salida Pabellón</th>
                <th>Termino Anestesia</th>
                <th>Termino Intervención</th>
                <th>Tipo de Intervención</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->id}}</td>
                <td>{{$solicitudPabellon->protocolo->fecha_intervencion}}</td>
                <td>{{$solicitudPabellon->pabellon->gl_seudonimo}}</td>
                <td>{{$solicitudPabellon->gl_tipo_intervencion}}</td>
            </tr>
        </table>
        <table id="tabla">
            <tr>
                <th colspan="2" id="subtituloTabla">Diagnósticos</th>
            </tr>
            <tr>
                <th>Diagnóstico Pre-Operatorio Cie10</th>
                <th>Diagnóstico Post-Operatorio Cie10</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->cie10pre->gl_glosa}}</td>
                <td>{{$solicitudPabellon->protocolo->cie10post->gl_glosa}}</td>
            </tr>
            <tr>
                <th>Diagnóstico Pre-Operatorio Texto</th>
                <th>Diagnóstico Post-Operatorio texto</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->gl_diagnostico_pre}}</td>
                <td>{{$solicitudPabellon->protocolo->gl_diagnostico_pos}}</td>
            </tr>
        </table>
        <table id="tabla">
            <tr>
                <th colspan="2" id="subtituloTabla">Equipo de Apoyo</th>
            </tr>
            <tr>
                <th>Cargo</th>
                <th>Nombre</th>
            </tr>
            @foreach ($solicitudPabellon->protocolo->equipo as $equipo)
                <tr>
                    <td>{{$equipo->labor->cod_des}}</td>
                    <td>{{$equipo->medico->nombre}}</td>
                </tr>
            @endforeach
        </table>
        <table id="tabla">
            <tr>
                <th colspan="2" id="subtituloTabla">Intervención(es)</th>
            </tr>
            <tr>
                <th>Código</th>
                <th>Glosa</th>
            </tr>
            @foreach ($solicitudPabellon->protocolo->prestaciones as $prestacion)
                <tr>
                    <td>{{$prestacion->prs_cod}}</td>
                    <td>{{$prestacion->prs_desc}}</td>
                </tr>
            @endforeach
        </table>
        <table id="tabla">
            <tr>
                <th colspan="2" id="subtituloTabla">Información de Intervención(es)</th>
            </tr>
            <tr>
                <th>Cirugía Practicada</th>
                <th>Riesgo Operatorio</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->gl_cirugia_practicada}}</td>
                <td>{{$solicitudPabellon->protocolo->gl_riesgo_operatorio}}</td>
            </tr>
            <tr>
                <th>Hallazgos IntraOperatorio</th>
                <th>Tipo Anestesia</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->gl_hallazgo}}</td>
                <td>{{$solicitudPabellon->protocolo->gl_tipo_anestesia}}</td>
            </tr>
            <tr>
                <th colspan="2">Descripción Operatoria</th>
            </tr>
            <tr>
                <td colspan="2">{{$solicitudPabellon->protocolo->gl_descripcion}}</td>
            </tr>
            <tr>
                <th>Incidencias Quirurgica</th>
                <th>Incidencias Anestesia</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->gl_incidencia_qui}}</td>
                <td>{{$solicitudPabellon->protocolo->gl_incidencia_ane}}</td>
            </tr>
            <tr>
                <th>Drenajes</th>
                <th>Biopsia</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->bo_drenaje == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_biopsia == 1 ? 'Si' : 'No'}}</td>
            </tr>
            <tr>
                <th>Cultivos</th>
                <th>Tipo Herida</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->bo_cultivo == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->gl_tipo_herida}}</td>
            </tr>
            <tr>
                <th>Perdida Sanguínea</th>
                <th>Profilaxis de antibióticos</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->nr_perdida_sanguinea}} cc</td>
                <td>{{$solicitudPabellon->protocolo->bo_profilosis == 1 ? 'Si' : 'No'}}</td>
            </tr>
            <tr>
                <th colspan="2">Enfermedad Tramboembólica</th>
            </tr>
            <tr>
                <td colspan="2">{{$solicitudPabellon->protocolo->gl_tromboembolica}}</td>
            </tr>
            <tr>
                <th colspan="2">Profilaxis recomendada</th>
            </tr>
            <tr>
                <td colspan="2">
                    @switch($solicitudPabellon->protocolo->gl_tromboembolica)
                        @case('Baja')
                            •	Sólo métodos físicos sin profilaxis farmacológica.
                            @break
                        @case('Media')
                            •	Heparina de 5000  U.I cada 12 hrs. sc <br>
                            •	Dalteparina (Fragmin) 5000 U.I /día sc <br>
                            •	Uso de medias antiembólicas, compresión neumática intermitente, vendaje elástico de extremidades y kinesioterapia de extremidades como medidas complementarias, especialmente si hay una contraindicación de uso de heparinas.                 
                            @break
                        @case('Alta')
                            •	Heparina 5000 U.I c/8-12 hrs. sc  <br>
                            •	Dalteparina (Fragmin) 5000 U.I /día sc <br>
                            •	MAE +CNI + Medidas generales: Los métodos físicos son complementarios y constituyen la alternativa en pacientes con riesgo excesivo de hemorragia, sangrado activo que son contraindicación de estos medicamentos, llegando incluso a la posibilidad de instalar un filtro de vena cava en alguna ocasión.
                            @break
                        @case('Muy Alta')
                            •	Heparina 5000 U.I c/8-12 hrs. sc <br>
                            •	Dalteparina (Fragmin) 5000 U.I /día <br>
                            •	MAE +CNI + Medidas generales: Los métodos físicos son complementarios y constituyen la alternativa en pacientes con riesgo excesivo de hemorragia, sangrado activo que son contraindicación de estos medicamentos. 							Se debe evaluar la posibilidad de instalar un filtro de vena cava en alguna ocasión.                 
                            @break
                        @default
                    @endswitch
                </td>
            </tr>
        </table>
        @if($solicitudPabellon->id_equipo == 1)
            <table>
                <tr>
                    <th>Anexo Cirugía Cardiaca</th>
                </tr>
            </table>
        @endif
        <table id="tabla">
            <tr>
                <th colspan="3" id="subtituloTabla">Recuentos</th>
            </tr>
            <tr>
                <th>Gasas</th>
                <th>Materiales</th>
                <th>Compresas</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->protocolo->bo_gasa == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_material == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->protocolo->bo_compresa == 1 ? 'Si' : 'No'}}</td>
            </tr>
        </table>
        <br><br><br><br><br>
        <table>
            <tr>
                <td align="center">___________________<br>Firma Cirujano</td>
            </tr>
        </table>
        <footer>
            Informe generado el día {{date('d/m/Y')}} a las {{date('H:i:s')}}.
        </footer>
    </body>
</html>