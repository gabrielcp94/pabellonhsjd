@extends('adminlte::page')

@section('title', 'Lista de Usuarios')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Lista de Usuarios</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                                <div class="col-sm-2">
                                    <a href="user/create" class="btn btn-success" type="button" title="Agregar Usuario"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Intervencion(es)</th>
                                    <th>Medico</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($protocolos as $protocolo)
                                        <tr style="font-size:12px">
                                            <td>{{$protocolo->id}}</td>
                                            <td>{{isset($protocolo->solicitudPabellon->paciente) ? $protocolo->solicitudPabellon->paciente->nombre : $protocolo->solicitudPabellon->nombre}}</td>
                                            <td>{{isset($protocolo->solicitudPabellon->paciente) ? $protocolo->solicitudPabellon->paciente->rut : $protocolo->solicitudPabellon->gl_rut}}</td>
                                            <td>@foreach($protocolo->solicitudPabellon->prestaciones as $prestacion)
                                                    {{$prestacion->prs_cod}} {{$prestacion->prs_desc}} <br>
                                                @endforeach <strong>{{$protocolo->solicitudPabellon->gl_intevension}}</strong>
                                            </td>
                                            <td>{{$protocolo->medico->nombre}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="protocolo/{{$protocolo->id}}/edit" title="Editar Protocolo" class="btn btn-warning btn-xs"><i class="fa fa-edit" style="color:white"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $protocolos->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop