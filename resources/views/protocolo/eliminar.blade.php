@extends('adminlte::page')

@section('title', 'Eliminar Protocolo')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i> Eliminar Protocolo N° {{$solicitudPabellon->protocolo->id}}</h1>
@stop

@section('content')
    <div class="card">
        <!-- form start -->
        <form action="{{ route('protocolo.destroy',$solicitudPabellon->protocolo->id) }}" method="POST">
        {{-- <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ProtocoloController@store')}}"> --}}
        {{ csrf_field() }}
        <input type="hidden" id="id" name="id" value="{{isset($solicitudPabellon) ? $solicitudPabellon->protocolo->id : ''}}"/>
        <input type="hidden" id="id_solicitud_pabellon" name="id_solicitud_pabellon" value="{{isset($solicitudPabellon) ? $solicitudPabellon->id : ''}}"/>
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
        </div>
        <div class="card-body">
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    @if($solicitudPabellon->paciente->id_tipo_identificacion_paciente == 1)
                        <strong>Rut: </strong>{{$solicitudPabellon->paciente->rut}}<br>
                    @else
                        <strong>Pasaporte: </strong>{{$solicitudPabellon->paciente->tx_pasaporte}}<br>
                    @endif
                    <strong>Ficha: </strong>
                    @if(isset($solicitudPabellon->paciente->nr_ficha))
                        {{$solicitudPabellon->paciente->nr_ficha}}
                    @else
                        Sin Información
                    @endif<br>
                    <strong>Fecha de Nacimiento: </strong>
                    @if($solicitudPabellon->paciente->fc_nacimiento != '0000-00-00')
                        {{date('d/m/Y', strtotime(str_replace("/",".",$solicitudPabellon->paciente->fc_nacimiento)))}} ({{$solicitudPabellon->paciente->edad}})
                    @else 
                        Sin Información
                    @endif<br>
                    <strong>Previsión: </strong>{{$solicitudPabellon->paciente->prevision->tx_descripcion}} {{$solicitudPabellon->paciente->clasificacionFonasa->tx_descripcion}}
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                    <strong>Nombre: </strong>{{$solicitudPabellon->paciente->nombre}}<br>
                    <strong>Sexo: </strong>{{$solicitudPabellon->paciente->sexo->tx_descripcion}}<br>
                    <strong>Dirección: </strong>{{$solicitudPabellon->paciente->tx_direccion}}<br>
                    <strong>Telefono: </strong>
                    @if(isset($solicitudPabellon->paciente->tx_telefono))
                        {{$solicitudPabellon->paciente->tx_telefono}}
                    @else
                        Sin Información
                    @endif
                </div>
                <!-- /.col -->
            </div>
        </div>

        <div class="card-body">
            <div class="row form-group">
                <div class="col-sm-4">
                    <label for="receptor">Mail:</label>
                    <select class="select2 select2-hidden-accessible" id="receptor" name="receptor[]" multiple="" data-placeholder="Seleccione mail(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                        @foreach ($usuarios as $usuario)
                            <option value="{{$usuario->gl_mail}}">{{$usuario->gl_nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4">
                    <label for="id_user">Usuario Solicitante</label>
                    <select class="form-control select_users" id="id_user" name="id_user" required>
                    </select>
                </div>
                <div class="col-sm-4">
                    <label for="mensaje">Motivo</label>
                    <textarea type="text" class="form-control" id="mensaje" name="mensaje" required></textarea>
                </div>
            </div>
        </div>

        <div class="card-footer text-right">
            @method('DELETE')
            <button onclick="guardar()" type="submit" class="btn btn-danger">Eliminar Protocolo</button>
        </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");
    var url_users = @json(url('getUser'));

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    $('#receptor').select2();

    $('.select_users').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione Usuario",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_users,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    });
</script>
@stop