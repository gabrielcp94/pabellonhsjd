<!DOCTYPE html>
<html lang="es">
    <head>
        <style type="text/css">
            table.table-bordered{ font-size: 13px; }
			table.table-bordered{
                border-collapse: separate;
                border: black 2px solid;
            }
            table.table-bordered td{ border: black 1px solid;}
			table.table-bordered th{ border: black 1px solid;}
        </style>
        <meta charset="UTF-8">
        <title>Insumo</title>
    </head>
    <body class="texto">
        <div class="col-xs-12 padding-0" >
            <table class="table" width="100%">
                <tr colspan="12">
                    <td width="10%">
                        <img style="max-width: 60px;" src="vendor/adminlte/dist/img/logo.png" >
                    </td>
                    <td>
                        <p style="margin-bottom:-15px;">Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                    </td>               
                </tr>
            </table>
        </div>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color:blanchedalmond;" >
                        <strong>Solicitud de Insumo: {{$solicitudInsumo->id}} (Solicitud de Pabellón: {{$solicitudInsumo->id_solicitud_pabellon}})</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%" style="background-color: #eeeeee;">
                        <strong>Solicitante</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Solicitud Visada Por</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Devolucion Visada Por</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%">
                        {{$solicitudInsumo->solicitante->nombre}}
                    </td>
                    <td width="33%">
                        {{$solicitudInsumo->visaSolicitud->nombre ?? ''}}
                    </td>
                    <td width="33%">
                        {{$solicitudInsumo->visaDevolucion->nombre ?? ''}}
                    </td>
                </tr>
                <tr>
                    <td width="34%" style="background-color: #eeeeee;">
                        <strong>Fecha de Solicitud</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Estado</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Categoría</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%">
                        {{$solicitudInsumo->fecha_solicitud}}
                    </td>
                    <td width="33%">
                        {{$solicitudInsumo->estado->gl_nombre}}
                    </td>
                    <td width="33%">
                        @switch($solicitudInsumo->id_categoria)
                            @case(1)
                                Insumos Pabellón
                                @break
                            @case(2)
                                Insumos Anestesia
                                @break
                            @case(3)
                                Medicina
                                @break
                            @case(4)
                                Recuperación
                                @break
                            @default
                        @endswitch
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color:blanchedalmond;" >
                        <strong>Información del Paciente</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%" colspan="2" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Fecha de Nacimiento</strong>
                    </td>
                </tr>
                <tr>
                    <td width="67%" colspan="2">
                        {{$paciente->nombre}}
                    </td>
                    <td width="33%">
                        {{$paciente->fecha_nacimiento}}
                    </td>
                </tr>
                <tr>
                    <td width="34%" style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Ficha</strong>
                    </td>
                    <td width="33%" style="background-color: #eeeeee;">
                        <strong>Edad</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%">
                        {{$paciente->rut}}
                    </td>
                    <td width="33%">
                        {{$paciente->ficha}}
                    </td>
                    <td width="33%">
                        {{$paciente->edad}}
                    </td>
                </tr>
                <tr>
                    <td width="34%" colspan="6" style="background-color: #eeeeee;">
                        <strong>Intervención</strong>
                    </td>
                </tr>
                <tr>
                    <td width="34%" colspan="6">
                        {!!$solicitudPabellon->prestaciones->pluck('nombre')->implode('<br>')!!}<br><strong>{{$solicitudPabellon->gl_intevension}}</strong>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="9" align="center" style="background-color:blanchedalmond;" >
                        <strong>Insumos. Total: ${{number_format($total)}}</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Codigo</strong>
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Cantidad Solicitada</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Cantidad Entregada</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Cantidad Usada</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Cantidad Devuelta</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Cantidad Merma</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Valor Unidad</strong>
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Valor Total</strong>
                    </td>
                </tr>
                @foreach ($solicitudInsumo->insumos as $insumo)
                    <tr>
                        <td width="10%" align="center">
                            {{$insumo->stock->insumo->gl_codigo_articulo}}
                        </td>
                        <td width="20%" align="center">
                            {{$insumo->stock->insumo->gl_nombre}}
                        </td>
                        <td width="10%" align="center">
                            {{$insumo->nr_solicitado}}
                        </td>
                        <td width="10%" align="center">
                            {{$solicitudInsumo->id_estado_solicitud != 9 ? $insumo->nr_entregado : $insumo->nr_solicitado}}
                        </td>
                        <td width="10%" align="center">
                            {{$solicitudInsumo->id_estado_solicitud != 9 ? $insumo->nr_ocupado : $insumo->nr_solicitado}}
                        </td>
                        <td width="10%" align="center">
                            {{$insumo->nr_devuelto}}
                        </td>
                        <td width="10%" align="center">
                            {{$insumo->nr_merma}}
                        </td>
                        <td width="10%" align="center">
                            ${{isset($insumo->do_costo) ? number_format($insumo->do_costo) : number_format($insumo->stock->insumo->nr_precio_medio_ponderado)}}
                        </td>
                        <td width="10%" align="center">
                            ${{number_format(($solicitudInsumo->id_estado_solicitud != 9 ? $insumo->nr_ocupado+$insumo->nr_merma : $insumo->nr_solicitado)*((isset($insumo->do_costo) ? ($insumo->do_costo) : $insumo->stock->insumo->nr_precio_medio_ponderado)))}}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>