@extends('adminlte::page')

@section('title', 'Chequeo Anestesia')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Chequeo Anestesia</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaAnestesiaController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaAnestesia->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-header"></div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="id_tecnico_anestesia">Técnico de Anestesia<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_tecnico_anestesia" name="id_tecnico_anestesia" required>
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($tecnicosAnestesistas as $tecnicoAnestesista)
                                    <option value={{$tecnicoAnestesista->id}} {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->id_tecnico_anestesia == $tecnicoAnestesista->id ? "selected" : ""}}>{{$tecnicoAnestesista->nombre}}</option>								
                                @endforeach
                            </select>       
                        </div>
                        <div class="col-sm-6">
                            <label for="id_enfermera_anestesia">Enfermera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_enfermera_anestesia" name="id_enfermera_anestesia" required>
                                <option value="">Seleccione Enfermera</option>
                                @foreach ($enfermeras as $enfermera)
                                    <option value={{$enfermera->id}} {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->id_enfermera_anestesia == $enfermera->id ? "selected" : ""}}>{{$enfermera->nombre}}</option>								
                                @endforeach
                            </select>  
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_maquina_anestesia">Máquina de Anestesia revisada<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_maquina_anestesia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_maquina_anestesia_1" name="bo_maquina_anestesia" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_maquina_anestesia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_maquina_anestesia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_maquina_anestesia_2" name="bo_maquina_anestesia" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_maquina_anestesia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_monitor_funcional">Mónitores Funcionando<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_monitor_funcional_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_monitor_funcional_1" name="bo_monitor_funcional" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_monitor_funcional == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_monitor_funcional_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_monitor_funcional_2" name="bo_monitor_funcional" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_monitor_funcional == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_cal_sodada">Cal Sodada<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_cal_sodada_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cal_sodada_1" name="bo_cal_sodada" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_cal_sodada == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_cal_sodada_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cal_sodada_2" name="bo_cal_sodada" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_cal_sodada == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_brazales">Brazales<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_brazales_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_brazales_1" name="bo_brazales" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_brazales == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_brazales_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_brazales_2" name="bo_brazales" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_brazales == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_laringoscopio">Laringoscopio Revisado<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_laringoscopio_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_laringoscopio_1" name="bo_laringoscopio" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_laringoscopio == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_laringoscopio_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_laringoscopio_2" name="bo_laringoscopio" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_laringoscopio == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_aspiracion">Aspiración Funcionando<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_aspiracion_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aspiracion_1" name="bo_aspiracion" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_aspiracion == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_aspiracion_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aspiracion_2" name="bo_aspiracion" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_aspiracion == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_picaron">Picarón<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_picaron_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_picaron_1" name="bo_picaron" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_picaron == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_picaron_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_picaron_2" name="bo_picaron" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_picaron == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_picaron_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_picaron_3" name="bo_picaron" value="2" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_picaron == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_portasueros">Portasueros<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_portasueros_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_portasueros_1" name="bo_portasueros" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_portasueros == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_portasueros_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_portasueros_2" name="bo_portasueros" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_portasueros == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_drogas">Drogas<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_drogas_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_drogas_1" name="bo_drogas" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_drogas == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_drogas_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_drogas_2" name="bo_drogas" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_drogas == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_insumos_anestesia">Insumos<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_insumos_anestesia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_insumos_anestesia_1" name="bo_insumos_anestesia" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_insumos_anestesia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_insumos_anestesia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_insumos_anestesia_2" name="bo_insumos_anestesia" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_insumos_anestesia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_bic">BIC<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_bic_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_bic_1" name="bo_bic" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_bic == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_bic_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_bic_2" name="bo_bic" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_bic == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_bic_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_bic_3" name="bo_bic" value="2" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_bic == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_bic">BIC</label>
                            <select class="select2 select2-hidden-accessible" id="gl_bic" name="gl_bic[]" multiple="" data-placeholder="" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                <option value="PCA" {{$solicitudPabellon->pausaAnestesia && stristr($solicitudPabellon->pausaAnestesia->gl_bic, "PCA") ? "selected" : ""}}>PCA</option>
                                <option value="BIC" {{$solicitudPabellon->pausaAnestesia && stristr($solicitudPabellon->pausaAnestesia->gl_bic, "BIC") ? "selected" : ""}}>BIC</option>
                                <option value="Base Primea" {{$solicitudPabellon->pausaAnestesia && stristr($solicitudPabellon->pausaAnestesia->gl_bic, "Base Primea") ? "selected" : ""}}>Base Primea</option>
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_carro_anestesia">Carro Anestesia completo<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_carro_anestesia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_carro_anestesia_1" name="bo_carro_anestesia" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_carro_anestesia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_carro_anestesia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_carro_anestesia_2" name="bo_carro_anestesia" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_carro_anestesia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_fonendoscopio">Fonendoscopio<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_fonendoscopio_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_fonendoscopio_1" name="bo_fonendoscopio" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_fonendoscopio == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_fonendoscopio_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_fonendoscopio_2" name="bo_fonendoscopio" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_fonendoscopio == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_game_elastic">Game Elastic<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_game_elastic_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_game_elastic_1" name="bo_game_elastic" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_game_elastic == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_game_elastic_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_game_elastic_2" name="bo_game_elastic" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_game_elastic == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_conductor">Conductor de tubo<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_conductor_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_conductor_1" name="bo_conductor" value="1" {{$solicitudPabellon->pausaAnestesia && $solicitudPabellon->pausaAnestesia->bo_conductor == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_conductor_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_conductor_2" name="bo_conductor" value="0" {{!$solicitudPabellon->pausaAnestesia || $solicitudPabellon->pausaAnestesia->bo_conductor == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="gl_observacion">Observaciones</label>
                            <input type="text" class="form-control" id="gl_observacion" name="gl_observacion" value="{{$solicitudPabellon->pausaAnestesia->gl_observacion ?? ''}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    $('#gl_bic').select2();
</script>
@stop