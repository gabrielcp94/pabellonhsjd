@extends('adminlte::page')

@section('title', 'Ingreso de Enfermería')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>{{$solicitudPabellon->id_pabellon == 1 ? 'Pausa Pre-Operatorio' : 'Ingreso de Enfermería'}}</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaPreController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaPre->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-header"></div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="fc_ingreso_servicio">Entrada Servicio<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_ingreso_servicio" name="fc_ingreso_servicio" value="{{$solicitudPabellon->pausaPre ? date("Y-m-d\TH:i", strtotime($solicitudPabellon->pausaPre->fc_ingreso_servicio)) : date("Y-m-d\TH:i")}}" required>
                            </div>	                 
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_aislamiento">Aislamiento<span style="color:#FF0000";>*</span></label>
                            <select class="select2 select2-hidden-accessible" id="gl_aislamiento" name="gl_aislamiento[]" multiple="" data-placeholder="" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                                <option value="Contacto" {{$solicitudPabellon->pausaPre && stristr($solicitudPabellon->pausaPre->gl_aislamiento, "Contacto") ? "selected" : ""}}>Contacto</option>
                                <option value="Gotitas" {{$solicitudPabellon->pausaPre && stristr($solicitudPabellon->pausaPre->gl_aislamiento, "Gotitas") ? "selected" : ""}}>Gotitas</option>
                                <option value="Aéreo" {{$solicitudPabellon->pausaPre && stristr($solicitudPabellon->pausaPre->gl_aislamiento, "Aéreo") ? "selected" : ""}}>Aéreo</option>
                                <option value="No" {{$solicitudPabellon->pausaPre && stristr($solicitudPabellon->pausaPre->gl_aislamiento, "No") ? "selected" : ""}}>No</option>
                            </select>                
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_brazalete">Brazalete<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_brazalete_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_brazalete_1" name="bo_brazalete" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_brazalete == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_brazalete_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_brazalete_2" name="bo_brazalete" value="0" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_brazalete == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="id_servicio">Servicio<span style="color:#FF0000";>*</span></label>
                            <select class="select2 select2-hidden-accessible" id="id_servicio" name="id_servicio" data-placeholder="" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                                <option value="">Seleccione Servicio</option>
                                @foreach ($unidades as $unidad)
                                    <option value={{$unidad->id}} {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->id_servicio == $unidad->id ? "selected" : ""}}>{{$unidad->gl_nombre}}</option>
                                @endforeach
                            </select>     
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_tabaco">Tabaco<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_tabaco" name="gl_tabaco" value="{{$solicitudPabellon->pausaPre->gl_tabaco ?? ''}}" required>
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_alcohol">Alcohol<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_alcohol" name="gl_alcohol" value="{{$solicitudPabellon->pausaPre->gl_alcohol ?? ''}}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="gl_droga">Droga<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_droga" name="gl_droga" value="{{$solicitudPabellon->pausaPre->gl_droga ?? ''}}" required>
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_peso">Peso<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_peso" name="gl_peso" value="{{$solicitudPabellon->pausaPre->gl_peso ?? ''}}" required>
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_talla">Talla<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_talla" name="gl_talla" value="{{$solicitudPabellon->pausaPre->gl_talla ?? ''}}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="gl_alergia">Alergias<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_alergia" name="gl_alergia" value="{{$solicitudPabellon->pausaPre->gl_alergia ?? ''}}" required>
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_hgt">HGT<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_hgt" name="gl_hgt" value="{{$solicitudPabellon->pausaPre->gl_hgt ?? ''}}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_hipertermia">Riesgo de Hipertermia Maligna<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_hipertermia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_hipertermia_1" name="bo_hipertermia" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_hipertermia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_hipertermia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_hipertermia_2" name="bo_hipertermia" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_hipertermia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_cancer">Cancer<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_cancer_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cancer_1" name="bo_cancer" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_cancer == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_cancer_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cancer_2" name="bo_cancer" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_cancer == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="id_marcacion">Marcación<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="id_marcacion_1">Izquierdo</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="id_marcacion_1" name="id_marcacion" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->id_marcacion == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="id_marcacion_2">Derecho</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="id_marcacion_2" name="id_marcacion" value="2" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->id_marcacion == 2 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-2">
                                    <label for="id_marcacion_3">Bilateral</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="id_marcacion_3" name="id_marcacion" value="3" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->id_marcacion == 3 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="id_marcacion_4">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="id_marcacion_4" name="id_marcacion" value="4" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->id_marcacion == 4 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="id_diagnostico_pre">Diag. Pre-Operatorio (CIE10)<span style="color:#FF0000";>*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control select_diagnosticos" id="id_diagnostico_pre" name="id_diagnostico_pre" required>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="gl_antecedente_morbido">Antec. Mórbidos<span style="color:#FF0000";>*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" id="gl_antecedente_morbido" name="gl_antecedente_morbido" required>{{$solicitudPabellon->pausaPre->gl_antecedente_morbido ?? ''}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="gl_antecedente_quirurgico">Antec. Quirúrgicos<span style="color:#FF0000";>*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" id="gl_antecedente_quirurgico" name="gl_antecedente_quirurgico" required>{{$solicitudPabellon->pausaPre->gl_antecedente_quirurgico ?? ''}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="gl_medicamento">Medicamentos uso Habitual<span style="color:#FF0000";>*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" id="gl_medicamento" name="gl_medicamento" required>{{$solicitudPabellon->pausaPre->gl_medicamento ?? ''}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_conciente">Conciente<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_conciente_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_conciente_1" name="bo_conciente" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_conciente == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_conciente_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_conciente_2" name="bo_conciente" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_conciente == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_orientacion">Orientado<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_orientacion_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_orientacion_1" name="bo_orientacion" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_orientacion == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_orientacion_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_orientacion_2" name="bo_orientacion" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_orientacion == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_ayuna">En Ayunas<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_ayuna_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ayuna_1" name="bo_ayuna" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_ayuna == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_ayuna_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ayuna_2" name="bo_ayuna" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_ayuna == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_via_venosa">Vía Venosa<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_via_venosa_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_via_venosa_1" name="bo_via_venosa" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_via_venosa == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_via_venosa_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_via_venosa_2" name="bo_via_venosa" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_via_venosa == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_fleboclisis">Fleboclisis<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_fleboclisis_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_fleboclisis_1" name="bo_fleboclisis" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_fleboclisis == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_fleboclisis_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_fleboclisis_2" name="bo_fleboclisis" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_fleboclisis == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_sng">S.N.G.<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_sng_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_sng_1" name="bo_sng" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_sng == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_sng_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_sng_2" name="bo_sng" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_sng == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_folley">S. Folley<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_folley_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_folley_1" name="bo_folley" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_folley == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_folley_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_folley_2" name="bo_folley" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_folley == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_ekg">E.K.G.<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_ekg_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ekg_1" name="bo_ekg" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_ekg == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_ekg_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ekg_2" name="bo_ekg" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_ekg == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_tqt_permeable">TQT Permeable<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_tqt_permeable_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tqt_permeable_1" name="bo_tqt_permeable" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_tqt_permeable == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_tqt_permeable_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tqt_permeable_2" name="bo_tqt_permeable" value="0" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_tqt_permeable == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_tqt_permeable_2">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tqt_permeable_2" name="bo_tqt_permeable" value="2" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_tqt_permeable == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_cortar_vello">Cortar Vello<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_cortar_vello_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cortar_vello_1" name="bo_cortar_vello" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_cortar_vello == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_cortar_vello_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cortar_vello_2" name="bo_cortar_vello" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_cortar_vello == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_calificacion_sg">Clasificación SG<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_calificacion_sg_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_calificacion_sg_1" name="bo_calificacion_sg" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_calificacion_sg == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_calificacion_sg_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_calificacion_sg_2" name="bo_calificacion_sg" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_calificacion_sg == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_ficha">Ficha<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_ficha_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ficha_1" name="bo_ficha" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_ficha == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_ficha_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_ficha_2" name="bo_ficha" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_ficha == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_consentimiento_informado">Consentimiento Inf.<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_consentimiento_informado_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_consentimiento_informado_1" name="bo_consentimiento_informado" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_consentimiento_informado == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_consentimiento_informado_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_consentimiento_informado_2" name="bo_consentimiento_informado" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_consentimiento_informado == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_rayos_x">Rayos X<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_rayos_x_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_rayos_x_1" name="bo_rayos_x" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_rayos_x == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_rayos_x_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_rayos_x_2" name="bo_rayos_x" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_rayos_x == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_eco_scanner">Eco-scanner<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_eco_scanner_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_eco_scanner_1" name="bo_eco_scanner" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_eco_scanner == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_eco_scanner_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_eco_scanner_2" name="bo_eco_scanner" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_eco_scanner == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_alergia_medio_contraste">Alergia a medio contraste<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_alergia_medio_contraste_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_alergia_medio_contraste_1" name="bo_alergia_medio_contraste" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_alergia_medio_contraste == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_alergia_medio_contraste_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_alergia_medio_contraste_2" name="bo_alergia_medio_contraste" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_alergia_medio_contraste == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_protesis_dental">Prótesis Dental<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_protesis_dental_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_protesis_dental_1" name="bo_protesis_dental" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_protesis_dental == 1 ? "checked" : ""}} onclick='bloquearProtesisDental($(this).val());' required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_protesis_dental_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_protesis_dental_2" name="bo_protesis_dental" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_protesis_dental == 0 ? "checked" : ""}} onclick='bloquearProtesisDental($(this).val());'>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_protesis_dental">Prótesis Dental</label>
                            <input type="text" class="form-control" id="gl_protesis_dental" name="gl_protesis_dental" value="{{$solicitudPabellon->pausaPre->gl_protesis_dental ?? ''}}">
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_presion_arterial">P/A<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_presion_arterial" name="gl_presion_arterial" value="{{$solicitudPabellon->pausaPre->gl_presion_arterial ?? ''}}" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_pulso">Pulso<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_pulso" name="gl_pulso" value="{{$solicitudPabellon->pausaPre->gl_pulso ?? ''}}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_piel_sana">Piel Sana<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="bo_piel_sana_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_piel_sana_1" name="bo_piel_sana" value="1" {{$solicitudPabellon->pausaPre && $solicitudPabellon->pausaPre->bo_piel_sana == 1 ? "checked" : ""}} onclick='bloquearPielSana($(this).val());' required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="bo_piel_sana_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_piel_sana_2" name="bo_piel_sana" value="0" {{!$solicitudPabellon->pausaPre || $solicitudPabellon->pausaPre->bo_piel_sana == 0 ? "checked" : ""}} onclick='bloquearPielSana($(this).val());'>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_piel_sana">Piel Sana</label>
                            <input type="text" class="form-control" id="gl_piel_sana" name="gl_piel_sana" value="{{$solicitudPabellon->pausaPre->gl_piel_sana ?? ''}}">
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_temperatura">T°<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_temperatura" name="gl_temperatura" value="{{$solicitudPabellon->pausaPre->gl_temperatura ?? ''}}" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_saturacion">SAT. O2<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_saturacion" name="gl_saturacion" value="{{$solicitudPabellon->pausaPre->gl_saturacion ?? ''}}" required>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="gl_examen">Exámenes<span style="color:#FF0000";>*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" id="gl_examen" name="gl_examen" required>{{$solicitudPabellon->pausaPre->gl_examen ?? ''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <a class="btn btn-warning pull-right" target="_blank" href="http://10.6.3.43/pabellon/pausa/pdf/pre_info/{{$solicitudPabellon->id}}" style="color: white">Hoja Información Paciente</a>
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    var url_diagnosticos = @json(url('getDiagnostico'));

    $('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    function bloquearProtesisDental(bo){
        if(bo != 1){
            $("#gl_protesis_dental").css('display','').attr('disabled', true);
        }else{
            $("#gl_protesis_dental").css('display','').attr('disabled', false);
            $("#gl_protesis_dental").css('display','').attr('required', true);
        }
    }

    function bloquearPielSana(bo){
        if(bo != 1){
            $("#gl_piel_sana").css('display','').attr('disabled', false);
            $("#gl_piel_sana").css('display','').attr('required', true);
        }else{
            $("#gl_piel_sana").css('display','').attr('disabled', true);
        }
    }

    $('#gl_aislamiento').select2();
    $('#id_servicio').select2();

    @if ($solicitudPabellon->pausaPre)
        var pausaPre = @json($solicitudPabellon->pausaPre);
        bloquearProtesisDental(pausaPre.bo_protesis_dental);
        bloquearPielSana(pausaPre.bo_piel_sana);

        var id_cie10 = [];
		var newOption1 = new Option('{{$solicitudPabellon->pausaPre->cie10->cd_cie10}} {{$solicitudPabellon->pausaPre->cie10->gl_glosa}}', {{ $solicitudPabellon->pausaPre->cie10->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_diagnostico_pre').append(id_cie10).trigger('change');
    @else
        var pausaPre = @json($solicitudPabellon);
        bloquearProtesisDental(0);
        bloquearPielSana(0);

        var id_cie10 = [];
		var newOption1 = new Option('{{$solicitudPabellon->cie10->cd_cie10}} {{$solicitudPabellon->cie10->gl_glosa}}', {{ $solicitudPabellon->cie10->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_diagnostico_pre').append(id_cie10).trigger('change');
	@endif
</script>
@stop