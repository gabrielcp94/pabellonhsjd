@extends('adminlte::page')

@section('title', 'Pausa Intra-Operatoria')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Pausa Intra-Operatoria</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaIntraController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaIntra->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <input type="hidden" id="bo_borrador" name="bo_borrador" value="0"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>CUP</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="gl_folley">S.Folley N°</label>
                            <input type="text" class="form-control" id="gl_folley" name="gl_folley" value="{{$solicitudPabellon->pausaIntra->gl_folley ?? ''}}">             
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_folley_balon">Balon con</label>
                            <input type="text" class="form-control" id="gl_folley_balon" name="gl_folley_balon" placeholder="CC" value="{{$solicitudPabellon->pausaIntra->gl_folley_balon ?? ''}}">             
                        </div>
                        <div class="col-sm-4">
                            <label for="id_folley_instalada">Instalada por</label>
                            <select class="form-control" id="id_folley_instalada" name="id_folley_instalada">
                                <option value="">Seleccione Enfermera</option>
                                @foreach ($enfermeras as $enfermera)
                                    <option value={{$enfermera->id}} {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->id_folley_instalada == $enfermera->id ? "selected" : ""}}>{{$enfermera->nombre}}</option>								
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Accesos Vasculares</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="gl_tipo_acceso">Tipo acceso vascular</label>
                            <select class="form-control" id="gl_tipo_acceso" name="gl_tipo_acceso">
                                <option value="" >Seleccione Tipo Acceso Vascular</option>
                                <option value="VVP" >VVP</option>
                                <option value="CVC" >CVC</option>
                                <option value="Linea Arterial" >Linea Arterial</option>
                            </select> 
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_sitio_puncion">Sitio punción</label>
                            <input type="text" class="form-control" id="gl_sitio_puncion" name="gl_sitio_puncion">             
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_calibre">Calibre</label>
                            <input type="text" class="form-control" id="gl_calibre" name="gl_calibre">             
                        </div>
                        <div class="col-sm-3">
                            <label for="id_responsable">Responsable</label>
                            <select class="form-control" id="id_responsable" name="id_responsable">
                                <option value="">Seleccione Profesional</option>
                                @foreach ($profesionales as $profesional)
                                    <option value={{$profesional->id}}>{{$profesional->nombre}}</option>								
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-sm-1">
                            <br><a class="btn btn-xl btn-success btn-xl pull-right" target="_blank" onclick="generar_tabla_accesos_vasculares(1,0)" style="color: white"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="accesosVasculares"> </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Drenajes</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="gl_tipo_drenaje">Tipo Drenaje</label>
                            <input type="text" class="form-control" id="gl_tipo_drenaje" name="gl_tipo_drenaje">             
                        </div>
                        <div class="col-sm-3">
                            <label for="nr_cantidad">N° Drenaje</label>
                            <input type="number" class="form-control" id="nr_cantidad" name="nr_cantidad">             
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_contenido">Contenido drenaje</label>
                            <input type="text" class="form-control" id="gl_contenido" name="gl_contenido">             
                        </div>
                        <div class="col-sm-1">
                            <br><a class="btn btn-xl btn-success btn-xl pull-right" target="_blank" onclick="generar_tabla_drenajes(1,0)" style="color: white"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="drenajes"> </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Aseo Pre Operatorio</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="bo_aseo_realizado">Realizado<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_aseo_realizado_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aseo_realizado_1" name="bo_aseo_realizado" value="1" onclick="aseo($(this).val());" {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->bo_aseo_realizado == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_aseo_realizado_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aseo_realizado_2" name="bo_aseo_realizado" value="0" onclick="aseo($(this).val());" {{!$solicitudPabellon->pausaIntra || $solicitudPabellon->pausaIntra->bo_aseo_realizado == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="gl_antiseptico">Antiséptico</label>
                            <select class="form-control" id="gl_antiseptico" name="gl_antiseptico" required>
                                <option value="" >Seleccione Antiséptico</option>
                                <option value="Povidona" {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->gl_antiseptico == "Povidona" ? "selected" : ""}}>Povidona</option>
                                <option value="Clorhexidina" {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->gl_antiseptico == "Clorhexidina" ? "selected" : ""}}>Clorhexidina</option>
                                <option value="No Aplica" {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->gl_antiseptico == "No Aplica" ? "selected" : ""}}>No Aplica</option>
                            </select> 
                        </div>
                        <div class="col-sm-4">
                            <label for="id_responsable_aseo">Responsable</label>
                            <select class="form-control" id="id_responsable_aseo" name="id_responsable_aseo" required>
                                <option value="">Seleccione Enfermera</option>
                                @foreach ($enfermeras as $enfermera)
                                    <option value={{$enfermera->id}} {{$solicitudPabellon->pausaIntra && $solicitudPabellon->pausaIntra->id_responsable_aseo == $enfermera->id ? "selected" : ""}}>{{$enfermera->nombre}}</option>								
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Uso Pabellón</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="fc_entrada_servicio">Entrada Servicio<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_entrada_servicio" name="fc_entrada_servicio" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_entrada_servicio) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_entrada_servicio)) : ""}}">
                            </div>	
                        </div>
                        <div class="col-sm-8">
                            <label for="gl_observacion_horario">Observaciones Horario</label>
                            <input type="text" class="form-control" id="gl_observacion_horario" name="gl_observacion_horario" value="{{$solicitudPabellon->pausaIntra->gl_observacion_horario ?? ''}}">             
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="fc_entrada_pabellon">Entrada Quirofano<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_entrada_pabellon" name="fc_entrada_pabellon" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_entrada_pabellon) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_entrada_pabellon)) : ""}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="fc_inicio_anestesia">Inicio Anestesia<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_inicio_anestesia" name="fc_inicio_anestesia" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_inicio_anestesia) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_inicio_anestesia)) : ""}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="fc_inicio_intervencion">Inicio Intervención<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_inicio_intervencion" name="fc_inicio_intervencion" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_inicio_intervencion) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_inicio_intervencion)) : ""}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="fc_salida_pabellon">Salida Quirofano<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_salida_pabellon" name="fc_salida_pabellon" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_salida_pabellon) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_salida_pabellon)) : ""}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="fc_fin_anestesia">Termino Anestesia<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_fin_anestesia" name="fc_fin_anestesia" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_fin_anestesia) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_fin_anestesia)) : ""}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="fc_fin_intervencion">Termino Intervención<span style="color:#FF0000";>*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="datetime-local" class="form-control" id="fc_fin_intervencion" name="fc_fin_intervencion" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaIntra->fc_fin_intervencion) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaIntra->fc_fin_intervencion)) : ""}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-warning" onclick="borrador()">Borrador</button>
                <button type="submit" class="btn btn-info" onclick="guardar()">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    function borrador(){
        $("#bo_borrador").val(1);
        $("#fc_entrada_servicio").css('display','').attr('required', false);
        $("#fc_entrada_pabellon").css('display','').attr('required', false);
        $("#fc_inicio_anestesia").css('display','').attr('required', false);
        $("#fc_inicio_intervencion").css('display','').attr('required', false);
        $("#fc_salida_pabellon").css('display','').attr('required', false);
        $("#fc_fin_anestesia").css('display','').attr('required', false);
        $("#fc_fin_intervencion").css('display','').attr('required', false);
    }

    function guardar(){
        $("#fc_entrada_servicio").css('display','').attr('required', true);
        $("#fc_entrada_pabellon").css('display','').attr('required', true);
        $("#fc_inicio_anestesia").css('display','').attr('required', true);
        $("#fc_inicio_intervencion").css('display','').attr('required', true);
        $("#fc_salida_pabellon").css('display','').attr('required', true);
        $("#fc_fin_anestesia").css('display','').attr('required', true);
        $("#fc_fin_intervencion").css('display','').attr('required', true);
    }

    function generar_tabla_accesos_vasculares(agregar, eliminar){
        var id_solicitud = document.getElementById("id_solicitud").value;
        var id_asignacion = document.getElementById("id_asignacion").value;
        var gl_tipo_acceso = document.getElementById("gl_tipo_acceso").value;
        var gl_sitio_puncion = document.getElementById("gl_sitio_puncion").value;
        var gl_calibre = document.getElementById("gl_calibre").value;
        var id_responsable = document.getElementById("id_responsable").value;
        if((gl_tipo_acceso == '' || gl_sitio_puncion == '' || gl_calibre == '' || id_responsable == '') && agregar == 1){
            alert('Ingrese todos los campos');
        }else{
            $.getJSON("{{action('PausaIntraController@agregarAccesoVascular')}}?gl_tipo_acceso="+gl_tipo_acceso+"&gl_sitio_puncion="+gl_sitio_puncion+"&gl_calibre="+gl_calibre+"&id_responsable="+id_responsable+"&agregar="+agregar+"&eliminar="+eliminar+"&id_solicitud="+id_solicitud+"&id_asignacion="+id_asignacion,
			function(data){
                let tablaAccesosVasculares= '<div class="col-12 table-responsive"><table class="table table-striped">';
                data.forEach(element => {
                    tablaAccesosVasculares+="<tr>";
                    tablaAccesosVasculares+="<th width='16%'>"+element.gl_tipo_acceso+"</th>";
                    tablaAccesosVasculares+="<th width='26%'>"+element.gl_sitio_puncion+"</th>";
                    tablaAccesosVasculares+="<th width='26%'>"+element.gl_calibre+"</th>";
                    tablaAccesosVasculares+="<th width='26%'>"+element.responsable.nombre+"</th>";
                    tablaAccesosVasculares+="<th width='6%'><a class='btn btn-sm btn-danger btn-sm pull-right' onclick='generar_tabla_accesos_vasculares(0, "+element.id+")' style='color: white'><i class='fa fa-trash'></i></th>";
                    tablaAccesosVasculares+="</tr>";
                });
                tablaAccesosVasculares+="</table></div>";  
                document.getElementById('accesosVasculares').innerHTML = tablaAccesosVasculares;
            })
            
            $("#gl_tipo_acceso").val(null);
            $("#gl_sitio_puncion").val(null);
            $("#gl_calibre").val(null);
            $("#id_responsable").val(null);
        }
	}
    generar_tabla_accesos_vasculares(0,0);

    function generar_tabla_drenajes(agregar, eliminar){
        var id_solicitud = document.getElementById("id_solicitud").value;
        var id_asignacion = document.getElementById("id_asignacion").value;
        var gl_tipo = document.getElementById("gl_tipo_drenaje").value;
        var nr_cantidad = document.getElementById("nr_cantidad").value;
        var gl_contenido = document.getElementById("gl_contenido").value;
        if((gl_tipo == '' || nr_cantidad == '' || gl_contenido == '') && agregar == 1){
            alert('Ingrese todos los campos');
        }else{
            $.getJSON("{{action('PausaIntraController@agregarDrenaje')}}?gl_tipo="+gl_tipo+"&nr_cantidad="+nr_cantidad+"&gl_contenido="+gl_contenido+"&agregar="+agregar+"&eliminar="+eliminar+"&id_solicitud="+id_solicitud+"&id_asignacion="+id_asignacion,
			function(data){
                let tablaDrenajes= '<div class="col-12 table-responsive"><table class="table table-striped">';
                data.forEach(element => {
                    tablaDrenajes+="<tr>";
                    tablaDrenajes+="<th width='35%'>"+element.gl_tipo+"</th>";
                    tablaDrenajes+="<th width='25%'>"+element.nr_cantidad+"</th>";
                    tablaDrenajes+="<th width='35%'>"+element.gl_contenido+"</th>";
                    tablaDrenajes+="<th width='5%'><a class='btn btn-sm btn-danger btn-sm pull-right' onclick='generar_tabla_drenajes(0, "+element.id+")' style='color: white'><i class='fa fa-trash'></i></th>";
                    tablaDrenajes+="</tr>";
                });
                tablaDrenajes+="</table></div>";  
                document.getElementById('drenajes').innerHTML = tablaDrenajes;
            })
            
            $("#gl_tipo_drenaje").val(null);
            $("#nr_cantidad").val(null);
            $("#gl_contenido").val(null);
        }
	}
    generar_tabla_drenajes(0,0);

    function verificarFechas(){
        var fc_entrada_servicio = document.getElementById("fc_entrada_servicio").value;
        var fc_entrada_pabellon = document.getElementById("fc_entrada_pabellon").value;
        var fc_inicio_anestesia = document.getElementById("fc_inicio_anestesia").value;
        var fc_inicio_intervencion = document.getElementById("fc_inicio_intervencion").value;
        var fc_salida_pabellon = document.getElementById("fc_salida_pabellon").value;
        var fc_fin_anestesia = document.getElementById("fc_fin_anestesia").value;
        var fc_fin_intervencion = document.getElementById("fc_fin_intervencion").value;
        var solicitudPabellon = @json($solicitudPabellon);
        var fc_intervencion = solicitudPabellon.fc_intervencion;
        var fc_minima = new Date(fc_intervencion);
        var fc_maxima = new Date(fc_intervencion);
        fc_maxima.setDate(fc_maxima.getDate() + 1);
        fc_maxima.setHours(23);
        fc_maxima.setMinutes(59);
        fc_maxima.setSeconds(59);
        fc_minima.setHours(00);
        fc_minima.setMinutes(00);
        fc_minima.setSeconds(00);
        if(new Date(fc_entrada_servicio) > fc_maxima || new Date(fc_entrada_servicio) < fc_minima){
            alert('La entrada al servicio debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_entrada_servicio").val(null);
        }
        if(new Date(fc_entrada_pabellon) > fc_maxima || new Date(fc_entrada_pabellon) < fc_minima){
            alert('La entrada al quirofano debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_entrada_pabellon").val(null);
        }
        if(new Date(fc_inicio_anestesia) > fc_maxima || new Date(fc_inicio_anestesia) < fc_minima){
            alert('El inicio de Anestesia debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_inicio_anestesia").val(null);
        }
        if(new Date(fc_inicio_intervencion) > fc_maxima || new Date(fc_inicio_intervencion) < fc_minima){
            alert('El inicio de intervención debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_inicio_intervencion").val(null);
        }
        if(new Date(fc_salida_pabellon) > fc_maxima || new Date(fc_salida_pabellon) < fc_minima){
            alert('La salida al quirofano debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_salida_pabellon").val(null);
        }
        if(new Date(fc_fin_anestesia) > fc_maxima || new Date(fc_fin_anestesia) < fc_minima){
            alert('El fin de Anestesia debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_fin_anestesia").val(null);
        }
        if(new Date(fc_fin_intervencion) > fc_maxima || new Date(fc_fin_intervencion) < fc_minima){
            alert('El fin de intervención debe ser entre la fecha de intervención ('+solicitudPabellon.fc_intervencion+') y un dia despues');
            $("#fc_fin_intervencion").val(null);
        }

        if(fc_entrada_pabellon > fc_salida_pabellon && fc_entrada_pabellon != '' && fc_salida_pabellon != ''){
            alert('La salida de quirofano debe ser posterior a la entrada');
            $("#fc_salida_pabellon").val(null);
        }
        if(fc_inicio_anestesia > fc_fin_anestesia && fc_inicio_anestesia != '' && fc_fin_anestesia != ''){
            alert('El termino de la anestesia debe ser posterior al inicio');
            $("#fc_fin_anestesia").val(null);
        }
        if(fc_inicio_intervencion > fc_fin_intervencion && fc_inicio_intervencion != '' && fc_fin_intervencion != ''){
            alert('El termino de la intervención debe ser posterior al inicio');
            $("#fc_fin_intervencion").val(null);
        }
        if(fc_inicio_intervencion < fc_entrada_pabellon && fc_inicio_intervencion != '' && fc_entrada_pabellon != ''){
            alert('El inicio de la intervención debe ser posterior a la entrada a quirofano');
            $("#fc_inicio_intervencion").val(null);
        }
        if(fc_fin_intervencion > fc_salida_pabellon && fc_fin_intervencion != '' && fc_salida_pabellon != ''){
            alert('El termino de la intervención debe ser antes de la salida de quirofano');
            $("#fc_fin_intervencion").val(null);
        }
    }

    function aseo(realizado){
        if(realizado == 1){
            $("#gl_antiseptico").css('display','').attr('disabled', false);
            $("#gl_antiseptico").css('display','').attr('required', true);
            $("#id_responsable_aseo").css('display','').attr('disabled', false);
            $("#id_responsable_aseo").css('display','').attr('required', true);
        }else{
            $("#gl_antiseptico").css('display','').attr('disabled', true);
            $("#id_responsable_aseo").css('display','').attr('disabled', true);
        }
    }

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    @if (isset($solicitudPabellon->pausaIntra))
        var solicitudPabellon = @json($solicitudPabellon);
        aseo(solicitudPabellon.pausa_intra.bo_aseo_realizado);
    @else
        aseo(0);
    @endif
</script>
@stop