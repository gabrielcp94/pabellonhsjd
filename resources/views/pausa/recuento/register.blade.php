@extends('adminlte::page')

@section('title', 'Pausa Recuento')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Pausa Recuento</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaRecuentoController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaRecuento->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <input type="hidden" id="bo_borrador" name="bo_borrador" value="0"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="id_anestesiologo">Anestesiologo<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_anestesiologo" name="id_anestesiologo">
                                <option value="">Seleccione Anestesiologo</option>
                                @foreach ($anestesiologos as $anestesiologo)
                                    <option value={{$anestesiologo->id}} {{isset($solicitudPabellon->pausaRecuento->id_anestesiologo) && $solicitudPabellon->pausaRecuento->id_anestesiologo == $anestesiologo->id ? 'selected' : ''}}>{{$anestesiologo->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="id_arsenalera">Arsenalera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_arsenalera" name="id_arsenalera">
                                <option value="">Seleccione Arsenalera</option>
                                @foreach ($arsenaleras as $arsenalera)
                                    <option value={{$arsenalera->id}} {{isset($solicitudPabellon->pausaRecuento->id_arsenalera) && $solicitudPabellon->pausaRecuento->id_arsenalera == $arsenalera->id ? 'selected' : ''}}>{{$arsenalera->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="id_pabellonera">Pabellonera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_pabellonera" name="id_pabellonera">
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($pabelloneras as $pabellonera)
                                    <option value={{$pabellonera->id}} {{isset($solicitudPabellon->pausaRecuento->id_pabellonera) && $solicitudPabellon->pausaRecuento->id_pabellonera == $pabellonera->id ? 'selected' : ''}}>{{$pabellonera->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="id_enfermera">Enfermera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_enfermera" name="id_enfermera">
                                <option value="">Seleccione Enfermera</option>
                                @foreach ($enfermeras as $enfermera)
                                    <option value={{$enfermera->id}} {{isset($solicitudPabellon->pausaRecuento->id_enfermera) && $solicitudPabellon->pausaRecuento->id_enfermera == $enfermera->id ? 'selected' : ''}}>{{$enfermera->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="id_cirujano">Cirujano<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_cirujano" name="id_cirujano">
                                <option value="">Seleccione Cirujano</option>
                                @foreach ($cirujanos as $cirujano)
                                    <option value={{$cirujano->id}} {{isset($solicitudPabellon->pausaRecuento->id_cirujano) && $solicitudPabellon->pausaRecuento->id_cirujano == $cirujano->id ? 'selected' : ''}}>{{$cirujano->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Recuentos de gasas y compresas</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-1">
                            <label for="tm_recuento_gc">Hora<br>-</label>
                            <input type="time" class="form-control" id="tm_recuento_gc" name="tm_recuento_gc">             
                        </div>
                        <div class="col-sm-1">
                            <label for="gl_recuento_qco">Tiempo<br>QCO</label>
                            <input type="text" class="form-control" id="gl_recuento_qco" name="gl_recuento_qco">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_gasa_mesa">Gasas<br>mesa</label>
                            <input type="number" class="form-control" id="nr_gasa_mesa" name="nr_gasa_mesa">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_gasa_afuera">Gasas<br>afuera</label>
                            <input type="number" class="form-control" id="nr_gasa_afuera" name="nr_gasa_afuera">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_gasa_total">Total<br>gasas</label>
                            <input type="number" class="form-control" id="nr_gasa_total" name="nr_gasa_total">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_compresa_mesa">Compresas<br>mesa</label>
                            <input type="number" class="form-control" id="nr_compresa_mesa" name="nr_compresa_mesa">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_compresa_afuera">Compresas<br>afuera</label>
                            <input type="number" class="form-control" id="nr_compresa_afuera" name="nr_compresa_afuera">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_compresa_total">Total<br>Compresas</label>
                            <input type="number" class="form-control" id="nr_compresa_total" name="nr_compresa_total">             
                        </div>
                        <div class="col-sm-3">
                            <label for="id_pabellonera_gc">Pabellonera<br>-</label>
                            <select class="form-control" id="id_pabellonera_gc" name="id_pabellonera_gc">
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($pabelloneras as $pabellonera)
                                    <option value={{$pabellonera->id}}>{{$pabellonera->nombre}}</option>								
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <br><a class="btn btn-xl btn-success btn-xl pull-right" target="_blank" onclick="generar_tabla_gasas_compresas(1,0)" style="color: white"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="gasasCompresas"> </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Recuentos total de agujas</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="nr_recuento_sutura">N° Sutura<span style="color:#FF0000";>*</span></label>
                            <input type="number" class="form-control" id="nr_recuento_sutura" name="nr_recuento_sutura" value="{{$solicitudPabellon->pausaRecuento->nr_recuento_sutura ?? ''}}">             
                        </div>
                        <div class="col-sm-3">
                            <label for="nr_recuento_aguja">N° Agujas<span style="color:#FF0000";>*</span></label>
                            <input type="number" class="form-control" id="nr_recuento_aguja" name="nr_recuento_aguja" value="{{$solicitudPabellon->pausaRecuento->nr_recuento_aguja ?? ''}}">             
                        </div>
                        <div class="col-sm-3">
                            <label for="nr_recuento_aguja_total">Total Agujas<span style="color:#FF0000";>*</span></label>
                            <input type="number" class="form-control" id="nr_recuento_aguja_total" name="nr_recuento_aguja_total" value="{{$solicitudPabellon->pausaRecuento->nr_recuento_aguja_total ?? ''}}">             
                        </div>
                        <div class="col-sm-3">
                            <label for="id_pabellonera_aguja">Pabellonera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_pabellonera_aguja" name="id_pabellonera_aguja">
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($pabelloneras as $pabellonera)
                                    <option value={{$pabellonera->id}} {{isset($solicitudPabellon->pausaRecuento->id_pabellonera_aguja) && $solicitudPabellon->pausaRecuento->id_pabellonera_aguja == $pabellonera->id ? 'selected' : ''}}>{{$pabellonera->nombre}}</option>								
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Recuentos de instrumental</strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-1">
                            <label for="tm_recuento_in">Hora</label>
                            <input type="time" class="form-control" id="tm_recuento_in" name="tm_recuento_in">             
                        </div>
                        <div class="col-sm-1">
                            <label for="nr_recuento_caja">N° caja</label>
                            <input type="text" class="form-control" id="nr_recuento_caja" name="nr_recuento_caja">             
                        </div>
                        <div class="col-sm-2">
                            <label for="gl_recuento_caja">Nombre caja</label>
                            <input type="text" class="form-control" id="gl_recuento_caja" name="gl_recuento_caja">             
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_recuento_inicio">Instrumental inicio</label>
                            <input type="number" class="form-control" id="nr_recuento_inicio" name="nr_recuento_inicio" placeholder="piezas">             
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_recuento_termino">Intrumental termino</label>
                            <input type="number" class="form-control" id="nr_recuento_termino" name="nr_recuento_termino" placeholder="piezas">             
                        </div>
                        <div class="col-sm-3">
                            <label for="id_pabellonera_in">Pabellonera</label>
                            <select class="form-control" id="id_pabellonera_in" name="id_pabellonera_in">
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($pabelloneras as $pabellonera)
                                    <option value={{$pabellonera->id}}>{{$pabellonera->nombre}}</option>								
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-sm-1">
                            <br><a class="btn btn-xl btn-success btn-xl pull-right" target="_blank" onclick="generar_tabla_instrumental(1,0)" style="color: white"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="instrumental"> </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label for="gl_observacion">Observaciones</label>
                            <input type="text" class="form-control" id="gl_observacion" name="gl_observacion" value="{{$solicitudPabellon->pausaRecuento->gl_observacion ?? ''}}">             
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-warning" onclick="borrador()">Borrador</button>
                <button type="submit" class="btn btn-info" onclick="guardar()">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    function borrador(){
        $("#bo_borrador").val(1);
        $("#id_anestesiologo").css('display','').attr('required', false);
        $("#id_arsenalera").css('display','').attr('required', false);
        $("#id_pabellonera").css('display','').attr('required', false);
        $("#id_enfermera").css('display','').attr('required', false);
        $("#id_cirujano").css('display','').attr('required', false);
        $("#nr_recuento_sutura").css('display','').attr('required', false);
        $("#nr_recuento_aguja").css('display','').attr('required', false);
        $("#nr_recuento_aguja_total").css('display','').attr('required', false);
        $("#id_pabellonera_aguja").css('display','').attr('required', false);
    }

    function guardar(){
        $("#id_anestesiologo").css('display','').attr('required', true);
        $("#id_arsenalera").css('display','').attr('required', true);
        $("#id_pabellonera").css('display','').attr('required', true);
        $("#id_enfermera").css('display','').attr('required', true);
        $("#id_cirujano").css('display','').attr('required', true);
        $("#nr_recuento_sutura").css('display','').attr('required', true);
        $("#nr_recuento_aguja").css('display','').attr('required', true);
        $("#nr_recuento_aguja_total").css('display','').attr('required', true);
        $("#id_pabellonera_aguja").css('display','').attr('required', true);
    }

    function generar_tabla_gasas_compresas(agregar, eliminar){
        var id_solicitud = document.getElementById("id_solicitud").value;
        var id_asignacion = document.getElementById("id_asignacion").value;
        var tm_recuento = document.getElementById("tm_recuento_gc").value;
        var gl_recuento_qco = document.getElementById("gl_recuento_qco").value;
        var nr_gasa_mesa = document.getElementById("nr_gasa_mesa").value;
        var nr_gasa_afuera = document.getElementById("nr_gasa_afuera").value;
        var nr_gasa_total = document.getElementById("nr_gasa_total").value;
        var nr_compresa_mesa = document.getElementById("nr_compresa_mesa").value;
        var nr_compresa_afuera = document.getElementById("nr_compresa_afuera").value;
        var nr_compresa_total = document.getElementById("nr_compresa_total").value;
        var id_pabellonera = document.getElementById("id_pabellonera_gc").value;
        if((tm_recuento == '' || gl_recuento_qco == '' || nr_gasa_mesa == '' || nr_gasa_afuera == '' || nr_gasa_total == '' || nr_compresa_mesa == '' || nr_compresa_afuera == '' || nr_compresa_total == '' || id_pabellonera == '') && agregar == 1){
            alert('Ingrese todos los campos');
        }else{
            $.getJSON("{{action('PausaRecuentoController@agregarGasasCompresas')}}?tm_recuento="+tm_recuento+"&gl_recuento_qco="+gl_recuento_qco+"&nr_gasa_mesa="+nr_gasa_mesa+"&nr_gasa_afuera="+nr_gasa_afuera+"&nr_gasa_total="+nr_gasa_total+"&nr_compresa_mesa="+nr_compresa_mesa+"&nr_compresa_afuera="+nr_compresa_afuera+"&nr_compresa_total="+nr_compresa_total+"&id_pabellonera="+id_pabellonera+"&agregar="+agregar+"&eliminar="+eliminar+"&id_solicitud="+id_solicitud+"&id_asignacion="+id_asignacion,
			function(data){
                let tablaGasasCompesas= '<div class="col-12 table-responsive"><table class="table table-striped">';
                data.forEach(element => {
                    tablaGasasCompesas+="<tr>";
                    tablaGasasCompesas+="<th width='8%'>"+element.tm_recuento+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.gl_recuento_qco+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_gasa_mesa+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_gasa_afuera+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_gasa_total+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_compresa_mesa+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_compresa_afuera+"</th>";
                    tablaGasasCompesas+="<th width='9%'>"+element.nr_compresa_total+"</th>";
                    tablaGasasCompesas+="<th width='30%'>"+element.pabellonera.nombre+"</th>";
                    tablaGasasCompesas+="<th width='6%'><a class='btn btn-sm btn-danger btn-sm pull-right' onclick='generar_tabla_gasas_compresas(0, "+element.id+")' style='color: white'><i class='fa fa-trash'></i></th>";
                    tablaGasasCompesas+="</tr>";
                });
                tablaGasasCompesas+="</table></div>";  
                document.getElementById('gasasCompresas').innerHTML = tablaGasasCompesas;
            })
            
            $("#tm_recuento_gc").val(null);
            $("#gl_recuento_qco").val(null);
            $("#nr_gasa_mesa").val(null);
            $("#nr_gasa_afuera").val(null);
            $("#nr_gasa_total").val(null);
            $("#nr_compresa_mesa").val(null);
            $("#nr_compresa_afuera").val(null);
            $("#nr_compresa_total").val(null);
            $("#id_pabellonera_gc").val(null);
        }
	}
    generar_tabla_gasas_compresas(0,0);

    function generar_tabla_instrumental(agregar, eliminar){
        var id_solicitud = document.getElementById("id_solicitud").value;
        var id_asignacion = document.getElementById("id_asignacion").value;
        var tm_recuento = document.getElementById("tm_recuento_in").value;
        var nr_recuento_caja = document.getElementById("nr_recuento_caja").value;
        var gl_recuento_caja = document.getElementById("gl_recuento_caja").value;
        var nr_recuento_inicio = document.getElementById("nr_recuento_inicio").value;
        var nr_recuento_termino = document.getElementById("nr_recuento_termino").value;
        var id_pabellonera = document.getElementById("id_pabellonera_in").value;

        if((tm_recuento == '' || nr_recuento_caja == '' || gl_recuento_caja == '' || nr_recuento_inicio == '' || nr_recuento_termino == '' || id_pabellonera == '') && agregar == 1){
            alert('Ingrese todos los campos');
        }else{
            $.getJSON("{{action('PausaRecuentoController@agregarInstrumental')}}?tm_recuento="+tm_recuento+"&nr_recuento_caja="+nr_recuento_caja+"&gl_recuento_caja="+gl_recuento_caja+"&nr_recuento_inicio="+nr_recuento_inicio+"&nr_recuento_termino="+nr_recuento_termino+"&id_pabellonera="+id_pabellonera+"&agregar="+agregar+"&eliminar="+eliminar+"&id_solicitud="+id_solicitud+"&id_asignacion="+id_asignacion,
			function(data){
                let tablaInstrumental= '<div class="col-12 table-responsive"><table class="table table-striped">';
                data.forEach(element => {
                    tablaInstrumental+="<tr>";
                    tablaInstrumental+="<th width='8%'>"+element.tm_recuento+"</th>";
                    tablaInstrumental+="<th width='9%'>"+element.nr_recuento_caja+"</th>";
                    tablaInstrumental+="<th width='17%'>"+element.gl_recuento_caja+"</th>";
                    tablaInstrumental+="<th width='17%'>"+element.nr_recuento_inicio+"</th>";
                    tablaInstrumental+="<th width='17%'>"+element.nr_recuento_termino+"</th>";
                    tablaInstrumental+="<th width='26%'>"+element.pabellonera.nombre+"</th>";
                    tablaInstrumental+="<th width='6%'><a class='btn btn-sm btn-danger btn-sm pull-right' onclick='generar_tabla_instrumental(0, "+element.id+")' style='color: white'><i class='fa fa-trash'></i></th>";
                    tablaInstrumental+="</tr>";
                });
                tablaInstrumental+="</table></div>";  
                document.getElementById('instrumental').innerHTML = tablaInstrumental;
            })
            
            $("#tm_recuento_in").val(null);
            $("#nr_recuento_caja").val(null);
            $("#gl_recuento_caja").val(null);
            $("#nr_recuento_inicio").val(null);
            $("#nr_recuento_termino").val(null);
            $("#id_pabellonera_in").val(null);
        }
	}
    generar_tabla_instrumental(0,0);

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })
</script>
@stop