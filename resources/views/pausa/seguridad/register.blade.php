@extends('adminlte::page')

@section('title', 'Pausa Seguridad')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Pausa Seguridad</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaSeguridadController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaSeguridad->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-header"></div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_nombre_paciente">Nombre Paciente<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_nombre_paciente_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_nombre_paciente_1" name="bo_nombre_paciente" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_nombre_paciente == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_nombre_paciente_2">NN</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_nombre_paciente_2" name="bo_nombre_paciente" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_nombre_paciente == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_intervencion_quirurgica">Intervención Quirúrgica<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_intervencion_quirurgica_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_intervencion_quirurgica_1" name="bo_intervencion_quirurgica" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_intervencion_quirurgica == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_intervencion_quirurgica_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_intervencion_quirurgica_2" name="bo_intervencion_quirurgica" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_intervencion_quirurgica == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_lado_intervencion">Lado Intervención<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_lado_intervencion_1">Izq.</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_lado_intervencion_1" name="bo_lado_intervencion" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_lado_intervencion == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_lado_intervencion_2">Der.</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_lado_intervencion_2" name="bo_lado_intervencion" value="2" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_lado_intervencion == 2 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_lado_intervencion_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_lado_intervencion_3" name="bo_lado_intervencion" value="3" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_lado_intervencion == 3 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_pasos_criticos">Pasos Crítico de Cirugía<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_pasos_criticos_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pasos_criticos_1" name="bo_pasos_criticos" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_pasos_criticos == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pasos_criticos_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pasos_criticos_2" name="bo_pasos_criticos" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_pasos_criticos == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_riesgo_q_a">Riesgo Qco. y Anestésico<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_riesgo_q_a_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_riesgo_q_a_1" name="bo_riesgo_q_a" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_riesgo_q_a == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_riesgo_q_a_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_riesgo_q_a_2" name="bo_riesgo_q_a" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_riesgo_q_a == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="tm_aprox_cirugia">Tiempo Aprox. de Cirugía<span style="color:#FF0000";>*</span></label>
                            <input type="time" class="form-control" id="tm_aprox_cirugia" name="tm_aprox_cirugia" value="{{$solicitudPabellon->pausaSeguridad ? $solicitudPabellon->pausaSeguridad->tm_aprox_cirugia : ''}}" required>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_equipo_anestesia">Eq. Anestesia<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_equipo_anestesia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_equipo_anestesia_1" name="bo_equipo_anestesia" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_equipo_anestesia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_equipo_anestesia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_equipo_anestesia_2" name="bo_equipo_anestesia" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_equipo_anestesia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_oximetro">Oximetro Puesto y funcionando<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_oximetro_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_oximetro_1" name="bo_oximetro" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_oximetro == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_oximetro_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_oximetro_2" name="bo_oximetro" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_oximetro == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_bandeja_revisada">Bandeja revisada por Anest.<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_bandeja_revisada_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_bandeja_revisada_1" name="bo_bandeja_revisada" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_bandeja_revisada == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_bandeja_revisada_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_bandeja_revisada_2" name="bo_bandeja_revisada" value="0" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_bandeja_revisada == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_instrumental_esteril">Instrumental Estéril e Insumos<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_instrumental_esteril_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_instrumental_esteril_1" name="bo_instrumental_esteril" value="1" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_instrumental_esteril == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_instrumental_esteril_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_instrumental_esteril_2" name="bo_instrumental_esteril" value="0" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_instrumental_esteril == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_disponibilidad_sangre">Disponibilidad de Sangre<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_disponibilidad_sangre_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_disponibilidad_sangre_1" name="bo_disponibilidad_sangre" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_disponibilidad_sangre == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_disponibilidad_sangre_2">N/A</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_disponibilidad_sangre_2" name="bo_disponibilidad_sangre" value="0" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_disponibilidad_sangre == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-7" style="background-color: whitesmoke">
                            <label for="gl_profilaxis_ete">Profilaxis ETE<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="gl_profilaxis_ete_1">Medidas Antiembólicas</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="gl_profilaxis_ete_1" name="gl_profilaxis_ete" value="Medidas Antiembólicas" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->gl_profilaxis_ete == "Medidas Antiembólicas" ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="gl_profilaxis_ete_2">Compresor Mecánico</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="gl_profilaxis_ete_2" name="gl_profilaxis_ete" value="Compresor Mecánico" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->gl_profilaxis_ete == "Compresor Mecánico" ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label for="gl_profilaxis_ete_3">Medidas Antiembólicas - Compresor Mecánico</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="gl_profilaxis_ete_3" name="gl_profilaxis_ete" value="Medidas Antiembólicas - Compresor Mecánico" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->gl_profilaxis_ete == "Medidas Antiembólicas - Compresor Mecánico" ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-1">
                                    <label for="gl_profilaxis_ete_4">N/A</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="gl_profilaxis_ete_4" name="gl_profilaxis_ete" value="No Aplica" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->gl_profilaxis_ete == "No Aplica" ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label for="bo_pausa_biopsia">Biopsia<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_pausa_biopsia_1">Diferida</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pausa_biopsia_1" name="bo_pausa_biopsia" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_pausa_biopsia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pausa_biopsia_2">Rápida</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pausa_biopsia_2" name="bo_pausa_biopsia" value="2" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_pausa_biopsia == 2 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pausa_biopsia_3">Múltiple</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pausa_biopsia_3" name="bo_pausa_biopsia" value="3" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_pausa_biopsia == 3 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pausa_biopsia_4">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pausa_biopsia_4" name="bo_pausa_biopsia" value="0" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_pausa_biopsia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_profilaxis_antibiotica">Profilaxis Antibiótica<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_profilaxis_antibiotica_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_profilaxis_antibiotica_1" name="bo_profilaxis_antibiotica" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_profilaxis_antibiotica == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_profilaxis_antibiotica_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_profilaxis_antibiotica_2" name="bo_profilaxis_antibiotica" value="2" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_profilaxis_antibiotica == 2 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_profilaxis_antibiotica_3">N/A</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_profilaxis_antibiotica_3" name="bo_profilaxis_antibiotica" value="0" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_profilaxis_antibiotica == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_alergia">Alergias<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_alergia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_alergia_1" name="bo_alergia" value="1" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->bo_alergia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_alergia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_alergia_2" name="bo_alergia" value="0" {{!$solicitudPabellon->pausaSeguridad || $solicitudPabellon->pausaSeguridad->bo_alergia == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="gl_alergeno">Nombre del Alérgeno</label>
                            <input type="text" class="form-control" id="gl_alergeno" name="gl_alergeno" value="{{$solicitudPabellon->pausaSeguridad->gl_alergeno ?? ''}}">             
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="id_prevencion_caida">Prevención de caídas<span style="color:#FF0000";>*</span></label>
                            <select class="select2 select2-hidden-accessible" id="id_prevencion_caida" name="id_prevencion_caida" data-placeholder="Seleccione Prevención Caida" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                                <option value="">Seleccione Prevención Caida</option>
                                @foreach ($prevencionesCaida as $prevencionCaida)
                                    <option value="{{$prevencionCaida->id}}" {{$solicitudPabellon->pausaSeguridad && $solicitudPabellon->pausaSeguridad->id_prevencion_caida == $prevencionCaida->id ? "selected" : ""}}>{{$prevencionCaida->gl_nombre}}</option>
                                @endforeach
                            </select>                
                        </div>
                        <div class="col-sm-6">
                            <label for="gl_prevencion_upp">Prevención UPP<span style="color:#FF0000";>*</span></label>
                            <select class="select2 select2-hidden-accessible" id="gl_prevencion_upp" name="gl_prevencion_upp[]" multiple="" data-placeholder="Seleccione Prevención Caida" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                                @foreach ($prevencionesUPP as $prevencionUPP)
                                    <option value="{{$prevencionUPP->id}}" {{$solicitudPabellon->pausaSeguridad && in_array($prevencionUPP->id, explode(',', $solicitudPabellon->pausaSeguridad->gl_prevencion_upp)) ? "selected" : ""}}>{{$prevencionUPP->gl_nombre}}</option>
                                @endforeach
                            </select>                
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="tm_profilaxis_antibiotica">Tiempo de Profilaxis Antibiótica</label>
                            <input type="time" class="form-control" id="tm_profilaxis_antibiotica" name="tm_profilaxis_antibiotica" value="{{$solicitudPabellon->pausaSeguridad ? $solicitudPabellon->pausaSeguridad->tm_profilaxis_antibiotica : ''}}">             
                        </div>
                        <div class="col-sm-3">
                            <label for="tm_administracion">Hora de Administración<span style="color:#FF0000";>*</span></label>
                            <input type="time" class="form-control" id="tm_administracion" name="tm_administracion" value="{{$solicitudPabellon->pausaSeguridad ? $solicitudPabellon->pausaSeguridad->tm_administracion : ''}}" required>               
                        </div>
                        <div class="col-sm-2 offset-sm-1">
                            <br><a class="btn btn-lg btn-danger btn-xs pull-right" target="_blank" onclick="sinProfilaxis()" style="color: white">Sin Profilaxis Antibiótica</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="gl_tipo_antibiotico">Tipo Antibiótico<span style="color:#FF0000";>*</span></label>
                            <input type="text" class="form-control" id="gl_tipo_antibiotico" name="gl_tipo_antibiotico" value="{{$solicitudPabellon->pausaSeguridad->gl_tipo_antibiotico ?? ''}}" required>             
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    function sinProfilaxis(){
        $("#tm_profilaxis_antibiotica").val('00:00');
        $("#tm_administracion").val('00:00');
        $("#gl_tipo_antibiotico").val('Sin Profilaxis Antibiótica');
    }

    $('#id_prevencion_caida').select2();
    $('#gl_prevencion_upp').select2();
</script>
@stop