@extends('adminlte::page')

@section('title', 'Chequeo Pabellón')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Chequeo Pabellón</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaPabellonController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaPabellon->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-header"></div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="id_pabellonera">Pabellonera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_pabellonera" name="id_pabellonera" required>
                                <option value="">Seleccione Pabellonera</option>
                                @foreach ($pabelloneras as $pabellonera)
                                    <option value={{$pabellonera->id}} {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->id_pabellonera == $pabellonera->id ? "selected" : ""}}>{{$pabellonera->nombre}}</option>								
                                @endforeach
                            </select>       
                        </div>
                        <div class="col-sm-6">
                            <label for="id_enfermera">Enfermera<span style="color:#FF0000";>*</span></label>
                            <select class="form-control" id="id_enfermera" name="id_enfermera" required>
                                <option value="">Seleccione Enfermera</option>
                                @foreach ($enfermeras as $enfermera)
                                    <option value={{$enfermera->id}} {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->id_enfermera == $enfermera->id ? "selected" : ""}}>{{$enfermera->nombre}}</option>								
                                @endforeach
                            </select>  
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_mesa">Mesa Funcionando<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_mesa_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_mesa_1" name="bo_mesa" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_mesa == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_mesa_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_mesa_2" name="bo_mesa" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_mesa == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_electro_bisturi">Electro-bisturí revisado<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_electro_bisturi_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_electro_bisturi_1" name="bo_electro_bisturi" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_electro_bisturi == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_electro_bisturi_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_electro_bisturi_2" name="bo_electro_bisturi" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_electro_bisturi == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_lampara">Lámparas revisadas<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_lampara_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_lampara_1" name="bo_lampara" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_lampara == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_lampara_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_lampara_2" name="bo_lampara" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_lampara == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_laparoscopia">Eq. Laparoscopía revisado<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_laparoscopia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_laparoscopia_1" name="bo_laparoscopia" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_laparoscopia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_laparoscopia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_laparoscopia_2" name="bo_laparoscopia" value="0" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_laparoscopia == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_laparoscopia_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_laparoscopia_3" name="bo_laparoscopia" value="2" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_laparoscopia == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_pierneras">Pierneras<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_pierneras_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pierneras_1" name="bo_pierneras" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_pierneras == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pierneras_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pierneras_2" name="bo_pierneras" value="0" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_pierneras == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_pierneras_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pierneras_3" name="bo_pierneras" value="2" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_pierneras == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_pinia">Piñas revisadas<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_pinia_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pinia_1" name="bo_pinia" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_pinia == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_pinia_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pinia_2" name="bo_pinia" value="0" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_pinia == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_pinia_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_pinia_3" name="bo_pinia" value="2" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_pinia == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_tarimas">Tarimas<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_tarimas_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tarimas_1" name="bo_tarimas" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_tarimas == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_tarimas_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tarimas_2" name="bo_tarimas" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_tarimas == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_arco">Arco<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_arco_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_arco_1" name="bo_arco" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_arco == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_arco_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_arco_2" name="bo_arco" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_arco == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_aspiracion">Aspiración Funcionando<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_aspiracion_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aspiracion_1" name="bo_aspiracion" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_aspiracion == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_aspiracion_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_aspiracion_2" name="bo_aspiracion" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_aspiracion == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_instrumental">Instrumental<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_instrumental_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_instrumental_1" name="bo_instrumental" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_instrumental == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_instrumental_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_instrumental_2" name="bo_instrumental" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_instrumental == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_insumos">Insumos Solicitado y Recibido<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_insumos_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_insumos_1" name="bo_insumos" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_insumos == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_insumos_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_insumos_2" name="bo_insumos" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_insumos == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="background-color: whitesmoke">
                            <label for="bo_osito">Osito<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_osito_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_osito_1" name="bo_osito" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_osito == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_osito_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_osito_2" name="bo_osito" value="0" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_osito == 0 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-4">
                                    <label for="bo_osito_3">No Aplica</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_osito_3" name="bo_osito" value="2" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_osito == 2 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="gl_energia">Energía<span style="color:#FF0000";>*</span></label>
                            <select class="select2 select2-hidden-accessible" id="gl_energia" name="gl_energia[]" multiple="" data-placeholder="" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
                                <option value="Enseal" {{$solicitudPabellon->pausaPabellon && stristr($solicitudPabellon->pausaPabellon->gl_energia, "Enseal") ? "selected" : ""}}>Enseal</option>
                                <option value="Harmonico" {{$solicitudPabellon->pausaPabellon && stristr($solicitudPabellon->pausaPabellon->gl_energia, "Harmonico") ? "selected" : ""}}>Harmonico</option>
                                <option value="Lisagure" {{$solicitudPabellon->pausaPabellon && stristr($solicitudPabellon->pausaPabellon->gl_energia, "Lisagure") ? "selected" : ""}}>Lisagure</option>
                                <option value="No Amerita" {{$solicitudPabellon->pausaPabellon && stristr($solicitudPabellon->pausaPabellon->gl_energia, "No Amerita") ? "selected" : ""}}>No Amerita</option>
                            </select>                
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_compresor_neumatico">Compresor Neumático<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="bo_compresor_neumatico_1">Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_compresor_neumatico_1" name="bo_compresor_neumatico" value="1" {{$solicitudPabellon->pausaPabellon && $solicitudPabellon->pausaPabellon->bo_compresor_neumatico == 1 ? "checked" : ""}} required>
                                </div>
                                <div class="col-sm-2">
                                    <label for="bo_compresor_neumatico_2">No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_compresor_neumatico_2" name="bo_compresor_neumatico" value="0" {{!$solicitudPabellon->pausaPabellon || $solicitudPabellon->pausaPabellon->bo_compresor_neumatico == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="gl_observacion">Observaciones</label>
                            <input type="text" class="form-control" id="gl_observacion" name="gl_observacion" value="{{$solicitudPabellon->pausaPabellon->gl_observacion ?? ''}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    $('#gl_energia').select2();
</script>
@stop