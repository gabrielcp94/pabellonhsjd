@extends('adminlte::page')

@section('title', 'Pausa Epicrisis')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i>Pausa Epicrisis</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PausaEpicrisisController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->pausaEpicrisis->id ?? ''}}"/>
            <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{$solicitudPabellon->id}}"/>
            <input type="hidden" id="id_asignacion" name="id_asignacion" value="{{$solicitudPabellon->asignacion->id}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            @include('paciente.datos')
            <div class="card-body">
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="gl_morbido">Antecedentes mórbidos<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_morbido" name="gl_morbido" required>{{$solicitudPabellon->pausaEpicrisis->gl_morbido ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_quirurgico">Antecedentes quirúrgicos<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_quirurgico" name="gl_quirurgico" required>{{$solicitudPabellon->pausaEpicrisis->gl_quirurgico ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_medicamento_uso">Medicamentos uso habitual<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_medicamento_uso" name="gl_medicamento_uso" required>{{$solicitudPabellon->pausaEpicrisis->gl_medicamento_uso ?? ''}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="gl_diagnostico_pre">Diagnostico Pre<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_diagnostico_pre" name="gl_diagnostico_pre" required>{{$solicitudPabellon->pausaEpicrisis->gl_diagnostico_pre ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_diagnostico_post">Diagnostico Post<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_diagnostico_post" name="gl_diagnostico_post" required>{{$solicitudPabellon->pausaEpicrisis->gl_diagnostico_post ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_intervension">Cirugía practicada<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_intervension" name="gl_intervension" required>{{$solicitudPabellon->pausaEpicrisis->gl_intervension ?? ''}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="gl_tipo_anestesia">Tipo anestesia<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="pausaEpicrisis_1">General</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_aseo_realizado_1" name="gl_tipo_anestesia" value="General" {{$solicitudPabellon->pausaEpicrisis && $solicitudPabellon->pausaEpicrisis->gl_tipo_anestesia == "General" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="pausaEpicrisis_2">Local</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="pausaEpicrisis_2" name="gl_tipo_anestesia" value="Local" {{!$solicitudPabellon->pausaEpicrisis || $solicitudPabellon->pausaEpicrisis->gl_tipo_anestesia == "Local" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label for="pausaEpicrisis_3">Regional</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="pausaEpicrisis_3" name="gl_tipo_anestesia" value="Regional" {{!$solicitudPabellon->pausaEpicrisis || $solicitudPabellon->pausaEpicrisis->gl_tipo_anestesia == "Regional" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_incidente">Incidentes<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_incidente" name="gl_incidente" required>{{$solicitudPabellon->pausaEpicrisis->gl_incidente ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="id_cirujano">Cirujano<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_cirujano" name="id_cirujano">
                            <option value="">Seleccione Cirujano</option>
                            @foreach ($cirujanos as $cirujano)
                                <option value={{$cirujano->id}} {{isset($solicitudPabellon->pausaEpicrisis->id_cirujano) && $solicitudPabellon->pausaEpicrisis->id_cirujano == $cirujano->id ? 'selected' : ''}}>{{$cirujano->nombre}}</option>								
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="fc_entrada_recuperacion">Fecha ingreso<span style="color:#FF0000";>*</span></label>
                        <input type="datetime-local" class="form-control" id="fc_entrada_recuperacion" name="fc_entrada_recuperacion" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaEpicrisis->fc_entrada_recuperacion) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaEpicrisis->fc_entrada_recuperacion)) : ""}}">
                    </div>
                    <div class="col-sm-4">
                        <label for="fc_salida_recuperacion">Fecha egreso<span style="color:#FF0000";>*</span></label>
                        <input type="datetime-local" class="form-control" id="fc_salida_recuperacion" name="fc_salida_recuperacion" onblur="verificarFechas();" value="{{isset($solicitudPabellon->pausaEpicrisis->fc_salida_recuperacion) ? date("Y-m-d\TH:i:s", strtotime($solicitudPabellon->pausaEpicrisis->fc_salida_recuperacion)) : ""}}">
                    </div>
                    <div class="col-sm-4">
                        <label for="id_anestesista">Anestesista<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_anestesista" name="id_anestesista">
                            <option value="">Seleccione Anestesiologo</option>
                            @foreach ($anestesiologos as $anestesiologo)
                                <option value={{$anestesiologo->id}} {{isset($solicitudPabellon->pausaEpicrisis->id_anestesista) && $solicitudPabellon->pausaEpicrisis->id_anestesista == $anestesiologo->id ? 'selected' : ''}}>{{$anestesiologo->nombre}}</option>								
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label for="gl_ultimo_pa">P/A<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_ultimo_pa" name="gl_ultimo_pa" value="{{$solicitudPabellon->pausaEpicrisis->gl_ultimo_pa ?? ''}}">             
                    </div>
                    <div class="col-sm-2">
                        <label for="gl_ultimo_fc">Frecuencia cardiaca<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_ultimo_fc" name="gl_ultimo_fc" value="{{$solicitudPabellon->pausaEpicrisis->gl_ultimo_fc ?? ''}}">             
                    </div>
                    <div class="col-sm-2">
                        <label for="gl_ultimo_resp">Resp.<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_ultimo_resp" name="gl_ultimo_resp" value="{{$solicitudPabellon->pausaEpicrisis->gl_ultimo_resp ?? ''}}">             
                    </div>
                    <div class="col-sm-2">
                        <label for="gl_ultimo_hgt">HGT<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_ultimo_hgt" name="gl_ultimo_hgt" value="{{$solicitudPabellon->pausaEpicrisis->gl_ultimo_hgt ?? ''}}">             
                    </div>
                    <div class="col-sm-4">
                        <label for="id_documento">Documentos Entregados</label>
                        <select class="select2 select2-hidden-accessible" id="id_documento" name="id_documento[]" multiple="" data-placeholder="" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                            <option value="">Seleccione Documentos</option>
                            @foreach ($documentos as $documento)
                                <option value={{$documento->id}} {{$solicitudPabellon->pausaEpicrisis && in_array($documento->id, explode(',', $solicitudPabellon->pausaEpicrisis->id_documento)) ? "selected" : ""}}>{{$documento->tx_descripcion}}</option>								
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="gl_medicamento_administrado">Medicamentos Administrados<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_medicamento_administrado" name="gl_medicamento_administrado" required>{{$solicitudPabellon->pausaEpicrisis->gl_medicamento_administrado ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_indicacion">Indicaciones de Enfermeria<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_indicacion" name="gl_indicacion" required>{{$solicitudPabellon->pausaEpicrisis->gl_indicacion ?? ''}}</textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_observacion">Observaciones de Enfermeria</label>
                        <textarea type="text" class="form-control" id="gl_observacion" name="gl_observacion">{{$solicitudPabellon->pausaEpicrisis->gl_observacion ?? ''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    $('#id_documento').select2();
</script>
@stop