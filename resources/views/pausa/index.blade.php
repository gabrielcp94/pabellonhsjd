@extends('adminlte::page')

@section('title', 'Proceso Enfermería')

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Proceso Enfermería</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        {{-- <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="ID" value="{{request()->id}}">
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                            </div>
                        </form> --}}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Intervencion(es)</th>
                                    <th>Fecha de Intervencion</th>
                                    <th>Pabellón</th>
                                    <th width='15%'><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($solicitudesPabellon as $solicitudPabellon)
                                        <tr style="font-size:12px">
                                            <td>{{$solicitudPabellon->id}}</td>
                                            <td><a href={{url("paciente/".$solicitudPabellon->paciente->id)}}>{{$solicitudPabellon->paciente->nombre}}</a></td>
                                            <td>{{$solicitudPabellon->paciente->rut}}</td>
                                            <td>@foreach($solicitudPabellon->prestaciones as $prestacion)
                                                    {{$prestacion->prs_cod}} {{$prestacion->prs_desc}} <br>
                                                @endforeach <strong>{{$solicitudPabellon->gl_intevension}}</strong>
                                            </td>
                                            <td>{{$solicitudPabellon->fecha_intervencion}}</td>
                                            <td>{{$solicitudPabellon->pabellon->nombre}}</td>
                                            <td>
                                                {{$solicitudPabellon->id_pabellon == 1 ? 'Pre-Operatorio' : 'Ingreso de Enfermería'}}<br>
                                                @if (!isset($solicitudPabellon->pausaPre))
                                                    <a class="btn btn-success btn-xs" href={{url("pausaPre/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                @else
                                                    <form action="{{ route('pausaPre.destroy',$solicitudPabellon->pausaPre->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/pre/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaPre/".$solicitudPabellon->pausaPre->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                                Chequeo Pabellón<br>
                                                @if (!isset($solicitudPabellon->pausaPabellon))
                                                    <a class="btn btn-success btn-xs" href={{url("pausaPabellon/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                @else
                                                    <form action="{{ route('pausaPabellon.destroy',$solicitudPabellon->pausaPabellon->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/checkp/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaPabellon/".$solicitudPabellon->pausaPabellon->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                                Chequeo Anestesia<br>
                                                @if (!isset($solicitudPabellon->pausaAnestesia))
                                                    <a class="btn btn-success btn-xs" href={{url("pausaAnestesia/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                @else
                                                    <form action="{{ route('pausaAnestesia.destroy',$solicitudPabellon->pausaAnestesia->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/checka/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaAnestesia/".$solicitudPabellon->pausaAnestesia->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                                @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                    Pausa de Seguridad<br>
                                                    @if (!isset($solicitudPabellon->pausaSeguridad))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaSeguridad/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                    @else
                                                        <form action="{{ route('pausaSeguridad.destroy',$solicitudPabellon->pausaSeguridad->id) }}" method="POST">
                                                            <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/pausa/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                            @if (in_array(900, Session::get('opciones')))
                                                                <a class="btn btn-primary btn-xs" href={{url("pausaSeguridad/".$solicitudPabellon->pausaSeguridad->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                @endif
                                                @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaSeguridad))
                                                    Intra-Operatorio<br>
                                                    @if (!isset($solicitudPabellon->pausaIntra))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaIntra/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                    @else
                                                        <form action="{{ route('pausaIntra.destroy',$solicitudPabellon->pausaIntra->id) }}" method="POST">
                                                            <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/intra/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                            @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaIntra->bo_borrador))
                                                                <a class="btn btn-primary btn-xs" href={{url("pausaIntra/".$solicitudPabellon->pausaIntra->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                @endif
                                                @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                    Recuento<br>
                                                    @if (!isset($solicitudPabellon->pausaRecuento))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaRecuento/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                    @else
                                                        <form action="{{ route('pausaRecuento.destroy',$solicitudPabellon->pausaRecuento->id) }}" method="POST">
                                                            <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/recuento/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                            @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaRecuento->bo_borrador))
                                                                <a class="btn btn-primary btn-xs" href={{url("pausaRecuento/".$solicitudPabellon->pausaRecuento->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                @endif
                                                @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                    Epicrisis Enfermería<br>
                                                    @if (!isset($solicitudPabellon->pausaEpicrisis))
                                                            <a class="btn btn-success btn-xs" href={{url("pausaEpicrisis/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a><br>
                                                    @else
                                                        <form action="{{ route('pausaEpicrisis.destroy',$solicitudPabellon->pausaEpicrisis->id) }}" method="POST">
                                                            <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/epicrisis/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                            @if (in_array(900, Session::get('opciones')))
                                                                <a class="btn btn-primary btn-xs" href={{url("pausaEpicrisis/".$solicitudPabellon->pausaEpicrisis->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $solicitudesPabellon->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop