@extends('adminlte::page')

@section('title', 'Consentimiento Informado')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i> Consentimiento Informado</h1>
@stop

@section('content')
    <div class="card">
        <!-- form start -->
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ConsentimientoController@store')}}">
        {{ csrf_field() }}
        <input type="hidden" id="id" name="id" value="{{isset($solicitudPabellon->pausaPre) ? $solicitudPabellon->pausaPabellon->id : ''}}"/>
        <input type="hidden" id="id_solicitud" name="id_solicitud" value="{{isset($solicitudPabellon) ? $solicitudPabellon->id : ''}}"/>
        <div class="card-header">
            <h5 class="card-title">
                “Es el proceso en que el profesional de salud del Hospital San Juan de Dios, que atiende al paciente, entrega personalmente a éste o a su representante, la información suficiente, razonada y comprensible sobre su diagnóstico, los objetivos de la intervención que se propone, sus características, beneficios y riesgos, los medios de los que dispone el Hospital, así como las alternativas a los procedimientos o intervenciones que se le ofrece con sus beneficios y riesgos, a fin de que el o la paciente, o su representante en su caso, razonadamente autorice o rechace la propuesta de atención médica.”
                <br><strong>Escribir personalmente por el profesional responsable con letra imprenta y legible.</strong>
                <br>En caso de pacientes con dificultad de comprensión, alteración de conciencia o menores de edad, se deberá pedir el Consentimiento Informado a sus representantes.
            </h5>
        </div>
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
        </div>
        @include('paciente.datos')
        <hr>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="bo_imposibilitado">Paciente imposibilitado de otorgar C.I.:</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="bo_imposibilitado_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_imposibilitado_1" name="bo_imposibilitado" value="1" required>
                            </div>
                            <div class="col-sm-2">
                                <label for="bo_imposibilitado_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_imposibilitado_2" name="bo_imposibilitado" value="0" checked>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="bo_menor">Paciente menor, C.I. otorgado por sus representantes.:</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="bo_menor_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_menor_1" name="bo_menor" value="1" required>
                            </div>
                            <div class="col-sm-2">
                                <label for="bo_menor_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_menor_2" name="bo_menor" value="0" checked>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="gl_nombre_representante">Nombre del representante legal:</label>
                        <input type="text" class="form-control" id="gl_nombre_representante" name="gl_nombre_representante">
                    </div>
                    <div class="col-sm-3">
                        <label for="gl_rut_representante">Rut:</label>
                        <input type="text" class="form-control" id="gl_rut_representante" name="gl_rut_representante">
                    </div>
                    <div class="col-sm-3">
                        <label for="gl_telefono_representante">Telefono:</label>
                        <input type="text" class="form-control" id="gl_telefono_representante" name="gl_telefono_representante">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="gl_domicilio_representante">Dirección:</label>
                        <input type="text" class="form-control" id="gl_domicilio_representante" name="gl_domicilio_representante">
                    </div>
                    <div class="col-sm-6">
                        <label for="gl_parentesco_representante">Parentesco o cercanía:</label>
                        <input type="text" class="form-control" id="gl_parentesco_representante" name="gl_parentesco_representante">
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="card-body">

        <div class="form-group">
            <div class="row">
                <div class="col-sm-12">
                    <label>Información que entrega el Profesional:</label>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="gl_diagnostico">El diagnóstico médico del paciente es:<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_diagnostico" name="gl_diagnostico">
                    </div>
                    <div class="col-sm-6">
                        <label for="gl_procedimiento">Y que es necesario se efectúe al paciente, el o los siguientes (s) procedimientos (s) diagnóstico (s) , terapéutico (s) y/ o intervención quirúrgica:<span style="color:#FF0000";>*</span></label>
                        <input type="text" class="form-control" id="gl_procedimiento" name="gl_procedimiento">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="gl_objetivo">Objetivos y beneficios del procedimiento y/o intervención quirúrgica:<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_objetivo" name="gl_objetivo"></textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_riesgo">Riesgos del procedimiento y/o intervención quirúrgica:<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_riesgo" name="gl_riesgo"></textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="gl_alternativo">Alternativa (s) al procedimiento y/o intervención quirúrgica y sus respectivos riesgos y beneficios:<span style="color:#FF0000";>*</span></label>
                        <textarea type="text" class="form-control" id="gl_alternativo" name="gl_alternativo"></textarea>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="card-footer text-right">
            <button onclick="guardar()" type="submit" class="btn btn-info">Guardar</button>
        </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");
</script>
@stop