@extends('adminlte::page')

@section('title', 'Lista de Solicitudes de Pabellón')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Lista de Solicitudes de Pabellón</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="ID" value="{{request()->id}}">
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Intervencion(es)</th>
                                    <th>Medico</th>
                                    <th>Estado</th>
                                </thead>
                                <tbody>
                                    @foreach ($solicitudesPabellon as $solicitudPabellon)
                                        <tr style="font-size:12px">
                                            <td>{{$solicitudPabellon->id}} 
                                                @if (isset($solicitudPabellon->protocolo)) 
                                                    {{-- {{$solicitudPabellon->protocolo->id}} --}}
                                                @endif
                                            </td>
                                            <td><a href={{url("paciente/".$solicitudPabellon->id_paciente)}}>{{($solicitudPabellon->paciente->edad) ? $solicitudPabellon->paciente->nombre : $solicitudPabellon->paciente->gl_nombre}}<a></td>
                                            <td>{{isset($solicitudPabellon->paciente) ? $solicitudPabellon->paciente->rut : $solicitudPabellon->gl_rut}} <br> {{isset($solicitudPabellon->gl_rut) ? $solicitudPabellon->gl_rut : ''}}</td>
                                            <td>@foreach($solicitudPabellon->prestaciones as $prestacion)
                                                    {{$prestacion->prs_cod}} {{$prestacion->prs_desc}} <br>
                                                @endforeach <strong>{{$solicitudPabellon->gl_intevension}}</strong>
                                            </td>
                                            <td>{{$solicitudPabellon->medico->nombre}}</td>
                                            <td>
                                                @if($solicitudPabellon->bo_lista_espera == 1 && $solicitudPabellon->id_estado == 1 && $solicitudPabellon->id_jefe_equipo == 0)
                                                    <span class="badge {{$solicitudPabellon->estado->gl_class_laravel}}">Lista Espera</span>
                                                @else
                                                    <span class="badge {{$solicitudPabellon->estado->gl_class_laravel}}">{{$solicitudPabellon->estado->gl_nombre}}</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $solicitudesPabellon->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop