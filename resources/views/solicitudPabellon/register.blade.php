@extends('adminlte::page')

@section('title', 'Ingresar Solicitud de Pabellón')

@section('content_header')
    <h1><i class="ace-icon fa fa-user-md"></i> Solicitud de Intervención Quirúrgica</h1>
@stop

@section('content')
    <div class="card">
        <!-- form start -->
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('SolicitudPabellonController@store')}}">
        {{ csrf_field() }}
        <input type="hidden" id="id" name="id" value="{{$solicitudPabellon->id ?? ''}}"/>
        <input type="hidden" id="id_paciente" name="id_paciente" value="{{$solicitudPabellon->id_paciente ?? ''}}"/>
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
        </div>
        <div class="card-body">
            @include('paciente.formulario')
        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Datos Solicitud</strong></h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="id_equipo">Equipo Quirúrgico<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_equipo" name="id_equipo" required>
                            <option value="">Seleccione Equipo Quirúrgico</option>
                            @foreach ($equiposMedicos as $equipoMedico)
                                <option value={{$equipoMedico->id}} {{isset($solicitudPabellon) && $solicitudPabellon->id_equipo == $equipoMedico->id ? "selected" : ""}}>{{$equipoMedico->gl_descripcion}}</option>								
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 offset-sm-1">
                        <label for="bo_auge">Ges<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_auge_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_auge_1" name="bo_auge" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_auge == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_auge_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_auge_2" name="bo_auge" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_auge == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2" style="background-color: whitesmoke">
                        <label for="bo_cama_critica">Cama Critica<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_cama_critica_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cama_critica_1" name="bo_cama_critica" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_cama_critica == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_cama_critica_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cama_critica_2" name="bo_cama_critica" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_cama_critica == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="bo_cancer">Cáncer<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_cancer_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cancer_1" name="bo_cancer" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_cancer == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_cancer_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_cancer_2" name="bo_cancer" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_cancer == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2" style="background-color: whitesmoke">
                        <label for="bo_latex">Alergia al Látex<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_latex_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_latex_1" name="bo_latex" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_latex == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_latex_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_latex_2" name="bo_latex" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_latex == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="id_especialidad_aa">Especialidad Medica<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_especialidad_aa" name="id_especialidad_aa" required>
                            <option value="">Seleccione Especialidad</option>
                            @foreach ($especialidades as $especialidad)
                                <option value={{$especialidad->id}} {{isset($solicitudPabellon) && $solicitudPabellon->id_especialidad_aa == $especialidad->id ? "selected" : ""}}>{{$especialidad->gl_nombre}}</option>								
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3 offset-sm-6">
                        <label for="bo_re_intervencion">Re-intervención no programada<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_re_intervencion_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_re_intervencion_1" name="bo_re_intervencion" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_re_intervencion == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_re_intervencion_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_re_intervencion_2" name="bo_re_intervencion" value="0" {{!isset($solicitudPabellon) || $solicitudPabellon->bo_re_intervencion == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5">
                        <label for="gl_tipo_intervencion">Tipo Intervención<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="gl_tipo_intervencion_1">Programada</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_intervencion_1" name="gl_tipo_intervencion" value="Programada" {{!isset($solicitudPabellon) || $solicitudPabellon->gl_tipo_intervencion == "Programada" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="gl_tipo_intervencion_2">Urgencia</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_intervencion_2" name="gl_tipo_intervencion" value="Urgencia" {{isset($solicitudPabellon) && $solicitudPabellon->gl_tipo_intervencion == "Urgencia" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label for="gl_tipo_intervencion_3">Agregada</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_tipo_intervencion_3" name="gl_tipo_intervencion" value="Agregada" {{isset($solicitudPabellon) && $solicitudPabellon->gl_tipo_intervencion == "Agregada" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 offset-sm-4" style="background-color: whitesmoke">
                        <label for="bo_hipertermia">Antecedente y/o Riesgo Hipertermia Maligna<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_hipertermia_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_hipertermia_1" name="bo_hipertermia" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_hipertermia == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_hipertermia_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_hipertermia_2" name="bo_hipertermia" value="0" {{!isset($solicitudPabellon) || $solicitudPabellon->bo_hipertermia == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12" style="background-color: whitesmoke">
                        <label for="id_tipo_atencion">Origen Solicitud<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            @foreach ($tiposAtencion as $tipoAtencion)
                                <div class="col-sm-2">
                                    <label for="id_tipo_atencion{{$tipoAtencion->id}}">{{$tipoAtencion->gl_descripcion}}</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="id_tipo_atencion{{$tipoAtencion->id}}" name="id_tipo_atencion" value="{{$tipoAtencion->id}}" {{isset($solicitudPabellon) && $solicitudPabellon->id_tipo_atencion == $tipoAtencion->id ? "checked" : ""}} onchange="llenarUnidadDemandante((this.value))" required>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="id_unidad">Unidad Demandante<span style="color:#FF0000";>*</span></label>
                        <select class="form-control select_unidades" id="id_unidad" name="id_unidad" disabled>
                        </select>
                    </div>
                    <div class="col-sm-3 offset-sm-2">
                        <label for="gl_sala">Sala</label>
                        <input type="text" class="form-control" id="gl_sala" name="gl_sala" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_sala : ''}}" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="gl_cama">Cama</label>
                        <input type="text" class="form-control" id="gl_cama" name="gl_cama" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_cama : ''}}" disabled>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="id_pabellon">Pabellón<span style="color:#FF0000";>*</span></label>
                        <select class="form-control" id="id_pabellon" name="id_pabellon" required>
                            <option value="">Seleccione Pabellón</option>
                            @foreach ($pabellones as $pabellon)
                                <option value={{$pabellon->id}} {{isset($solicitudPabellon) && $solicitudPabellon->id_pabellon == $pabellon->id ? "selected" : ""}}>{{$pabellon->nombre}}</option>								
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3 offset-sm-6">
                        <label for="bo_estudio">Estudio Pre-Quirúrgico Completo<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_estudio_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_estudio_1" name="bo_estudio" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_estudio == 1 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_estudio_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_estudio_2" name="bo_estudio" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_estudio == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="bo_lista_espera">Fecha Cirugía<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="bo_lista_espera_1">Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_lista_espera_1" name="bo_lista_espera" value="0" onchange="listaEspera($(this).val());" {{isset($solicitudPabellon) && $solicitudPabellon->bo_lista_espera == 0 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-3">
                                <label for="bo_lista_espera_2">No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_lista_espera_2" name="bo_lista_espera" value="1" onchange="listaEspera($(this).val());" {{isset($solicitudPabellon) && $solicitudPabellon->bo_lista_espera == 1 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 offset-sm-1">
                        <label for="fc_intervencion">Fecha Intervención<span style="color:#FF0000";>*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="date" class="form-control" id="fc_intervencion" name="fc_intervencion" value="{{isset($solicitudPabellon) ? $solicitudPabellon->fc_intervencion : ''}}" disabled>
                        </div>	
                    </div>
                    <div class="col-sm-3">
                        <label for="tm_estimado">Tiempo Estimado<span style="color:#FF0000";>*</span></label>
                        <input type="time" class="form-control" id="tm_estimado" name="tm_estimado" value="{{isset($solicitudPabellon) ? $solicitudPabellon->tm_estimado : ''}}" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="nr_orden">Orden en Tabla<span style="color:#FF0000";>*</span></label>
                        <input type="number" class="form-control" id="nr_orden" name="nr_orden" min=1 value="{{isset($solicitudPabellon) ? $solicitudPabellon->nr_orden : ''}}" readonly>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Intervención</strong></h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="id_diagnostico_1">Diag. Pre-Operatorio (CIE10)<span style="color:#FF0000";>*</span></label>
                        <select class="form-control select_diagnosticos" id="id_diagnostico_1" name="id_diagnostico_1" required>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="gl_diagnostico_2">Diag. Pre-Operatorio (Texto)</label>
                        <input type="text" class="form-control" id="gl_diagnostico_2" name="gl_diagnostico_2" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_diagnostico_2 : ''}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="intervencion">Intervencion(es)<span style="color:#FF0000";>*</span></label>
                        <select class="form-control select_intervenciones" id="intervencion" name="intervencion[]" multiple required>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="gl_intervencion">Intervención Propuesta (Texto)</label>
                        <input type="text" class="form-control" id="gl_intervencion" name="gl_intervencion" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_intevension : ''}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12" style="background-color: whitesmoke">
                        <div class="row">
                            <div class="col-sm-1">
                                <label for="gl_lado">Lado<span style="color:#FF0000";>*</span></label>
                            </div>
                            <div class="col-sm-1 offset-sm-1">
                                <label for="gl_lado_1">Izquierdo</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_1" name="gl_lado" value="Izquierdo" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Izquierdo" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_2">Derecho</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_2" name="gl_lado" value="Derecho" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Derecho" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_3">Central</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_3" name="gl_lado" value="Central" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Central" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_4">Bilateral</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_4" name="gl_lado" value="Bilateral" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Bilateral" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_5">No aplica</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_5" name="gl_lado" value="No aplica" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "No aplica" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label for="id_medico">Medico Solicitante<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-4">
                    <select class="form-control select_medicos" id="id_medico" name="id_medico" required>
                    </select>
                </div>
            </div>

            {{-- <div class="row form-group">
                <div class="col-sm-3">
                    <label for="id_diagnostico_1">Diag. Pre-Operatorio (CIE10)<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_diagnosticos" id="id_diagnostico_1" name="id_diagnostico_1" required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="gl_diagnostico_2">Diag. Pre-Operatorio (Texto)</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="gl_diagnostico_2" name="gl_diagnostico_2" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_diagnostico_2 : ''}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="intervencion">Intervencion(es)<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_intervenciones" id="intervencion" name="intervencion[]" multiple required>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="gl_intervencion">Intervención Propuesta (Texto)</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="gl_intervencion" name="gl_intervencion" value="{{isset($solicitudPabellon) ? $solicitudPabellon->gl_intevension : ''}}">
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">
                                <label for="gl_lado">Lado<span style="color:#FF0000";>*</span></label>
                            </div>
                            <div class="col-sm-1 offset-sm-1">
                                <label for="gl_lado_1">Izquierdo</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_1" name="gl_lado" value="Izquierdo" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Izquierdo" ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_2">Derecho</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_2" name="gl_lado" value="Derecho" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Derecho" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_3">Central</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_3" name="gl_lado" value="Central" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Central" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_4">Bilateral</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_4" name="gl_lado" value="Bilateral" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "Bilateral" ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-1">
                                <label for="gl_lado_5">No aplica</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="gl_lado_5" name="gl_lado" value="No aplica" {{isset($solicitudPabellon) && $solicitudPabellon->gl_lado == "No aplica" ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="id_medico">Medico Solicitante<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-9">
                    <select class="form-control select_medicos" id="id_medico" name="id_medico" required>
                    </select>
                </div>
            </div> --}}
        </div>

        <div class="card-header">
            <h3 style="color:green" class="card-title"><strong>Personal de Apoyo</strong></h3>
        </div>
        <div class="card-body">
            <div class="row form-group">
                <div class="col-sm-1">
                    <label for="personal">Personal<span style="color:#FF0000";>*</span></label>
                </div>
                <div class="col-sm-11">
                    <select class="form-control select_equipos" id="equipos" name="equipos[]" onchange="personal();" multiple required>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="gl_riesgo_operatorio">Riesgo Operatorio<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_1">Grave</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_1" name="gl_riesgo_operatorio" value="Grave" {{isset($solicitudPabellon) && $solicitudPabellon->gl_riesgo_operatorio == "Grave" ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_2">Mediano</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_2" name="gl_riesgo_operatorio" value="Mediano" {{isset($solicitudPabellon) && $solicitudPabellon->gl_riesgo_operatorio == "Mediano" ? "checked" : ""}}>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_riesgo_operatorio_3">Leve</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_riesgo_operatorio_3" name="gl_riesgo_operatorio" value="Leve" {{isset($solicitudPabellon) && $solicitudPabellon->gl_riesgo_operatorio == "Leve" ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 offset-sm-2" style="background-color: whitesmoke">
                    <label for="gl_tipo_anestesia">Tipo Anestesia<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_1">General</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_1" name="gl_tipo_anestesia" value="General" {{isset($solicitudPabellon) && $solicitudPabellon->gl_tipo_anestesia == "General" ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_2">Local</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_2" name="gl_tipo_anestesia" value="Local" {{isset($solicitudPabellon) && $solicitudPabellon->gl_tipo_anestesia == "Local" ? "checked" : ""}}>
                        </div>
                        <div class="col-sm-3">
                            <label for="gl_tipo_anestesia_3">Regional</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="gl_tipo_anestesia_3" name="gl_tipo_anestesia" value="Regional" {{isset($solicitudPabellon) && $solicitudPabellon->gl_tipo_anestesia == "Regional" ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2" style="background-color: whitesmoke">
                    <label for="bo_sangre">Sangre<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_sangre_1">Sí</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="bo_sangre_1" name="bo_sangre" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_sangre == 1 ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_sangre_2">No</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="bo_sangre_2" name="bo_sangre" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_sangre == 0 ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 offset-sm-4">
                    <label for="bo_radiologia">Radiología<span style="color:#FF0000";>*</span></label>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_radiologia_1">Sí</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="bo_radiologia_1" name="bo_radiologia" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_radiologia == 1 ? "checked" : ""}} required>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_radiologia_2">No</label>
                        </div>
                        <div class="col-sm-1">
                            <input class="form-check-input" type="radio" id="bo_radiologia_2" name="bo_radiologia" value="0" {{isset($solicitudPabellon) && $solicitudPabellon->bo_radiologia == 0 ? "checked" : ""}}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    <label for="gl_observacion">Requerimientos Especiales</label>
                </div>
                <div class="col-sm-8">
                    <textarea type="text" class="form-control" id="gl_observacion" name="gl_observacion">{{isset($solicitudPabellon) ? $solicitudPabellon->gl_observacion : ''}}</textarea>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer text-right">
            <button onclick="guardar()" type="submit" class="btn btn-info">Ingresar Solicitud</button>
        </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    var url_unidad_demandante = @json(url('getUnidadDemandante/'));
    var url_diagnosticos = @json(url('getDiagnostico'));
    var url_medicos = @json(url('getMedico'));
    var url_intervenciones = @json(url('getPrestacion'));
    var url_equipos = @json(url('getEquipo'));

    function guardar(){
        $("#id_sexo").css('display','').attr('disabled', false);
        $("#fc_nacimiento").css('display','').attr('disabled', false);
        $("#id_prevision").css('display','').attr('disabled', false);
        $var = $( "form" ).serialize();
        $.getJSON("{{action('SolicitudPabellonController@validador')}}?"+$var,
			function(data){
                if(data.ok == 0){
                    alert(data.alerta);
                    stop();
                }
            })
    }

    function getPerson(rut, id_tipo_identificacion_paciente, ficha){
        if(id_tipo_identificacion_paciente == 5 && ficha){
            rut = ficha;
        }
        $.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut+"&id_tipo_identificacion="+id_tipo_identificacion_paciente,
			function(data){
                if(data.encontrado == true){
                    $("#id_paciente").val(data.id_paciente);
                    if(id_tipo_identificacion_paciente != 5){
                        if(data.id_tipo_dentificacion_paciente){
                            $("#id_tipo_identificacion_paciente").val(data.id_tipo_dentificacion_paciente);
                        }
                        $("#rut").val(data.rut);
                    }
                    if(data.nr_ficha){
                        $("#nr_ficha").val(data.nr_ficha);
                    }
                    $("#id_sexo").val(data.id_sexo);
                    $("#tx_nombre").val(data.tx_nombre);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
                    $("#fc_nacimiento").val(data.fc_nacimiento);
                    $("#id_prevision").val(data.id_prevision);
                    $("#id_clasificacion_fonasa").val(data.id_clasificacion_fonasa);
                    if(data.tx_direccion != null && data.tx_direccion != ' '){
                        $("#tx_direccion").val(data.tx_direccion);
                    }
                    if(data.cdgComuna != null && data.cdgComuna != ' '){
                        $("#id_comuna").val(data.cdgComuna);
                    }
                    if(data.tx_telefono != null){
                        $("#tx_telefono").val(data.tx_telefono);
                    }
                    bloqueoFonasa(data.id_clasificacion_fonasa);
                }else{
                    if(id_tipo_identificacion_paciente < 3){
                        alert('Paciente no encontrado');
                    }
                }
            })
	}

    function bloqueoRut(id_tipo_identificacion_paciente){
        if(id_tipo_identificacion_paciente > 2){
            $("#rut").css('display','').attr('disabled', true);
        }else{
            $("#rut").css('display','').attr('disabled', false);
            $("#rut").css('display','').attr('required', true);
        }
        if(id_tipo_identificacion_paciente == 5){
            $("#nr_ficha").css('display','').attr('required', true);
        }else{
            $("#nr_ficha").css('display','').attr('required', false);
        }
    }

    function bloqueoFonasa(id_prevision){
        if(id_prevision != 1){
            $("#id_clasificacion_fonasa").css('display','').attr('disabled', true);
        }else{
            $("#id_clasificacion_fonasa").css('display','').attr('disabled', false);
        }
    }

    function llenarUnidadDemandante(id_origen){
        $("#id_unidad").css('display','').attr('disabled', false);
        $("#id_unidad").css('display','').attr('required', true);
        $("#gl_sala").css('display','').attr('disabled', false);
        $("#gl_cama").css('display','').attr('disabled', false);
        $('.select_unidades').select2({
            width: '99%',
            allowClear: true,
            minimumInputLength: 0,
            placeholder: "Seleccione Unidad",
            ajax: {
                url: url_unidad_demandante+'/'+id_origen,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                }
                
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    }

    function listaEspera(bo){
        if(bo == 0){
            $("#fc_intervencion").css('display','').attr('disabled', false);
            $("#fc_intervencion").css('display','').attr('required', true);
            $("#tm_estimado").css('display','').attr('disabled', false);
            $("#tm_estimado").css('display','').attr('required', true);
            $("#nr_orden").css('display','').attr('readonly', false);
            $("#nr_orden").css('display','').attr('required', true);
        }else{
            $("#fc_intervencion").css('display','').attr('disabled', true);
            $("#fc_intervencion").css('display','').attr('required', false);
            $("#tm_estimado").css('display','').attr('disabled', true);
            $("#tm_estimado").css('display','').attr('required', false);
            $("#nr_orden").css('display','').attr('readonly', true);
            $("#nr_orden").css('display','').attr('required', false);
        }
    }

    // function nuevoApoyo(){
    //     var div = $(".equipo"); 

    //     //find all select2 and destroy them   
    //     div.find(".select_equipos").each(function(index)
    //     {
    //         if ($(this).data('select2')) {
    //             $(this).select2('destroy');
    //         } 
    //     });
    //     div.find(".select_labores").each(function(index)
    //     {
    //         if ($(this).data('select2')) {
    //             $(this).select2('destroy');
    //         } 
    //     });

    //     //Now clone you select2 div 
    //     $('.equipo:last').clone( true).insertAfter(".equipo:last"); 
    //     $('.apoyoEliminar:last').removeClass("hidden");

    //     //we must have to re-initialize  select2 
    //     $('.select_equipos:last').val(null).trigger('change');
    //     $('.select_equipos').select2({
    //         width: '99%',
    //         allowClear: true,
    //         minimumInputLength: 1,
    //         placeholder: "Ingrese Nombre o Rut",
    //         language: {
    //             inputTooShort: function() {
    //                 return 'Ingrese 1 o más caracteres para la búsqueda';
    //             }
    //         },
    //         ajax: {
    //             url: url_equipos,
    //             // dataType: 'json',
    //             data: function (params) {
    //                 var query = {
    //                     search: params.term,
    //                     type: 'public'
    //                 }
    //                 // Query parameters will be ?search=[term]&type=public
    //                 return query;
    //             }
    //         }
    //     });
    //     $('.select_labores:last').val(null).trigger('change');
    //     $('.select_labores').select2({
    //         width: '99%',
    //         allowClear: true,
    //         minimumInputLength: 1,
    //         placeholder: "",
    //         language: {
    //             inputTooShort: function() {
    //                 return 'Primero debe ingresar nombre o rut del personal';
    //             }
    //         }
    //     });
	// }

    // // elimina fila nuevo personal
    // $('.close-div-equipo').on('click', function(){
    //     $(this).closest("#equipo").remove();
    // });

    // function llenarLabores(id_medico){
    //     $('.select_labores').select2({
    //         width: '99%',
    //         allowClear: true,
    //         minimumInputLength: 0,
    //         placeholder: "Seleccione Labor",
    //         ajax: {
    //             url: url_labores+'/'+id_medico,
    //             dataType: 'json',
    //             data: function (params) {
    //                 var query = {
    //                     search: params.term,
    //                     type: 'public'
    //                 }
    //                 return query;
    //             }
    //         }
    //     });
    // }
    

    $('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    $('.select_medicos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 0,
        placeholder: "Seleccione Medico Solicitante",
        language: {
            inputTooShort: function() {
                return '';
            }
        },
        ajax: {
            url: url_medicos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    $('.select_intervenciones').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Código",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_intervenciones,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

    // setInterval('personal(5)', 3000);
    personal();

    function personal(){
        // $var = '&equipos%5B%5D=9217-6';
        $var = '';
        $x = $("#equipos").val();
        $x.forEach($personal => {
            $var = $var+'&equipos%5B%5D='+$personal;
        });

        $(".select_equipos").select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Rut",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_equipos+'?'+$var,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })
        // // $var = $( "form" ).serialize();
        // $var = $link;
        // console.log(1);
        // console.log($link);
        // console.log(2);
        // console.log($var2);
        // console.log(3);
        // console.log($var);
        // console.log(url_equipos+'?'+$var);
        // console.log(url_equipos+'?'+$var2);
    }

    

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    @if (isset($solicitudPabellon))
        var solicitudPabellon = @json($solicitudPabellon);
        llenarUnidadDemandante(solicitudPabellon.id_tipo_atencion);
        // listaEspera(solicitudPabellon.bo_lista_espera);

        var id_unidad = [];
		var newOption1 = new Option('{{ $solicitudPabellon->unidad->gl_nombre}}', {{ $solicitudPabellon->unidad->id }}, true, true);
		id_unidad.push(newOption1);
        $('#id_unidad').append(id_unidad).trigger('change');

        @if (isset($solicitudPabellon->fc_intervencion) && $solicitudPabellon->fc_intervencion != '0000-00-00')
            listaEspera(0);
        @endif

        var id_prestacion = [];
        solicitudPabellon.prestaciones.forEach(element => {
            var newOption1 = new Option(element.prs_cod+' - '+element.prs_desc, element.prs_corr, true, true);
            id_prestacion.push(newOption1);
        });
        $('#intervencion').append(id_prestacion).trigger('change');

        var id_cie10 = [];
		var newOption1 = new Option('{{$solicitudPabellon->cie10->cd_cie10}} {{$solicitudPabellon->cie10->gl_glosa}}', {{ $solicitudPabellon->cie10->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_diagnostico_1').append(id_cie10).trigger('change');

        var id_medico = [];
		var newOption1 = new Option('{{ $solicitudPabellon->medico->nombre}}', {{ $solicitudPabellon->medico->pro_cod }}, true, true);
		id_medico.push(newOption1);
        $('#id_medico').append(id_medico).trigger('change');

        var id_equipo = [];
        solicitudPabellon.equipo.forEach(element => {
            var newOption1 = new Option(element.medico.pro_nombres+' '+element.medico.pro_apepat+' '+element.medico.pro_apemat+', '+element.labor.cod_des, element.id_new, true, true);
            id_equipo.push(newOption1);
        });
        $('#equipos').append(id_equipo).trigger('change');
	@endif
</script>
@stop