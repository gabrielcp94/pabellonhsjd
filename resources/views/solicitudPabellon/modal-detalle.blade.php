<div class="modal fade" id="modal-detalle" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><strong>Detalle Solicitud {{$solicitudPabellon->id}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card bg-gradient">
                    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="card-title">
                            <i class="fa fa-user"></i>
                            <strong>Paciente ({{$solicitudPabellon->paciente->id}})</strong>
                        </h3>
                        <div class="card-tools">
                            @if (in_array(900, Session::get('opciones')))
                                <a class="btn btn-primary btn-xs" href={{url("paciente/".$solicitudPabellon->paciente->id."/edit")}}><i class="fas fa-edit"></i></a>
                            @endif
                            <button type="button" class="btn btn btn-sm" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        @include('paciente.datos')
                    </div>
                </div>

                <div class="card bg-gradient">
                    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="card-title">
                            <i class="fa fa-file-medical"></i>
                            <strong>Solicitud de Intervencion ({{$solicitudPabellon->id}})</strong>
                        </h3>
                        <div class="card-tools">
                            <a class="btn btn-danger btn-xs" href={{url("solicitudPabellon/".$solicitudPabellon->id)}} target="_blank"><i class="fas fa-file-pdf"></i></a>
                            @if (in_array(900, Session::get('opciones')))
                                <a class="btn btn-primary btn-xs" href={{url("solicitudPabellon/".$solicitudPabellon->id."/edit")}}><i class="fas fa-edit"></i></a>
                            @endif
                            <button type="button" class="btn btn btn-sm" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        @include('solicitudPabellon.datos')
                    </div>
                </div>
                
                @if (isset($solicitudPabellon->protocolo))
                    <div class="card bg-gradient collapsed-card">
                        <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                <i class="fa fa-user-md"></i>
                                Protocolo ({{$solicitudPabellon->protocolo->id}})
                            </h3>
                            <div class="card-tools">
                                <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_protocolo/{{$solicitudPabellon->protocolo->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                @if (in_array(900, Session::get('opciones')))
                                    <a class="btn btn-primary btn-xs" href={{url("protocolo/".$solicitudPabellon->protocolo->id."/edit")}}><i class="fas fa-edit"></i></a>
                                    <a class="btn btn-danger btn-xs" href={{url("eliminarProtocolo?id=".$solicitudPabellon->protocolo->id)}}><i class="fas fa-trash"></i></a>
                                @endif
                                <button type="button" class="btn btn btn-sm" data-card-widget="collapse">
                                <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            @include('protocolo.datos')
                        </div>
                    </div>
                @endif
            
                @if ($solicitudPabellon->id_estado == 2 || $solicitudPabellon->id_estado == 3)
                    <div class="card bg-gradient collapsed-card">
                        <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="card-title">
                            <i class="fa fa-clipboard"></i>
                            Pausas
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn btn-sm" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                            </button>
                        </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Pausa</th>
                                            <th><i class="fas fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$solicitudPabellon->id_pabellon == 1 ? 'Pre-Operatorio' : 'Ingreso de Enfermería'}} 
                                                @if (isset($solicitudPabellon->pausaPre))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaPre))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaPre/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaPre.destroy',$solicitudPabellon->pausaPre->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/pre/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaPre/".$solicitudPabellon->pausaPre->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chequeo Pabellón
                                                @if (isset($solicitudPabellon->pausaPabellon))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaPabellon))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaPabellon/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaPabellon.destroy',$solicitudPabellon->pausaPabellon->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/checkp/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaPabellon/".$solicitudPabellon->pausaPabellon->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chequeo Anestesia
                                                @if (isset($solicitudPabellon->pausaAnestesia))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaAnestesia))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaAnestesia/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaAnestesia.destroy',$solicitudPabellon->pausaAnestesia->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/checka/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaAnestesia/".$solicitudPabellon->pausaAnestesia->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pausa de Seguridad
                                                @if (isset($solicitudPabellon->pausaSeguridad))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaSeguridad))
                                                    @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaSeguridad/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaSeguridad.destroy',$solicitudPabellon->pausaSeguridad->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/pausa/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaSeguridad/".$solicitudPabellon->pausaSeguridad->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Intra-Operatorio
                                                @if (isset($solicitudPabellon->pausaIntra))
                                                    @if ($solicitudPabellon->pausaIntra->bo_borrador == 0)
                                                        <i class="fas fa-check" style="color: green"></i></td>
                                                    @else
                                                        <i class="fas fa-exclamation" style="color: yellow"></i></td>
                                                    @endif
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaIntra))
                                                    @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaSeguridad))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaIntra/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaIntra.destroy',$solicitudPabellon->pausaIntra->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/intra/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaIntra->bo_borrador))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaIntra/".$solicitudPabellon->pausaIntra->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Recuento
                                                @if (isset($solicitudPabellon->pausaRecuento))
                                                    @if ($solicitudPabellon->pausaRecuento->bo_borrador == 0)
                                                        <i class="fas fa-check" style="color: green"></i></td>
                                                    @else
                                                        <i class="fas fa-exclamation" style="color: yellow"></i></td>
                                                    @endif
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaRecuento))
                                                    @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaRecuento/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaRecuento.destroy',$solicitudPabellon->pausaRecuento->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/recuento/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaRecuento->bo_borrador))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaRecuento/".$solicitudPabellon->pausaRecuento->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Recuperación
                                                @if (isset($solicitudPabellon->pausaRecuperacion))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaRecuperacion))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        {{-- <a class="button" href="pausaPre/create?id={{$solicitudPabellon->id}}"><i class="fas fa-edit" style="color: green"></i></a> --}}
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaRecuperacion.destroy',$solicitudPabellon->pausaRecuperacion->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/recuperacion/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            @if (isset($solicitudPabellon->pausaRecuperacion) && $solicitudPabellon->pausaRecuperacion->bo_borrador == 0)
                                                                <a class="btn btn-warning btn-xs" onclick="return confirm('¿Esta seguro de revertir el alta?');" href={{url("revertirAlta?id=".$solicitudPabellon->id)}} title="Revertir Alta"><i class="fas fa-procedures"></i></a>
                                                            @endif
                                                            {{-- <a class="button" href="pausaPre/{{$solicitudPabellon->pausaPre->id}}/edit"><i class="fas fa-edit"></i></a> --}}
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Recuperación Transitoria
                                                @if (isset($solicitudPabellon->pausaRecuperacionTransitoria))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaRecuperacionTransitoria))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        {{-- <a class="button" href="pausaPre/create?id={{$solicitudPabellon->id}}"><i class="fas fa-edit" style="color: green"></i></a> --}}
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaRecuperacion.destroy',$solicitudPabellon->pausaRecuperacionTransitoria->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/recuperacion/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}/3" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            {{-- @if (isset($solicitudPabellon->pausaRecuperacion) && $solicitudPabellon->pausaRecuperacion->bo_borrador == 0)
                                                                <a class="btn btn-warning btn-xs" onclick="return confirm('¿Esta seguro de revertir el alta?');" href="revertirAlta?id={{$solicitudPabellon->id}}" title="Revertir Alta"><i class="fas fa-procedures"></i></a>
                                                            @endif --}}
                                                            {{-- <a class="button" href="pausaPre/{{$solicitudPabellon->pausaPre->id}}/edit"><i class="fas fa-edit"></i></a> --}}
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Epicrisis Enfermería
                                                @if (isset($solicitudPabellon->pausaEpicrisis))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaEpicrisis))
                                                    @if (in_array(900, Session::get('opciones')) || (in_array(351, Session::get('opciones')) && $solicitudPabellon->pausaPre))
                                                        <a class="btn btn-success btn-xs" href={{url("pausaEpicrisis/create?id=".$solicitudPabellon->id)}}><i class="fas fa-edit"></i></a>
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaEpicrisis.destroy',$solicitudPabellon->pausaEpicrisis->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/epicrisis/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            <a class="btn btn-primary btn-xs" href={{url("pausaEpicrisis/".$solicitudPabellon->pausaEpicrisis->id."/edit")}}><i class="fas fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Traslado
                                                @if (isset($solicitudPabellon->pausaTraslado))
                                                    <i class="fas fa-check" style="color: green"></i></td>
                                                @else
                                                    <i class="fas fa-times" style="color: red"></i></td>
                                                @endif
                                            </td>
                                            <td>
                                                @if (!isset($solicitudPabellon->pausaTraslado))
                                                    @if (in_array(351, Session::get('opciones')))
                                                        {{-- <a class="button" href="pausaPre/create?id={{$solicitudPabellon->id}}"><i class="fas fa-edit" style="color: green"></i></a> --}}
                                                    @else
                                                        No tiene
                                                    @endif
                                                @else
                                                    <form action="{{ route('pausaTraslado.destroy',$solicitudPabellon->pausaTraslado->id) }}" method="POST">
                                                        <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_pausa/traslado/{{$solicitudPabellon->id}}/{{$solicitudPabellon->asignacion->id}}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                        @if (in_array(900, Session::get('opciones')))
                                                            {{-- @if (isset($solicitudPabellon->pausaRecuperacion) && $solicitudPabellon->pausaRecuperacion->bo_borrador == 0)
                                                                <a class="btn btn-warning btn-xs" onclick="return confirm('¿Esta seguro de revertir el alta?');" href="revertirAlta?id={{$solicitudPabellon->id}}" title="Revertir Alta"><i class="fas fa-procedures"></i></a>
                                                            @endif --}}
                                                            {{-- <a class="button" href="pausaPre/{{$solicitudPabellon->pausaPre->id}}/edit"><i class="fas fa-edit"></i></a> --}}
                                                            @csrf
                                                            @method('DELETE')
                                                            <button onclick="return confirm('¿Esta seguro de eliminar esta pausa?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                        @endif
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            
                @if ((isset($solicitudPabellon->solicitudesInsumos) && count($solicitudPabellon->solicitudesInsumos) > 0) || (isset($solicitudPabellon->solicitudesInsumosBodega) && count($solicitudPabellon->solicitudesInsumosBodega) > 0))
                    <div class="card bg-gradient collapsed-card">
                        <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="card-title">
                            <i class="fa fa-medkit"></i>
                            Insumos
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn btn-sm" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                            </button>
                        </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Solicitante</th>
                                            <th>Fecha</th>
                                            <th>Estado</th>
                                            <th><i class="fas fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($solicitudPabellon->solicitudesInsumos->sortBy('id') as $solicitudInsumo)
                                            <tr>
                                                <td>{{$solicitudInsumo->id}}</td>
                                                <td>{{$solicitudInsumo->solicitante->nombre}}</td>
                                                <td>{{date('d/m/Y', strtotime(str_replace("/",".",$solicitudInsumo->fc_solicitud)))}}</td>
                                                <td><span class="badge {{$solicitudInsumo->estado->gl_class_laravel}}">{{$solicitudInsumo->estado->gl_nombre}}</span></td>
                                                <td>
                                                    <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_insumo/{{$solicitudInsumo->id}}/1" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                    @if (in_array(914, Session::get('opciones')))   
                                                        <a class="btn btn-danger btn-xs" href={{url("solicitudInsumo/".$solicitudInsumo->id)}} target="_blank"><i class="fas fa-dollar-sign"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @foreach ($solicitudPabellon->solicitudesInsumosBodega->sortBy('id') as $solicitudInsumoBodega)
                                            <tr style="color: blue">
                                                <td>{{$solicitudInsumoBodega->id}}</td>
                                                <td>{{$solicitudInsumoBodega->gl_solicitante}}</td>
                                                <td>{{date('d/m/Y', strtotime(str_replace("/",".",$solicitudInsumoBodega->fc_solicitud)))}}</td>
                                                <td><span class="badge badge-info">Entrega Especial</span></td>
                                                <td>
                                                    <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_insumo/{{$solicitudInsumoBodega->id}}/0" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>