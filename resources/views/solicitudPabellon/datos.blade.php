<div class="card-body">
    @if(isset($solicitudPabellon->desvincular))
        <div class="row invoice-info">
            <div class="col-sm-12 invoice-col" style="color: red">
                <strong>Motivo de Suspención: </strong>{{$solicitudPabellon->desvincular->gl_motivo}}
            </div>
        </div>
    @endif
    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            <strong>Re-Intervención: </strong>{{$solicitudPabellon->re_intervencion}}<br>
            <strong>Unidad Demandante: </strong>{{$solicitudPabellon->unidad->gl_nombre}}<br>
            <strong>Fecha/Tiempo Estimado: </strong>{{date('d/m/Y', strtotime(str_replace("/",".",$solicitudPabellon->fc_intervencion)))}} {{$solicitudPabellon->tm_estimado}}<br>
            <strong>Medico Solicitante: </strong>{{$solicitudPabellon->medico->nombre}}<br>
            <strong>Tipo Intervención: </strong>{{$solicitudPabellon->gl_tipo_intervencion}}<br>
            <strong>Equipo Quirúrgico: </strong>{{$solicitudPabellon->equipoMedico->nombre}}<br>
        </div>
        <div class="col-sm-3 invoice-col">
            <strong>Lista Espera: </strong>{{$solicitudPabellon->lista_espera}}<br>
            <strong>Sala: </strong>{{$solicitudPabellon->gl_sala}}<br>
            <strong>Pabellón: </strong>{{$solicitudPabellon->auge}}<br>
            <strong>Lado: </strong>{{$solicitudPabellon->gl_lado}}<br>
            <strong>Sangre: </strong>{{$solicitudPabellon->sangre}}<br>
            <strong>Radiología: </strong>{{$solicitudPabellon->radiologia}}<br>
        </div>
        <div class="col-sm-3 invoice-col">
            <strong>AUGE: </strong>{{$solicitudPabellon->auge}}<br>
            <strong>Cama: </strong>{{$solicitudPabellon->gl_cama}}<br>
            <strong>Fecha Ingreso: </strong>{{date('d/m/Y', strtotime(str_replace("/",".",$solicitudPabellon->fc_ingreso)))}}<br>
            <strong>Cancer: </strong>{{$solicitudPabellon->cancer}}<br>
            <strong>Cama Critica: </strong>{{$solicitudPabellon->cama_critica}}<br>
            <strong>Alergia al Látex: </strong>{{$solicitudPabellon->latex}}<br>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            <strong>Diagnostico: </strong>{{$solicitudPabellon->gl_diagnostico_2}}<br>
        </div>
        <div class="col-sm-6 invoice-col">
            <strong>Cie10: </strong>{{$solicitudPabellon->cie10->gl_glosa}}<br>
        </div>
    </div>
    <strong>Requerimientos Especiales: </strong>{{$solicitudPabellon->gl_observacion}}<br>
    <strong style="color: red">{{$solicitudPabellon->bo_carga_masiva == 1 ? 'Carga Masiva' : ''}}</strong><br>
    <br>
    <H5 align="center"><strong>Intervenciones</strong></H5>
    <div class="col-12 table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Codigo Fonasa</th>
                    <th>Glosa Fonasa</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitudPabellon->prestaciones as $prestacion)
                    <tr>
                        <td>{{$prestacion->prs_cod}}</td>
                        <td>{{$prestacion->prs_desc}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <H5 align="center"><strong>Equipo Médico</strong></H5>
    <div class="col-12 table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Cargo</th>
                    <th>Nombre</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitudPabellon->equipo as $equipo)
                    <tr>
                        <td>{{$equipo->labor->cod_des}}</td>
                        <td>{{$equipo->medico->nombre}}</td>
                    </tr>
                @endforeach
                @if (isset($personal))
                    @foreach ($personal as $equipo)
                        <tr style="color: blue">
                            <td>{{$equipo->labor->cod_des}}</td>
                            <td>{{$equipo->medico->nombre}}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    @if (isset($solicitudPabellon->asignacion))
        <br>
        <H5 align="center"><strong>Datos de Asignación</strong></H5>
        @if($solicitudPabellon->asignacion->bo_condicional != 0)
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col" style="color: red">
                    <strong>Intervención Condicional por: </strong>{{$solicitudPabellon->asignacion->condicional->gl_nombre}}
                </div>
            </div>
        @endif
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>Sala Pabellón: </strong>{{$solicitudPabellon->asignacion->nrPabellon->gl_nombre}}<br>
                <strong>Inicio de Intervención: </strong>{{date('d/m/Y H:i:s', strtotime(str_replace("/",".",$solicitudPabellon->asignacion->fc_inicio)))}}<br>
            </div>
            <div class="col-sm-6 invoice-col">
                <strong>Tiempo Asignado: </strong>{{$solicitudPabellon->tm_estimado}}<br>
                <strong>Termino de Intervención: </strong>{{date('d/m/Y H:i:s', strtotime(str_replace("/",".",$solicitudPabellon->asignacion->fc_termino)))}}<br>
            </div>
        </div>
    @endif
</div>