@extends('adminlte::page')

@section('title', 'Lista de Solicitudes de Pabellón')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Lista de Solicitudes de Pabellón</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="ID" value="{{request()->id}}">
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                {{-- <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div> --}}
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <a href={{url("solicitudPabellonCorregir")}} class="btn btn-success" type="button" title="Corregir Solicitudes">Corregir</a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    {{$solicitudesPabellonSinIDPaciente}} de {{$solicitudesPabellonConIDPaciente}} <strong>({{round(($solicitudesPabellonSinIDPaciente*100)/$solicitudesPabellonConIDPaciente, 2)}}%)</strong> Solicitudes sin ID Paciente
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Ficha</th>
                                    <th>Fecha de Ingreso</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($solicitudesPabellon as $solicitudPabellon)
                                        <tr style="font-size:12px">
                                            <td>{{$solicitudPabellon->id}}</td>
                                            <td>{{$solicitudPabellon->nombre}}</td>
                                            <td>{{isset($solicitudPabellon->gl_rut) ? $solicitudPabellon->gl_rut : ''}} {{$solicitudPabellon->gl_tipo_identificacion}}</td>
                                            <td>{{$solicitudPabellon->fc_nacimiento}}</td>
                                            <td>{{$solicitudPabellon->nr_ficha}}</td>
                                            <td>{{$solicitudPabellon->fc_ingreso}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $solicitudesPabellon->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop