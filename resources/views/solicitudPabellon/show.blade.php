<html>
    <head>
        @include('PDF.style')
        <title>Solicitud de Pabellón</title>
    </head>
    <body>
        @include('PDF.encabezado')
        <h2 align="center">{{$solicitudPabellon->bo_lista_espera == 1 && $solicitudPabellon->fc_salida_espera == '0000-00-00 00:00:00' && $solicitudPabellon->id_estado == 1 ? 'Indicación de Intervención Quirúrgica' : 'Solicitud de Intervención Quirúrgica'}}</h2>
        @include('paciente.datosPDF')
        <table id="tabla">
            <tr>
                <th colspan="3" style="background: blanchedalmond">Intervención Requerida</th>
            </tr>
            <tr>
                <th>Pabellón</th>
                <th colspan="2">Equipo Médico</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->pabellon->nombre}}</td>
                <td colspan="2">{{$solicitudPabellon->equipoMedico->nombre}}</td>
            </tr>
            <tr>
                <th colspan="3">Diagnostico</th>
            </tr>
            <tr>
                <td colspan="3">{{$solicitudPabellon->cie10->gl_glosa}}</td>
            </tr>
            <tr>
                <th colspan="3">Intervención Propuesta</th>
            </tr>
            <tr>
                <td colspan="3">{!!$solicitudPabellon->prestaciones->pluck('nombre')->implode('<br>')!!}<br><strong>{{$solicitudPabellon->gl_intevension}}</strong></td>
            </tr>
            {{-- <tr>
                <th colspan="3">Intervención Propuesta (Texto)</th>
            </tr>
            <tr>
                <td colspan="3">{{$solicitudPabellon->gl_intevension}}</td>
            </tr> --}}
            <tr>
                <th>Lado</th>
                <th colspan="2">Cama Critica</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->gl_lado}}</td>
                <td colspan="2">{{$solicitudPabellon->bo_cama_critica == 1 ? 'Si' : 'No'}}</td>
            </tr>
            <tr>
                <th>Anestesia</th>
                <th>Sangre</th>
                <th>Radiología</th>
            </tr>
            <tr>
                <td>{{$solicitudPabellon->gl_tipo_anestesia}}</td>
                <td>{{$solicitudPabellon->bo_sangre == 1 ? 'Si' : 'No'}}</td>
                <td>{{$solicitudPabellon->bo_radiologia == 1 ? 'Si' : 'No'}}</td>
            </tr>
        </table>
        <br><br><br><br><br>
        <table>
            <tr>
                <td>Fecha Indicación IQX: {{$solicitudPabellon->fecha_ingreso}}<br>Fecha Solicitud IQX: {{$solicitudPabellon->fecha_solicitud}}</td>
                <td align="right">___________________<br>{{$solicitudPabellon->usuario->nombre}}<br>{{$solicitudPabellon->usuario->gl_rut}}</td>
            </tr>
        </table>
    </body>
</html>