@extends('adminlte::page')

@section('title', 'Vaciado Estadistica')

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Mis Referencias Odontologicas</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="fc_inicio">Fecha de Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_inicio" name="fc_inicio" value="{{isset(request()->fc_inicio) ? request()->fc_inicio : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-3">
                                    <label for="fc_termino">Fecha de Termino</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_termino" name="fc_termino" value="{{isset(request()->fc_termino) ? request()->fc_termino : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div align="right" class="col-sm-6">
                                    <a type="button" onclick="filtro($('#fc_inicio').val(), $('#fc_termino').val())" class="btn btn-info"><i style="color:white">Filtrar</i></a>
                                    <a type="button" onclick="excel($('#fc_inicio').val(), $('#fc_termino').val())" title="Exportar a Excel" class="btn btn-success"><i class="fa fa-file-excel" style="color:white"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                <label for="pabellon">Pabellon:</label>
                                    <select class="select2 select2-hidden-accessible" id="pabellones" name="pabellones[]" multiple="" data-placeholder="Seleccione pabellon(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        @foreach ($pabellones as $pabellon)
                                            <option value={{$pabellon->id}} >{{$pabellon->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="intervenciones">Intervención:</label>
                                        <select class="form-control select_intervenciones" id="intervenciones" name="intervenciones[]" multiple>
                                        </select>
                                </div>
                            </div>
                        </form>
                        @if (count($solicitudesPabellon) < 1)
                            <h2 class="text-center"> No se han encontrado registros. </h2>
                        @else
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead>
                                    <th width="5%">ID</th>
                                    <th width="5%">GES</th>
                                    <th width="10%">Rut</th>
                                    <th width="15%">Nombre</th>
                                    <th width="5%">Ficha</th>
                                    <th width="15%">Diagnostico</th>
                                    <th width="30%">Intervención</th>
                                    <th width="5%">Fecha</th>
                                    <th width="10%"><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($solicitudesPabellon as $solicitudPabellon)
                                        <tr role="row" class="odd">
                                            <td>{{$solicitudPabellon->protocolo->id}}</td>
                                            <td>{{$solicitudPabellon->protocolo->ges}}</td>
                                            <td>{{$solicitudPabellon->paciente->rut_sin_puntos}}</td>
                                            <td>{{$solicitudPabellon->paciente->nombre}}</td>
                                            <td>{{$solicitudPabellon->paciente->nr_ficha}}</td>
                                            <td><strong>{{$solicitudPabellon->protocolo->cie10post->cd_cie10}}</strong> {{$solicitudPabellon->protocolo->cie10post->gl_glosa}}</td>
                                            <td>@foreach ($solicitudPabellon->protocolo->prestaciones as $prestacion)
                                                    <strong>{{$prestacion->prs_cod}}</strong> {{$prestacion->prs_desc}} <br>
                                                @endforeach
                                            </td>
                                        <td>{{$solicitudPabellon->protocolo->fecha_intervencion}}</td>
                                            <td><a type="button" target="_blank" title="Ver Protocolo" class="btn btn-danger btn-xs"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                <a type="button" target="_blank" title="Editar Protocolo" class="btn btn-warning btn-xs"><i class="fa fa-edit" style="color:white"></i></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $solicitudesPabellon->appends(request()->query())->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    var url_intervenciones = @json(url('getPrestacionTodas'));

    function filtro(fc_inicio, fc_termino){
        if(fc_termino < fc_inicio ){
            alert('La fecha de termino debe ser despues de la fecha de inicio');
        }else{
            $var = $( "form" ).serialize();
            window.location.href = "vaciadoIntervenciones?"+$var;
        }
    }

    function excel(fc_inicio, fc_termino){
        if(fc_termino < fc_inicio ){
            alert('La fecha de termino debe ser despues de la fecha de inicio');
        }else{
            $var = $( "form" ).serialize();
        window.location.href = "vaciadoIntervencionesExcel?"+$var;
        }
    }

    @if(isset(request()->pabellones))
        var user = @json(request()->all());
        var pabellones = user.pabellones;
        $("#pabellones").val(pabellones).trigger('change');
    @endif

    @if(isset(request()->intervenciones))
        var intervenciones_array = [];
        @foreach (request()->intervenciones as $intervencion)
            var newOption1 = new Option('{{ $intervencion}}', {{ $intervencion }}, true, true);
            intervenciones_array.push(newOption1);
        @endforeach
        $('#intervenciones').append(intervenciones_array).trigger('change');
    @endif

    $('#pabellones').select2();

    $('#intervenciones').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Ingrese Nombre o Código",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_intervenciones,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })
</script>
@stop