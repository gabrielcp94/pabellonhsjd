<table id="tabla">
    <tr>
        <th colspan="3" id="tituloTabla">Información del Paciente</th>
    </tr>
    <tr>
        <th colspan="2">Nombre</th>
        <th>Fecha de Nacimiento</th>
    </tr>
    <tr>
        <td colspan="2">{{$solicitudPabellon->paciente->nombre}}</td>
        <td>{{$solicitudPabellon->paciente->fecha_nacimiento}} ({{$solicitudPabellon->paciente->edad}})</td>
    </tr>
    <tr>
        <th>Rut</th>
        <th>Ficha</th>
        <th>Telefono</th>
    </tr>
    <tr>
        <td>{{$solicitudPabellon->paciente->rut}}</td>
        <td>{{$solicitudPabellon->paciente->nr_ficha}}</td>
        <td>{{$solicitudPabellon->paciente->tx_telefono}}</td>
    </tr>
    <tr>
        <th colspan="3">Dirección</th>
    </tr>
    <tr>
        <td colspan="3">{{$solicitudPabellon->paciente->tx_direccion}}</td>
    </tr>
</table>
<br>