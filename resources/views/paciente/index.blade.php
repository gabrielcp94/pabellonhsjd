@extends('adminlte::page')

@section('title', 'Lista de Pacientes')

@section('content')
	<div class="card card-info">
		<div class="card-header">
		    <h3 class="card-title">Lista de Pacientes</h3>
		</div>
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <input type="text" name="tx_nombre" class="form-control" id="tx_nombre" placeholder="Nombre" value="{{request()->tx_nombre}}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="tx_apellido_paterno" class="form-control" id="tx_apellido_paterno" placeholder="Apellido Paterno" value="{{request()->tx_apellido_paterno}}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="tx_apellido_materno" class="form-control" id="tx_apellido_materno" placeholder="Apellido Materno" value="{{request()->tx_apellido_materno}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                                <div class="col-sm-2">
                                    <a href={{url('paciente/create')}} class="btn btn-success" type="button" title="Agregar Paciente"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="nr_ficha" class="form-control" id="nr_ficha" placeholder="Ficha" value="{{request()->nr_ficha}}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="tx_pasaporte" class="form-control" id="tx_pasaporte" placeholder="Pasaporte" value="{{request()->tx_pasaporte}}">
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="ID" value="{{request()->id}}">
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Rut/Pasaporte</th>
                                    <th>Sexo</th>
                                    <th>Fc. Nacimiento</th>
                                    <th>Teléfono</th>
                                    <th>Previsión</th>
                                    <th>Ficha</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($pacientes as $paciente)
                                        <tr style="font-size:12px">
                                            <td>{{$paciente->id}}</td>
                                            <td><a href={{url("paciente/".$paciente->id)}}>{{$paciente->nombre}}</a></td>
                                            <td>{{$paciente->identificacion}}</td>
                                            <td>{{$paciente->sexo->tx_descripcion}}</td>
                                            <td>{{$paciente->fecha_nacimiento}}</td>
                                            <td>{{$paciente->tx_telefono}}</td>
                                            <td>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}</td>
                                            <td>{{$paciente->nr_ficha}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href={{url("paciente/".$paciente->id."/edit")}} title="Editar Paciente" class="btn btn-warning btn-xs"><i class="fa fa-edit" style="color:white"></i></a>
                                                    <form action="{{ route('paciente.destroy',$paciente->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button onclick="return confirm('¿Esta seguro de eliminar este paciente?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" style="color:white"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $pacientes->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop