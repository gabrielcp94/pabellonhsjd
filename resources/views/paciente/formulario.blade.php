<div class="form-group">
    <div class="row">
        <div class="col-sm-4">
            <label>Tipo de Identificación<span style="color:#FF0000";>*</span></label>
            <div class="row form-group">
                <div class="col-sm-5">
                    <select class="form-control" id="id_tipo_identificacion_paciente" name="id_tipo_identificacion_paciente" onblur="getPerson($('#rut').val(), $(this).val(), $('#nr_ficha').val());" onclick="bloqueoRut($(this).val())" required>
                        <option value="">Seleccione</option>
                        @foreach ($tiposIdentificacion as $tipoIdentificacion)
                            <option value="{{$tipoIdentificacion->id}}" {{isset($paciente) && $paciente->id_tipo_identificacion == $tipoIdentificacion->id ? "selected" : ""}}>{{$tipoIdentificacion->tx_descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="rut" name="rut" onblur="getPerson($(this).val(), $('#id_tipo_identificacion_paciente').val());" value="{{$paciente->identificacion ?? ''}}">
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="tx_nombre">Nombre<span style="color:#FF0000";>*</span></label>
            <input type="text" class="form-control" id="tx_nombre" name="tx_nombre" value="{{$paciente->tx_nombre ?? ''}}" required>
        </div>
        <div class="col-sm-2">
            <label for="tx_apellido_paterno">A. Paterno<span style="color:#FF0000";>*</span></label>
            <input type="text" class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" value="{{$paciente->tx_apellido_paterno ?? ''}}" required>
        </div>
        <div class="col-sm-2">
            <label for="tx_apellido_materno">A. Materno</label>
            <input type="text" class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" value="{{$paciente->tx_apellido_materno ?? ''}}">
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-2">
            <label for="nr_ficha">Ficha</label>
            <input type="number" class="form-control" id="nr_ficha" name="nr_ficha" onblur="getPerson($(this).val(), 5);" value="{{$paciente->nr_ficha ?? ''}}">
        </div>
        <div class="col-sm-2">
            <label for="id_sexo">Sexo<span style="color:#FF0000";>*</span></label>
            <select class="form-control" id="id_sexo" name="id_sexo" required>
                <option value="">Seleccione</option>
                @foreach ($sexos as $sexo)
                    <option value="{{$sexo->id}}" {{isset($paciente) && $paciente->id_sexo == $sexo->id ? "selected" : ""}}>{{$sexo->tx_descripcion}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="fc_nacimiento">Fecha de Nacimiento<span style="color:#FF0000";>*</span></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="date" class="form-control" id="fc_nacimiento" name="fc_nacimiento" value="{{$paciente->fc_nacimiento ?? ''}}" required>
            </div>	
        </div>
        <div class="col-sm-2">
            <label for="id_prevision">Previsión</label>
            <select class="form-control" id="id_prevision" name="id_prevision" onclick="bloqueoFonasa($(this).val())">
                <option value="">Seleccione Previsión</option>
                @foreach ($previsiones as $prevision)
                    <option value={{$prevision->id}} {{isset($paciente) && $paciente->id_prevision == $prevision->id ? "selected" : ""}}>{{$prevision->tx_descripcion}}</option>								
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <label for="id_clasificacion_fonasa">Clasificación</label>
            <select class="form-control" id="id_clasificacion_fonasa" name="id_clasificacion_fonasa">
                <option value="">Seleccione Previsión</option>
                @foreach ($clasificacionesFonasa as $clasificacionFonasa)
                    <option value={{$clasificacionFonasa->id}} {{isset($paciente) && $paciente->id_clasificacion_fonasa == $clasificacionFonasa->id ? "selected" : ""}}>{{$clasificacionFonasa->tx_descripcion}}</option>								
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-2">
            <label for="tx_telefono">Teléfono/Celular</label>
            <input type="text" class="form-control" id="tx_telefono" name="tx_telefono" value="{{$paciente->tx_telefono ?? ''}}">
        </div>
        <div class="col-sm-2">
            <label for="id_comuna">Comuna</label>
            <select class="form-control" id="id_comuna" name="id_comuna">
                <option value="">Seleccione Comuna</option>
                @foreach ($comunas as $comuna)
                    <option value={{$comuna->cd_comuna}} {{isset($paciente) && $paciente->id_comuna == $comuna->id ? "selected" : ""}}>{{$comuna->tx_descripcion}}</option>								
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="tx_direccion">Dirección</label>
            <input type="text" class="form-control" id="tx_direccion" name="tx_direccion" value="{{$paciente->tx_direccion ?? ''}}">
        </div>
        @if(isset($pabellones))
            <div class="col-sm-2 offset-sm-2">
                <label for="bo_neonato">Recién Nacido</label>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="bo_neonato_1">Sí</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="form-check-input" type="radio" id="bo_neonato_1" name="bo_neonato" value="1" {{isset($solicitudPabellon) && $solicitudPabellon->bo_neonato == 1 ? "checked" : ""}} required>
                    </div>
                    <div class="col-sm-3">
                        <label for="bo_neonato_2">No</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="form-check-input" type="radio" id="bo_neonato_2" name="bo_neonato" value="0" {{!isset($solicitudPabellon) || $solicitudPabellon->bo_neonato == 0 ? "checked" : ""}}>
                    </div>
                </div>						
            </div>
        @endif
    </div>
</div>