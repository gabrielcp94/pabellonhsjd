@extends('adminlte::page')

@section('title', 'Editar Paciente')

@section('content_header')
    <h1><i class="ace-icon fa fa-user"></i>{{isset($paciente) ? 'Editar' : 'Crear'}} Paciente</h1>
@stop

@section('content')
    <div class="card">
        <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('PacienteController@store')}}">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{$paciente->id ?? ''}}"/>
            <div class="card-header">
                <h3 style="color:green" class="card-title"><strong>Datos Paciente</strong></h3>
            </div>
            <div class="card-body">
                @include('paciente.formulario')
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");

    function getPerson(rut, id_tipo_identificacion_paciente, ficha){
        if(id_tipo_identificacion_paciente == 5 && ficha){
            rut = ficha;
        }
        $.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut+"&id_tipo_identificacion="+id_tipo_identificacion_paciente,
			function(data){
                if(data.encontrado == true){
                    if(id_tipo_identificacion_paciente != 5){
                        if(data.id_tipo_dentificacion_paciente){
                            $("#id_tipo_identificacion_paciente").val(data.id_tipo_dentificacion_paciente);
                        }
                        $("#rut").val(data.rut);
                    }
                    if(data.nr_ficha){
                        $("#nr_ficha").val(data.nr_ficha);
                    }
                    $("#id_sexo").val(data.id_sexo);
                    $("#tx_nombre").val(data.tx_nombre);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
                    $("#fc_nacimiento").val(data.fc_nacimiento);
                    $("#id_prevision").val(data.id_prevision);
                    $("#id_clasificacion_fonasa").val(data.id_clasificacion_fonasa);
                    if(data.tx_direccion != null && data.tx_direccion != ' '){
                        $("#tx_direccion").val(data.tx_direccion);
                    }
                    if(data.cdgComuna != null && data.cdgComuna != ' '){
                        $("#id_comuna").val(data.cdgComuna);
                    }
                    if(data.tx_telefono != null){
                        $("#tx_telefono").val(data.tx_telefono);
                    }
                    bloqueoFonasa(data.id_clasificacion_fonasa);
                }else{
                    if(id_tipo_identificacion_paciente < 3){
                        alert('Paciente no encontrado');
                    }
                }
            })
	}

    function bloqueoRut(id_tipo_identificacion_paciente){
        if(id_tipo_identificacion_paciente > 2){
            $("#rut").css('display','').attr('disabled', true);
        }else{
            $("#rut").css('display','').attr('disabled', false);
            $("#rut").css('display','').attr('required', true);
        }
        if(id_tipo_identificacion_paciente == 5){
            $("#nr_ficha").css('display','').attr('required', true);
        }else{
            $("#nr_ficha").css('display','').attr('required', false);
        }
    }

    function bloqueoFonasa(id_prevision){
        if(id_prevision != 1){
            $("#id_clasificacion_fonasa").css('display','').attr('disabled', true);
        }else{
            $("#id_clasificacion_fonasa").css('display','').attr('disabled', false);
        }
    }

    $(".select2-general").select2({
        placeholder: "Seleccionar",
        allowClear: true
    })

    @if(isset($paciente))
        var paciente = @json($paciente);
        bloqueoRut(paciente.id_tipo_dentificacion_paciente);
        bloqueoFonasa(paciente.id_clasificacion_fonasa);
    @endif
</script>
@stop