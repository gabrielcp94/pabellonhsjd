<div class="card-body">
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <strong>{{$solicitudPabellon->paciente->tipoIdentificacion->tx_descripcion}}: </strong>{{$solicitudPabellon->paciente->identificacion}}<br>
            <strong>Ficha: </strong>{{$solicitudPabellon->paciente->nr_ficha ?? 'Sin Información'}}<br>
            <strong>Previsión: </strong>{{$solicitudPabellon->paciente->prevision->tx_descripcion}} {{$solicitudPabellon->paciente->clasificacionFonasa->tx_descripcion}}
        </div>
        <div class="col-sm-4 invoice-col">
            <strong>Nombre: </strong>{{$solicitudPabellon->paciente->nombre}}<br>
            <strong>Sexo: </strong>{{$solicitudPabellon->paciente->sexo->tx_descripcion}}<br>
            <strong>Dirección: </strong>{{$solicitudPabellon->paciente->tx_direccion ?? 'Sin Información'}}<br>
        </div>
        <div class="col-sm-4 invoice-col">
            <strong>Fecha de Nacimiento: </strong>{{$solicitudPabellon->paciente->fecha_nacimiento}}<br>
            <strong>Edad: </strong>{{$solicitudPabellon->paciente->edad}}<br>
            <strong>Telefono: </strong>{{$solicitudPabellon->paciente->tx_telefono ?? 'Sin Información'}}
        </div>
    </div>
</div>