@extends('adminlte::page')

@section('title', 'Perfil Paciente')

@section('content')
	<div class="invoice p-3 mb-3">
		@if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
               <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
            </div>
        @endif

        <div class="row">
			<div class="col-10 row">
				<h4><i class="ace-icon fa fa-user"></i> {{$paciente->nombre}}</h4>
            </div>
            <div class="col-2 box-tools pull-right">
                @if (in_array(900, Session::get('opciones')))
                    <a class="btn btn-primary btn-xs" href={{url("paciente/".$paciente->id."/edit")}}><i class="fas fa-edit"></i></a>
                @endif
                <a class="btn btn-xxs btn-success btn-xs pull-right" href="http://10.4.237.27/login?rUser={{Auth::user()->gl_rut}}&tUser={{$token}}&pac={{$paciente->id}}" target="_blank"><i class="fa fa-eye"></i> Perfil Hospitalizado </a>
            </div>
        </div>

        <div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
                <strong>{{$paciente->tipoIdentificacion->tx_descripcion}}: </strong>{{$paciente->identificacion}}<br>
                <strong>Ficha: </strong>{{$paciente->nr_ficha ?? 'Sin Información'}}<br>
				<strong>Fecha de Nacimiento: </strong>{{$paciente->fecha_nacimiento}}<br>
                <strong>Dirección: </strong>{{$paciente->tx_direccion ?? 'Sin Información'}}
			</div>
            
			<div class="col-sm-6 invoice-col">
                <strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
                <strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}<br>
				<strong>Edad: </strong>{{$paciente->edad}}<br>
				<strong>Telefono: </strong>{{$paciente->tx_telefono ?? 'Sin Información'}}
			</div>
		</div>
        <br>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#solicitud" data-toggle="tab">Solicitudes <span class="badge badge-secondary">{{count($solicitudesPabellon)}}</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="#epicrisis" data-toggle="tab">Epicrisis <span class="badge badge-secondary">{{count($arr_epicrises)}}</span></a></li>
                        <li class="nav-item"><a class="nav-link" href="#biopsia" data-toggle="tab">Biopsia <span class="badge badge-secondary">{{count($informes)}}</span></a></li>
                    </ul>
                </div>

                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="solicitud">
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Intervención</th>
                                                <th>Equipo</th>
                                                <th>F. Intervención</th>
                                                <th>Pabellón</th>
                                                <th>Estado</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($solicitudesPabellon->sortByDesc('id') as $solicitudPabellon)
                                                <tr>
                                                    <td>{{$solicitudPabellon->id}}</td>
                                                    <td>{!!$solicitudPabellon->prestaciones->pluck('nombre')->implode('<br>')!!}<br><strong>{{$solicitudPabellon->gl_intevension}}</strong></td>
                                                    <td>{{$solicitudPabellon->equipoMedico->nombre}}</td>
                                                    <td>{{$solicitudPabellon->fecha_intervencion}}</td>
                                                    <td>{{$solicitudPabellon->pabellon->nombre}}</td>
                                                    <td><span class="badge {{$solicitudPabellon->estado->gl_class_laravel}}">{{$solicitudPabellon->lista_espera ?? $solicitudPabellon->estado->gl_nombre}}</span></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a onclick="detalle({{$solicitudPabellon->id}})" type="button" title="Detalle" class="btn btn-primary btn-xs"><i class="fa fa-file" style="color:white"></i></a>
                                                            @if($solicitudPabellon->protocolo)
                                                                <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_protocolo/{{$solicitudPabellon->protocolo->id}}" target="_blank" title="Protocolo"><i class="fas fa-file-pdf"></i></a>
                                                            @else    
                                                                @if (in_array(401, Session::get('opciones')) && $solicitudPabellon->id_estado == 2)
                                                                    <a class="btn btn-warning btn-xs" href={{url("protocolo/create?id_solicitud=".$solicitudPabellon->id)}} title="Ingresar Protocolo"><i class="fas fa-notes-medical" style="color:white"></i></a>
                                                                @endif
                                                            @endif
                                                            @if($solicitudPabellon->consentimiento)
                                                                <a class="btn btn-success btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_consentimiento/{{$solicitudPabellon->id}}" target="_blank" title="Consentimiento Informado"><i class="fas fa-check"></i></a>
                                                            @elseif($solicitudPabellon->id_estado < 3) {{-- Estados Anterior a Confirmada--}}
                                                                <a class="btn btn-warning btn-xs" href={{url("consentimiento/create?id_solicitud=".$solicitudPabellon->id)}} title="Ingresar Consentimiento Informado"><i class="fas fa-check" style="color:white"></i></a>
                                                            @endif
                                                            @if($solicitudPabellon->id_estado == 5) {{--Estado Retirada--}}
                                                                <a class="btn btn-danger btn-xs" href="http://10.6.3.43/pabellon/buscar_ficha/pdf_retirada/{{$solicitudPabellon->id}}" target="_blank" title="Motivo Retiro"><i class="fas fa-eye"></i></a>
                                                            @endif
                                                            @if (in_array(900, Session::get('opciones')) && $solicitudPabellon->asignacion)
                                                                <a class="btn btn-danger btn-xs" onclick="return confirm('¿Esta seguro de volver a Lista de Espera?');" href={{url("devolverListaEspera?id_solicitud=".$solicitudPabellon->id)}} title="Devolver a Lista de Espera"><i class="fas fa-clock" style="color:white"></i></a>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="epicrisis">
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    @if (isset($arr_epicrises) && count($arr_epicrises) > 0)
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Tipo</th>
                                                    <th>Medico</th>
                                                    <th>F. Ingreso</th>
                                                    <th>F. Egreso</th>
                                                    <th><i class="fas fa-cog"></i></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($arr_epicrises as $arr_epicrisis)
                                                    <tr>
                                                        <td>{{$arr_epicrisis['pac_corr']}}</td>
                                                        <td>{{$arr_epicrisis['tipo']}}</td>
                                                        <td>{{$arr_epicrisis['medico']['name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_ingreso']))}}</td>
                                                        <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_egreso']))}}</td>
                                                        <td><a class="btn btn-danger btn-xs" href="http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id={{$arr_epicrisis['id']}}&tipo={{$arr_epicrisis['tipo']}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="biopsia">
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    @if (isset($informes) && count($informes) > 0)
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Tipo Estudio</th>
                                                    <th>Código Estudio</th>
                                                    <th>Servicio Solicitante</th>
                                                    <th>F. Toma</th>
                                                    <th>F. Cierre</th>
                                                    <th><i class="fas fa-cog"></i></th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                @foreach ($informes as $key => $informe)
                                                    @if($informe->codigoEstudio)
                                                        <tr>
                                                            <td>
                                                                @switch($informe->tipoEstudio)
                                                                    @case('B')
                                                                        Biopsia
                                                                        @break
                                                                    @case('C')
                                                                        Consulta
                                                                        @break
                                                                    @case('I')
                                                                        Interconsulta
                                                                        @break
                                                                    @default 
                                                                        {{$informe->tipoEstudio}}
                                                                @endswitch
                                                            <td>{{$informe->codigoEstudio}}</td>
                                                            <td>{{$informe->servicio_solitante}}</td>
                                                            <td>{{date('d-m-Y', strtotime($informe->fecha_toma_muestra))}}</td>
                                                            <td>{{date('d-m-Y', strtotime($informe->fechaCierre))}}</td>
                                                            <td><a class="btn btn-danger btn-xs" onclick="pdfBiopsia('{{isset($paciente) ? $paciente->rut_sin_puntos : ''}}',{{$key}});" href="/getBiopsia?rut={{isset($paciente) ? $paciente->rut_sin_puntos : ''}}&fila={{$key}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal"></div> 
@stop

@section('js')
<script>
    function detalle(id)
	{
		$.get( '{{ url("solicitudPabellonDetalle") }}/' + id, function( data ) {
			$( "#modal" ).html( data );
			$( "#modal-detalle" ).modal();
		});
	};

    $(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop