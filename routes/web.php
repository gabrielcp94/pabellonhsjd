<?php

use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/loginPortal', 'Auth\LoginController@loginPortal')->name('loginPortal');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/inicio', 'HomeController@inicio')->name('inicio');;
Route::get('/portal', 'HomeController@portal');

// Usuario
Route::resource('/user', 'UserController');
Route::get('/resetUsuario/{id}', 'UserController@resetUsuario');
Route::get('/deleteUsuario/{id}', 'UserController@deleteUsuario');

// Paciente
Route::resource('/paciente', 'PacienteController');
Route::get('buscarPaciente', 'PacienteController@buscarPaciente');
Route::get('getBiopsia', 'PacienteController@getBiopsia');

// Solicitud Pabellon
Route::resource('/solicitudPabellon', 'SolicitudPabellonController');
Route::get('/validador', 'SolicitudPabellonController@validador');
Route::get('/solicitudPabellonDetalle/{id}', 'SolicitudPabellonController@detalle');
Route::get('/indexPausas', 'SolicitudPabellonController@indexPausas');
Route::get('/devolverListaEspera', 'SolicitudPabellonController@devolverListaEspera');
Route::get('/solicitudPabellonCorregir', 'SolicitudPabellonController@corregir');
Route::get('/solicitudPabellonSinPaciente', 'SolicitudPabellonController@solicitudPabellonSinPaciente');

// Consentimiento Informado
Route::resource('/consentimiento', 'ConsentimientoController');

// Pausas
Route::resource('/pausaPre', 'PausaPreController');

Route::resource('/pausaPabellon', 'PausaPabellonController');

Route::resource('/pausaAnestesia', 'PausaAnestesiaController');

Route::resource('/pausaSeguridad', 'PausaSeguridadController');

Route::resource('/pausaIntra', 'PausaIntraController');
Route::get('/agregarAccesoVascular', 'PausaIntraController@agregarAccesoVascular');
Route::get('/agregarDrenaje', 'PausaIntraController@agregarDrenaje');

Route::resource('/pausaRecuento', 'PausaRecuentoController');
Route::get('/agregarGasasCompresas', 'PausaRecuentoController@agregarGasasCompresas');
Route::get('/agregarInstrumental', 'PausaRecuentoController@agregarInstrumental');

Route::resource('/pausaRecuperacion', 'PausaRecuperacionController');
Route::get('/revertirAlta', 'PausaRecuperacionController@revertirAlta');

Route::resource('/pausaEpicrisis', 'PausaEpicrisisController');

Route::resource('/pausaTraslado', 'PausaTrasladoController');

// Protocolo
Route::resource('/protocolo', 'ProtocoloController');
Route::get('/eliminarProtocolo', 'ProtocoloController@eliminarProtocolo');

Route::resource('/informe', 'InformeController');
Route::get('/vaciadoIntervenciones', 'InformeController@vaciadoIntervenciones');
Route::get('/vaciadoIntervencionesExcel', 'InformeController@vaciadoIntervencionesExcel');

// Insumos
Route::resource('/solicitudInsumo', 'SolicitudInsumoController');


// Get
Route::get('/getDatosRut', 'GetController@getDatosRut');
Route::get('/getUser', 'GetController@getUser');
Route::get('/getUnidadDemandante/{id}', 'GetController@getUnidadDemandante');
Route::get('/getDiagnostico', 'GetController@getDiagnostico');
Route::get('/getMedico', 'GetController@getMedico');
Route::get('/getPrestacion', 'GetController@getPrestacion');
Route::get('/getEquipo', 'GetController@getEquipo');
Route::get('/getPrestacionTodas', 'GetController@getPrestacionTodas');
Route::get('/getNrPabellon/{id}', 'GetController@getNrPabellon');
Route::get('/getRut', 'GetController@getRut');
Route::get('/agregarFichaGenPaciente', 'GetController@agregarFichaGenPaciente');