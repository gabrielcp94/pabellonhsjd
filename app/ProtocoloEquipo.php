<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProtocoloEquipo extends Model
{
    protected $table = 'cb_protocolo_medico';
    public $guarded = [];
    public $timestamps = false;

    public function labor()
    {
		    return $this->belongsTo('App\Labor', 'cd_labor', 'cod_car');
    }

    public function medico()
    {
		    return $this->belongsTo('App\Medico', 'id_medico', 'pro_cod');
    }
}
