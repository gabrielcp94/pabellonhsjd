<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_pais';
}
