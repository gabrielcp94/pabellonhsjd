<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaRecuentoIn extends Model
{
    protected $table = 'cb_pausa_recuento_in';

    public $guarded = [];
    public $timestamps = false;

    // Relaciones
    public function pabellonera()
    {
		return $this->belongsTo('App\Medico', 'id_pabellonera');
    }
}
