<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrPabellon extends Model
{
    protected $table = 'cb_nr_pabellon';
    public $guarded = [];
}
