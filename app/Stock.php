<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'cb_stock';

    public function insumo()
    {
		    return $this->belongsTo('App\Insumo', 'id_insumo');
    }
}
