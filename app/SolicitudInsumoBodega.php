<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudInsumoBodega extends Model
{
    protected $table = 'cb_solicitud_insumo';
}
