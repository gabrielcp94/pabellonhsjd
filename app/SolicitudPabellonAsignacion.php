<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SolicitudPabellonAsignacion extends Model
{
    protected $table = 'cb_solicitud_pabellon_asignacion';

    public function pabellon()
    {
		    return $this->belongsTo('App\Pabellon', 'id_pabellon');
    }

    public function nrPabellon()
    {
		    return $this->belongsTo('App\NrPabellon', 'id_nr_pabellon');
    }

    public function personal()
    {
		    return $this->hasMany('App\SolicitudPabellonPersonal', 'id_nr_pabellon', 'id_nr_pabellon')->where('fc_solicutud', Carbon::parse($this->fc_inicio)->format('Y-m-d'));
    }

    public function condicional()
    {
		    return $this->belongsTo('App\SolicitudPabellonAsignacionCondicional', 'bo_condicional');
    }
}
