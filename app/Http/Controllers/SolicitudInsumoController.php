<?php

namespace App\Http\Controllers;

use App\SolicitudInsumo;
use Illuminate\Http\Request;

class SolicitudInsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SolicitudInsumo  $solicitudInsumo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $solicitudInsumo = SolicitudInsumo::with('solicitante', 'visaSolicitud', 'visaDevolucion', 'solicitudPabellon', 'solicitudPabellon.paciente', 'estado', 'insumos.stock.insumo')->find($id);
        $solicitudPabellon = $solicitudInsumo->solicitudPabellon;
        $paciente = $solicitudPabellon->paciente;
        $total = 0;
        foreach ($solicitudInsumo->insumos as $key => $insumo) {
            if($solicitudInsumo->id_estado_solicitud != 9){
                $num = $insumo->nr_ocupado + $insumo->nr_merma;
            }else{
                $num = $insumo->nr_solicitado;
            }
            if($insumo->do_costo == null){
                $costo = $insumo->stock->insumo->nr_precio_medio_ponderado;
            }else{
                $costo = $insumo->do_costo;
            }
            $costoTotal = $num*$costo;
            $total = $total + $costoTotal;
        }

        $pdf = \PDF::loadView('solicitudInsumo.show', compact('solicitudInsumo', 'solicitudPabellon', 'paciente', 'total'));
     
        return $pdf->stream('archivo.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SolicitudInsumo  $solicitudInsumo
     * @return \Illuminate\Http\Response
     */
    public function edit(SolicitudInsumo $solicitudInsumo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SolicitudInsumo  $solicitudInsumo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SolicitudInsumo $solicitudInsumo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SolicitudInsumo  $solicitudInsumo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SolicitudInsumo $solicitudInsumo)
    {
        //
    }
}
