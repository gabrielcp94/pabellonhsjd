<?php

namespace App\Http\Controllers;

use App\PausaEpicrisis;
use App\SolicitudPabellon;
use App\Medico;
use App\Documento;
use Illuminate\Http\Request;

class PausaEpicrisisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $anestesiologos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', [3,20]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $cirujanos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [10,11,14,15,16,6]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $documentos = Documento::where('bo_estado', 1)->get();
        return view('pausa.epicrisis.register', compact('solicitudPabellon',  'anestesiologos', 'cirujanos', 'documentos'));
    }

    public function store(Request $request)
    {
        $pausaEpicrisis = PausaEpicrisis::updateOrCreate(['id_solicitud'=> $request->id_solicitud], $request->except('_token'));
        if($pausaEpicrisis){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaEpicrisis->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Epicrisis");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Epicrisis");
        }
    }

    public function show(PausaEpicrisis $pausaEpicrisis)
    {
        //
    }

    public function edit($id)
    {
        $pausaEpicrisis = PausaEpicrisis::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $pausaEpicrisis->id_solicitud)
                ->first();
        $anestesiologos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', [3,20]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $cirujanos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [10,11,14,15,16,6]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $documentos = Documento::where('bo_estado', 1)->get();
        return view('pausa.epicrisis.register', compact('solicitudPabellon',  'anestesiologos', 'cirujanos', 'documentos'));
    }

    public function update(Request $request, PausaEpicrisis $pausaEpicrisis)
    {
        //
    }

    public function destroy($id)
    {
        $pausaEpicrisis = PausaEpicrisis::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaEpicrisis->id_solicitud);
        if($pausaEpicrisis->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
