<?php

namespace App\Http\Controllers;

use App\PausaPabellon;
use App\SolicitudPabellon;
use App\Medico;
use Illuminate\Http\Request;

class PausaPabellonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPabellon')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $pabelloneras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 2);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.pabellon.register', compact('solicitudPabellon', 'pabelloneras', 'enfermeras'));
    }

    public function store(Request $request)
    {
        $pausaPabellon = PausaPabellon::updateOrCreate(['id_solicitud'=> $request->id_solicitud], $request->except('_token'));
        if($pausaPabellon){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaPabellon->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Pabellón");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Pabellón");
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pausaPabellon = PausaPabellon::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPabellon')
                ->whereHas('paciente')
                ->whereHas('pausaPabellon', function ($query) use ($pausaPabellon){
                    $query->where('id', $pausaPabellon->id);
                })
                ->first();
        $pabelloneras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 2);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.pabellon.register', compact('solicitudPabellon', 'pabelloneras', 'enfermeras'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $pausaPabellon = PausaPabellon::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaPabellon->id_solicitud);
        if($pausaPabellon->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
