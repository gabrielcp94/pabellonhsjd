<?php

namespace App\Http\Controllers;

use App\Consentimiento;
use App\SolicitudPabellon;
use Illuminate\Http\Request;

class ConsentimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::find($request->id_solicitud);
        return view('consentimiento.register', compact('solicitudPabellon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::find($request->id_solicitud);
        $consentimiento = Consentimiento::updateOrCreate([
            'id_solicitud' => $request->id_solicitud
        ], $request->except('_token'));
        if($consentimiento){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado el Consentimiento Informado");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado el Consentimiento Informado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consentimiento  $consentimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Consentimiento $consentimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consentimiento  $consentimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Consentimiento $consentimiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consentimiento  $consentimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consentimiento $consentimiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consentimiento  $consentimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consentimiento $consentimiento)
    {
        //
    }
}
