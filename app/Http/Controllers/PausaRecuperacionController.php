<?php

namespace App\Http\Controllers;

use App\PausaRecuperacion;
use App\SolicitudPabellon;
use Illuminate\Http\Request;

class PausaRecuperacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PausaRecuperacion  $pausaRecuperacion
     * @return \Illuminate\Http\Response
     */
    public function show(PausaRecuperacion $pausaRecuperacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PausaRecuperacion  $pausaRecuperacion
     * @return \Illuminate\Http\Response
     */
    public function edit(PausaRecuperacion $pausaRecuperacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PausaRecuperacion  $pausaRecuperacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PausaRecuperacion $pausaRecuperacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PausaRecuperacion  $pausaRecuperacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pausaRecuperacion = PausaRecuperacion::with('controles')->find($id);
        foreach ($pausaRecuperacion->controles as $key => $value) {
            $value->delete();
        }
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaRecuperacion->id_solicitud);
        if($pausaRecuperacion->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }

    public function revertirAlta(Request $request)
    {
        $pausaRecuperacion = PausaRecuperacion::where('id_solicitud', $request->id)->latest()->first();
        $pausaRecuperacion->bo_borrador = 1;
        $pausaRecuperacion->save();
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($request->id);
        return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha revertido el Alta del Paciente");
    }
}
