<?php

namespace App\Http\Controllers;

use App\User;
use App\Perfil;
use App\Bodega;
use App\Especialidad;
use App\EquipoMedico;
use App\Pabellon;
use App\Labor;
use App\TipoProfesional;
use App\Medico;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(User::class, 'user');
    }

    public function index(Request $request)
    {
        $rut = str_replace(".","",request()->rut);
        $users = User::with('Perfiles', 'Bodegas', 'Especialidades', 'EquiposMedicos', 'Pabellones')
                ->when($request->has('rut') && !is_null($request->rut), function ($collection) use ($request, $rut) {
                    return $collection->whereRaw("gl_rut LIKE ?", ['%'.$rut.'%']);
                })
                ->when($request->has('nombre') && !is_null($request->nombre), function ($collection) use ($request) {
                    return $collection->whereRaw("gl_nombre LIKE ?", ['%'.$request->nombre.'%']);
                })
                ->paginate(10);
        return view('user.index', compact('users'));
    }

    public function create()
    {
        $perfiles = Perfil::get();
        $bodegas = Bodega::get();
        $especialidades = Especialidad::get();
        $equiposMedicos = EquipoMedico::get();
        $pabellones = Pabellon::get();
        $labores = Labor::get();
        $tiposProfesionales = TipoProfesional::get();
        return view('user.register', compact('perfiles', 'bodegas', 'especialidades', 'equiposMedicos', 'pabellones', 'labores', 'tiposProfesionales'));
    }

    public function store(Request $request)
    {
        $dataRequest = [
            'gl_nombre' => $request->gl_nombre,
            'gl_rut' => $request->gl_rut,
            'gl_mail' => $request->gl_mail,
        ];
        $user = User::updateOrCreate(['gl_rut'=> $request->gl_rut], $dataRequest);
        $user->Perfiles()->sync($request->perfiles);
        $user->Bodegas()->sync($request->bodegas);
        $user->Especialidades()->sync($request->especialidades);
        $user->EquiposMedicos()->sync($request->equiposMedicos);
        $user->Pabellones()->sync($request->pabellones);

        if($request->equipoClinico == 'on'){
            $rut = explode("-", $request->gl_rut);
            $dataMedico = [
                'pro_rut' => $rut[0],
                'pro_digito' => $rut[1],
                'pro_tipo' => $request->tipoProfesional,
                'pro_apepat' => $request->tx_paterno,
                'pro_apemat' => $request->tx_materno,
                'pro_nombres' => $request->tx_nombre,
            ];
            $medico = Medico::updateOrCreate(['pro_rut'=> $rut[0]], $dataMedico);
            $medico->labores()->sync($request->labores);
        }

        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return redirect('/user')->with('message', 'El usuario '.$user->nombre.' a sido guardado exitosamente');
    }

    public function edit(User $user)
    {
        $user = User::with( 'Perfiles', 'Perfiles.Opciones', 'Bodegas', 'Especialidades', 'EquiposMedicos', 'Pabellones')->find($user->id);
        $rut = explode("-", $user->gl_rut);
        $medico = Medico::with('Labores')->where('pro_rut', $rut[0])->first();
        $perfiles = Perfil::get();
        $bodegas = Bodega::get();
        $especialidades = Especialidad::get();
        $equiposMedicos = EquipoMedico::get();
        $pabellones = Pabellon::get();
        $labores = Labor::get();
        $tiposProfesionales = TipoProfesional::get();
        return view('user.register', compact('user', 'medico', 'perfiles', 'bodegas', 'especialidades', 'equiposMedicos', 'pabellones', 'labores', 'tiposProfesionales'));  
    }

    public function resetUsuario($id) // Reiniciar Contraseña
    {
        // PARA REINICIAR TODAS LAS CONTRASEÑAS BCRYPT
        // $users = User::get();
        // foreach ($users as $key => $user) {
        //     $password = substr($user->gl_rut, 0, 4);
        //     $user->password = $password;
        //     $user->save();
        // }

        $user = User::find($id);
        $password = substr($user->gl_rut, 0, 4);
        $user->gl_clave = $password;
        $user->password = $password;
        $user->save();

        return redirect('/user')->with('message', "La contraseña del usuario ".$user->nombre."  a sido reiniciada");
    }

    public function deleteUsuario($id)
    {
        $user = User::find($id);
        $user->Perfiles()->detach();
        $user->Bodegas()->detach();
        $user->Especialidades()->detach();
        $user->EquiposMedicos()->detach();
        $user->Pabellones()->detach();
        if($user->forceDelete()){
            return redirect('/user')->with('message', "El usuario a sido eliminado correctamente");
        }else{
            return redirect('/user')->with('error', "El usuario no a sido eliminado, intente nuevamente");
        }
    }
}