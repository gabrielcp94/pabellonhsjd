<?php

namespace App\Http\Controllers;

use App\PausaPre;
use App\SolicitudPabellon;
use App\UnidadDemandante;
use Illuminate\Http\Request;

class PausaPreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPre')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $unidades = UnidadDemandante::where('bo_pabellon', 1)->orderBy('gl_nombre')->get();
        return view('pausa.pre.register', compact('solicitudPabellon', 'unidades'));
    }

    public function store(Request $request)
    {
        $pausaPre = PausaPre::updateOrCreate(['id_solicitud'=> $request->id_solicitud], $request->except('_token'));
        if($pausaPre){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaPre->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Pre-Operatoria");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Pre-Operatoria");
        }
    }

    public function show(PausaPre $pausaPre)
    {
        //
    }
    
    public function edit($id)
    {
        $pausaPre = PausaPre::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPre')
                ->find($pausaPre->id_solicitud);
        $unidades = UnidadDemandante::where('bo_pabellon', 1)->orderBy('gl_nombre')->get();
        return view('pausa.pre.register', compact('solicitudPabellon', 'unidades'));
    }
    
    public function update(Request $request, PausaPre $pausaPre)
    {
        //
    }
    
    public function destroy($id)
    {
        $pausaPre = PausaPre::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaPre->id_solicitud);
        if($pausaPre->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
