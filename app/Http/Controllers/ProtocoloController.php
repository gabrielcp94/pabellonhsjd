<?php

namespace App\Http\Controllers;

use App\Protocolo;
use App\ProtocoloEquipo;
use App\SolicitudPabellon;
use App\SolicitudPabellonEquipo;
use App\EquipoMedico;
use App\Pabellon;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\InformationReceived;
use Auth;
use Illuminate\Http\Request;

class ProtocoloController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $protocolos = Protocolo::with('solicitudPabellon')
                ->whereHas('solicitudPabellon')
                ->orderBy('id', 'DESC')
                ->paginate(10);
        return view('protocolo.index', compact('protocolos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'equipo.labor', 'equipo.medico', 'asignacion', 'asignacion.nrPabellon', 'protocolo', 'protocolo.prestaciones', 'protocolo.equipo', 'pausaIntra')
                ->find($request->id_solicitud);
        $equiposMedicos = EquipoMedico::orderBy('gl_descripcion')->get();
        $pabellones = Pabellon::where('bo_activo', '1')->get();
        return view('protocolo.register', compact('solicitudPabellon', 'equiposMedicos', 'pabellones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find(request()->id_solicitud_pabellon);
        $gl_motivo = "";
        if(request()->bo_re_intervencion == 1){
            $gl_motivo = "Aplica Reintervencion";
        }
        if(is_null(request()->gl_diagnostico_pre)){
            request()->gl_diagnostico_pre = '';
        }

        $protocolo = Protocolo::updateOrCreate([
            'id' => request()->id
        ],[
            'id_solicitud_pabellon'=> request()->id_solicitud_pabellon,
            'bo_re_intervencion'=> request()->bo_re_intervencion,
            'bo_lista_espera'=> request()->bo_lista_espera,
            'bo_auge'=> request()->bo_auge,
            'fc_entrada_pabellon'=> date("Y-m-d H:i:s", strtotime(request()->fc_entrada_pabellon)),
            'fc_salida_pabellon'=> date("Y-m-d H:i:s", strtotime(request()->fc_salida_pabellon)),
            'fc_inicio_anestesia'=> date("Y-m-d H:i:s", strtotime(request()->fc_inicio_anestesia)),
            'fc_fin_anestesia'=> date("Y-m-d H:i:s", strtotime(request()->fc_fin_anestesia)),
            'fc_inicio_intervencion'=> date("Y-m-d H:i:s", strtotime(request()->fc_inicio_intervencion)),
            'fc_fin_intervencion'=> date("Y-m-d H:i:s", strtotime(request()->fc_fin_intervencion)),
            'id_diagnostico_pre'=> request()->id_diagnostico_pre,
            'gl_diagnostico_pre'=> request()->gl_diagnostico_pre,
            'id_diagnostico_pos'=> request()->id_diagnostico_pos,
            'gl_diagnostico_pos'=> request()->gl_diagnostico_pos,
            'gl_cirugia_practicada'=> request()->gl_cirugia_practicada,
            'gl_riesgo_operatorio'=> request()->gl_riesgo_operatorio,
            'gl_tipo_anestesia'=> request()->gl_tipo_anestesia,
            'gl_hallazgo'=> request()->gl_hallazgo,
            'gl_descripcion'=> request()->gl_descripcion,
            'gl_incidencia_qui'=> request()->gl_incidencia_qui,
            'gl_incidencia_ane'=> request()->gl_incidencia_ane,
            'bo_drenaje'=> request()->bo_drenaje,
            'bo_biopsia'=> request()->bo_biopsia,
            'bo_cultivo'=> request()->bo_cultivo,
            'gl_tipo_herida'=> request()->gl_tipo_herida,
            'nr_perdida_sanguinea'=> request()->nr_perdida_sanguinea,
            'gl_tromboembolica'=> request()->gl_tromboembolica,
            'bo_tratamiento_antibiotico'=> request()->bo_tratamiento_antibiotico,
            'bo_profilosis'=> request()->bo_profilosis,
            'bo_gasa'=> request()->bo_gasa,
            'bo_material'=> request()->bo_material,
            'bo_compresa'=> request()->bo_compresa,
            'bo_eliminado'=> 0,
            'id_diagnostico_estadistica'=> request()->id_diagnostico_pos,
            'id_tipo_intervencion'=> 0,
            'id_clase_intervencion'=> 0,
            'id_equipo'=> request()->id_equipo,
            'bo_antiguo'=> 0,
            'gl_motivo'=> $gl_motivo,
            'bo_procuramiento'=> request()->bo_procuramiento,
            'bo_cancer'=> request()->bo_cancer
        ]);

        // Prestaciones
        $solicitudPabellon->prestaciones()->sync($request->intervencion);
        $protocolo->prestacionesOriginales()->sync($request->intervencion);
        $protocolo->prestaciones()->sync($request->intervencion);

        //Equipo
        $equipoDelete = SolicitudPabellonEquipo::where('id_solicitud', $solicitudPabellon->id)->delete();
        foreach ($request->equipos as $key => $equipo) {
            $equipo = explode("-", $equipo);
            SolicitudPabellonEquipo::updateOrCreate([
                'id_solicitud' => $solicitudPabellon->id,
                'id_medico' => $equipo[0]
            ],[
                'id_solicitud' => $solicitudPabellon->id,
                'id_medico' => $equipo[0],
                'cd_labor' => $equipo[1]
            ]);
        }
        $equipoDelete = ProtocoloEquipo::where('id_protocolo', $protocolo->id)->delete();
        foreach ($request->equipos as $key => $equipo) {
            $equipo = explode("-", $equipo);
            ProtocoloEquipo::updateOrCreate([
                'id_protocolo' => $protocolo->id,
                'id_medico' => $equipo[0]
            ],[
                'id_protocolo' => $protocolo->id,
                'id_medico' => $equipo[0],
                'cd_labor' => $equipo[1]
            ]);
        }

        if($protocolo){
            $solicitudPabellon->id_estado = 3;
            $solicitudPabellon->save();
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado el Protocolo");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado el Protocolo");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Protocolo  $protocolo
     * @return \Illuminate\Http\Response
     */
    public function show(Protocolo $protocolo)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'equipo.labor', 'equipo.medico', 'asignacion', 'asignacion.nrPabellon', 'protocolo', 'protocolo.prestaciones', 'protocolo.equipo', 'pausaIntra')
                ->whereHas('protocolo', function ($query) use ($protocolo){
                    $query->where('id', $protocolo->id);
                })
                ->first();

        $pdf = \PDF::loadView('protocolo.show', compact('solicitudPabellon'));
     
        return $pdf->stream('archivo.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Protocolo  $protocolo
     * @return \Illuminate\Http\Response
     */
    public function edit(Protocolo $protocolo)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'equipo.labor', 'equipo.medico', 'asignacion', 'asignacion.nrPabellon', 'protocolo', 'protocolo.prestaciones', 'protocolo.equipo', 'pausaIntra')
                ->whereHas('protocolo', function ($query) use ($protocolo){
                    $query->where('id', $protocolo->id);
                })
                ->first();
        foreach ($solicitudPabellon->protocolo->equipo as $key => $equipo) {
            $equipo->id_new = $equipo->medico->pro_cod.'-'.$equipo->labor->cod_car;
        }
        $equiposMedicos = EquipoMedico::orderBy('gl_descripcion')->get();
        $pabellones = Pabellon::where('bo_activo', '1')->get();
        return view('protocolo.register', compact('solicitudPabellon', 'equiposMedicos', 'pabellones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Protocolo  $protocolo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Protocolo $protocolo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Protocolo  $protocolo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->id_user);
        $request->request->add(['usuario_solicitante' => $user->gl_nombre]);
        $protocolo = Protocolo::with('prestacionesOriginales', 'prestaciones', 'equipo', 'addendum')->find($request->id);
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($protocolo->id_solicitud_pabellon);
        $solicitudPabellon->id_estado = 2;
        $solicitudPabellon->save();
        $request->request->add(['protocolo' => $protocolo]);
        $request->request->add(['id_correo' => 1]);
        Mail::to($request->receptor)->send(new InformationReceived($request));
        $protocolo->prestacionesOriginales()->detach();
        $protocolo->prestaciones()->detach();
        $protocolo->equipo()->delete();
        $protocolo->addendum()->delete();
        $protocolo->delete();
        return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado el protocolo");
    }

    public function eliminarProtocolo(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'equipo.labor', 'equipo.medico', 'asignacion', 'asignacion.nrPabellon', 'protocolo', 'pausaIntra')
                // ->whereHas('paciente')
                ->whereHas('protocolo', function ($query) use ($request){
                    $query->where('id', $request->id);
                })
                ->first();
        foreach ($solicitudPabellon->equipo as $key => $equipo) {
            $equipo->id_new = $equipo->medico->pro_cod.'-'.$equipo->labor->cod_car;
        }
        $usuarios = User::whereRaw("gl_mail LIKE ?", '%@%')->get();
        $equiposMedicos = EquipoMedico::orderBy('gl_descripcion')->get();
        $pabellones = Pabellon::where('bo_activo', '1')->get();
        return view('protocolo.eliminar', compact('solicitudPabellon', 'usuarios', 'equiposMedicos', 'pabellones'));
    }
}
