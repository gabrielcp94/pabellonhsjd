<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Api\FonasaApi;
use App\User;
use App\Paciente;
use App\PacienteTrakcare;
use App\Comuna;
use App\UnidadDemandante;
use App\Cie10;
use App\Prestacion;
use App\Medico;
use App\NrPabellon;

class GetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    public function getDatosRut(Request $request, FonasaApi $fonasaApi)
    {
        if(request()->rut == NULL){
            return $data;
        }
        switch (request()->id_tipo_identificacion) {
            case 1:
                $rut = str_replace(".","",request()->rut);
                $rutDV = explode("-", $rut);
                $data = Paciente::where('nr_run', $rutDV[0])->where('tx_digito_verificador', $rutDV[1])->first();
                $dataFonasa = $fonasaApi->fetchNormalized($rutDV[0], $rutDV[1]);

                // En caso de que fonasa esté caida, retorna los valores de nuestra base solamente
                if($dataFonasa['api_fonasa'] == false){
                    if($data){
                        $data['encontrado'] = true;
                    }
                    return $data;
                }

                if(!$data){
                    if($dataFonasa['nr_run']){
                        $data = $dataFonasa;
                        $data['fc_nacimiento'] = date("Y-m-d", strtotime(str_replace("/",".", $dataFonasa['fecha_nacimiento'])));
                    }else{
                        $data['encontrado'] = false;
                        return $data;
                    }
                    
                }else{
                    $data->id_paciente = $data['id'];
                    $data->id_sexo = $dataFonasa['id_sexo'];
                    $data->tx_nombre = $dataFonasa['tx_nombre'];
                    $data->tx_apellido_paterno = $dataFonasa['tx_apellido_paterno'];
                    $data->tx_apellido_materno = $dataFonasa['tx_apellido_materno'];
                    $data->fc_nacimiento = date("Y-m-d", strtotime(str_replace("/",".", $dataFonasa['fecha_nacimiento'])));
                    $data->id_prevision = $dataFonasa['id_prevision'];
                    $data->id_clasificacion_fonasa = $dataFonasa['id_clasificacion_fonasa'];
                }

                // En caso de registrar un usuario, verifica si ya existe
                $user = User::where('gl_rut', $rut)->first();
                if(isset($user)){
                    $data['user'] = true;
                    $data['id'] = $user->id;
                }else{
                    $data['user'] = false;
                }
                break;
            case 2:
                $data = Paciente::where('tx_pasaporte', request()->rut)->first();
                break;
            case 3:
                $data = Paciente::where('nr_ficha', request()->rut)->first();
                break;
            case 5:
                $data = Paciente::where('nr_ficha', request()->rut)->first();
                break;
        }
        if(isset($data)){
            if(isset($data->id_comuna) && $data->id_comuna != null){
                $comuna = Comuna::find($data->id_comuna);
                $data['cdgComuna'] = $comuna->cd_comuna;
            }
            $data['encontrado'] = true;
        }else{
            $data['encontrado'] = false;
        }
        return $data;
    }

    public function getUser(Request $request)
    {
        $users = User::whereRaw("gl_nombre LIKE ?", [$request->search.'%'])->get();
        // dd($users);
        $data['results'] = array();
        foreach($users as $key => $user){
            $producto = array();
            $producto['id'] = $user->id;
            $producto['text'] = "{$user->gl_nombre}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getUnidadDemandante(Request $request,$id)
    {
        $unidades = UnidadDemandante::where('id_tipo_atencion',$id)->where('bo_pabellon', 1)->orderBy('gl_nombre')->get();
        $data['results'] = array();
        foreach($unidades as $key => $unidad){
            $producto = array();
            $producto['id'] = $unidad->id;
            $producto['text'] = "{$unidad->gl_nombre}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getDiagnostico(Request $request)
    {
        $diagnosticos = Cie10::where('bo_uso', 1)->whereRaw("CONCAT(cd_cie10,' ',gl_glosa) LIKE ?", ['%'.$request->search.'%'])->get();
        $data['results'] = array();
        foreach($diagnosticos as $key => $diagnostico){
            $producto = array();
            $producto['id'] = $diagnostico->id;
            $producto['text'] = "{$diagnostico->cd_cie10} {$diagnostico->gl_glosa}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getMedico(Request $request)
    {
        $medicos = Medico::with('labores')
                            ->whereRaw("CONCAT(pro_rut,'-',pro_digito) LIKE ?", [$request->search.'%'])
                            ->orWhereRaw("CONCAT(pro_nombres,' ',pro_apepat,' ',pro_apemat) LIKE ?", ['%'.$request->search.'%'])
                            ->orWhereRaw("CONCAT(SUBSTRING_INDEX(pro_nombres, ' ', 1),' ',pro_apepat,' ',pro_apemat) LIKE ?", ['%'.$request->search.'%'])
                            ->orWhereRaw("CONCAT(pro_apepat,' ',pro_nombres) LIKE ?", ['%'.$request->search.'%'])
                            ->orWhereRaw("CONCAT(pro_apemat,' ',pro_nombres) LIKE ?", ['%'.$request->search.'%'])
                            ->groupBy('pro_rut')
                            ->orderBy('pro_nombres')
                            ->orderBy('pro_apepat')
                            ->get();
        $data['results'] = array();
        foreach($medicos as $key => $medico){
            foreach($medico->labores as $key => $labor){
                if($labor->id == 6){
                    $producto = array();
                    $producto['id'] = $medico->id;
                    $producto['text'] = "{$medico->nombre}, {$medico->rut}"; 
                    if(in_array($producto, $data['results']) == FALSE){
                        array_push($data['results'], $producto);
                    }
                }
            }
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getPrestacion(Request $request)
    {
        $prestaciones = Prestacion::where('bo_estado', 1)->where('bo_pabellon', 1)->whereRaw("CONCAT(prs_cod,' - ',prs_desc) LIKE ?", ['%'.$request->search.'%'])->get();
        $data['results'] = array();
        foreach($prestaciones as $key => $prestacion){
            $producto = array();
            $producto['id'] = $prestacion->id;
            $producto['prs_cod'] = $prestacion->prs_cod;
            $producto['text'] = "{$prestacion->nombre}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getEquipo(Request $request)
    {
        $equipo = array();
        if($request->equipos){
            foreach ($request->equipos as $key => $value) {
                $id = explode("-", $value);
                array_push($equipo, $id[0]);
            }
        }
        $equipos = Medico::with('labores')
                        ->where(function ($query) use ($request){
                                $query->whereRaw("CONCAT(pro_rut,'-',pro_digito) LIKE ?", [$request->search.'%'])
                                        ->orWhereRaw("CONCAT(pro_nombres,' ',pro_apepat,' ',pro_apemat) LIKE ?", ['%'.$request->search.'%'])
                                        ->orWhereRaw("CONCAT(SUBSTRING_INDEX(pro_nombres, ' ', 1),' ',pro_apepat,' ',pro_apemat) LIKE ?", ['%'.$request->search.'%'])
                                        ->orWhereRaw("CONCAT(pro_apepat,' ',pro_nombres) LIKE ?", ['%'.$request->search.'%'])
                                        ->orWhereRaw("CONCAT(pro_apemat,' ',pro_nombres) LIKE ?", ['%'.$request->search.'%']);
                        })
                        ->whereNotIn('pro_cod', $equipo)
                        ->groupBy('pro_rut')
                        ->get();
        $data['results'] = array();
        foreach($equipos as $key => $equipo){
            if(count($equipo->labores) > 0){
                foreach ($equipo->labores as $key => $labor) {
                    $producto = array();
                    $producto['id'] = "{$equipo->id}-{$labor->cod_car}";
                    $producto['text'] = "{$equipo->nombre}, {$labor->cod_des}"; 
                    array_push($data['results'], $producto);
                }
            }
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getPrestacionTodas(Request $request)
    {
        $prestaciones = Prestacion::where('bo_pabellon', 1)->whereRaw("CONCAT(prs_cod,' - ',prs_desc) LIKE ?", ['%'.$request->search.'%'])->groupBy('prs_cod')->get();
        $data['results'] = array();
        foreach($prestaciones as $key => $prestacion){
            $producto = array();
            $producto['id'] = $prestacion->id;
            $producto['text'] = "{$prestacion->nombre}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function getNrPabellon(Request $request,$id)
    {
        $nrPabellones = NrPabellon::where('id_pabellon',$id)->where('bo_activo', '1')->get();
        $data['results'] = array();
        foreach($nrPabellones as $key => $nrPabellon){
            $producto = array();
            $producto['id'] = $nrPabellon->id;
            $producto['text'] = "{$nrPabellon->gl_nombre}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Get  $get
     * @return \Illuminate\Http\Response
     */
    public function show(Get $get)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Get  $get
     * @return \Illuminate\Http\Response
     */
    public function edit(Get $get)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Get  $get
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Get $get)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Get  $get
     * @return \Illuminate\Http\Response
     */
    public function destroy(Get $get)
    {
        //
    }

    public function agregarFichaGenPaciente()
    {
        $pacientes = Paciente::whereNull('nr_ficha')
                                ->whereNotNull('nr_run')
                                ->where('nr_run', '>', '999999')
                                ->where('id', '!=', '185041')
                                ->where('id', '!=', '231057')
                                ->where('id', '!=', '232807')
                                ->where('id', '!=', '233413')
                                ->where('id', '!=', '235362')
                                ->where('id', '>', '237102')
                                ->get();
        foreach ($pacientes as $key => $paciente) {
            $pacienteTrakcare = PacienteTrakcare::where('RUT', $paciente->nr_run)->where('DV', $paciente->tx_digito_verificador)->whereNotNull('ficha')->first();
            if ($pacienteTrakcare) {
                dump($paciente->id);
                dump($paciente->rut_sin_puntos);
                dump($pacienteTrakcare->rut_completo);
                dump(intval($pacienteTrakcare->FICHA));
                $pacienteCambio = Paciente::where('id', $paciente->id)->update(['nr_ficha' => intval($pacienteTrakcare->FICHA)]);
            }
        }
        // $pacientesTrakcare = PacienteTrakcare::take(10)->get();
        // dump($pacientesTrakcare);
        dd($pacientes);
    }
}
