<?php

namespace App\Http\Controllers;

use App\PausaIntra;
use App\PausaIntraAccesoVascular;
use App\PausaIntraDrenaje;
use App\SolicitudPabellon;
use App\Medico;
use Illuminate\Http\Request;

class PausaIntraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $profesionales = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,19,4,20,3,7,9]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.intra.register', compact('solicitudPabellon', 'enfermeras', 'profesionales'));
    }

    public function store(Request $request)
    {
        $intraRequest = [
            'id_solicitud'=> request()->id_solicitud,
            'id_asignacion'=> request()->id_asignacion,
            'bo_borrador'=> request()->bo_borrador,
            'gl_folley'=> request()->gl_folley,
            'gl_folley_balon'=> request()->gl_folley_balon,
            'id_folley_instalada'=> request()->id_folley_instalada,
            'bo_aseo_realizado'=> request()->bo_aseo_realizado,
            'gl_antiseptico'=> request()->gl_antiseptico,
            'id_responsable_aseo'=> request()->id_responsable_aseo,
            'fc_entrada_servicio'=> request()->fc_entrada_servicio,
            'gl_observacion_horario'=> request()->gl_observacion_horario,
            'fc_entrada_pabellon'=> request()->fc_entrada_pabellon,
            'fc_inicio_anestesia'=> request()->fc_inicio_anestesia,
            'fc_inicio_intervencion'=> request()->fc_inicio_intervencion,
            'fc_salida_pabellon'=> request()->fc_salida_pabellon,
            'fc_fin_anestesia'=> request()->fc_fin_anestesia,
            'fc_fin_intervencion'=> request()->fc_fin_intervencion
        ];
        $pausaIntra = PausaIntra::updateOrCreate(['id_solicitud' => $request->id_solicitud], $intraRequest);
        if($pausaIntra){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaIntra->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Intra-Operatorio");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Intra-Operatorio");
        }
    }

    public function show(PausaIntra $pausaIntra)
    {
        //
    }

    public function edit($id)
    {
        $pausaIntra = PausaIntra::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $pausaIntra->id_solicitud)
                ->first();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $profesionales = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,19,4,20,3,7,9]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.intra.register', compact('solicitudPabellon', 'enfermeras', 'profesionales'));
    }

    public function update(Request $request, PausaIntra $pausaIntra)
    {
        //
    }

    public function destroy($id)
    {
        $pausaIntra = PausaIntra::with('accesoVascular', 'drenaje')->find($id);
        foreach ($pausaIntra->accesoVascular as $key => $value) {
            $value->delete();
        }
        foreach ($pausaIntra->drenaje as $key => $value) {
            $value->delete();
        }
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaIntra->id_solicitud);
        if($pausaIntra->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }

    public function agregarAccesoVascular(Request $request)
    {
        if($request->agregar == 1){
            $accesoVascular = PausaIntraAccesoVascular::create($request->except('agregar', 'eliminar'));
        }elseif($request->eliminar != 0){
            PausaIntraAccesoVascular::find($request->eliminar)->delete();
        }
        $accesos = PausaIntraAccesoVascular::with('responsable')->where('id_solicitud', $request->id_solicitud)->get();
        return $accesos;
    }

    public function agregarDrenaje(Request $request)
    {
        if($request->agregar == 1){
            $drenaje = PausaIntraDrenaje::create($request->except('agregar', 'eliminar'));
        }elseif($request->eliminar != 0){
            PausaIntraDrenaje::find($request->eliminar)->delete();
        }
        $drenajes = PausaIntraDrenaje::where('id_solicitud', $request->id_solicitud)->get();
        return $drenajes;
    }
}
