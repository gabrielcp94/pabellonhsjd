<?php

namespace App\Http\Controllers;

use App\SolicitudPabellon;
use App\SolicitudPabellonEquipo;
use App\SolicitudPabellonPersonal;
use App\TipoIdentificacionPaciente;
use App\Sexo;
use App\Prevision;
use App\ClasificacionFonasa;
use App\Comuna;
use App\EquipoMedico;
use App\EspecialidadMedica;
use App\TipoAtencion;
use App\Pabellon;
use App\Paciente;
use App\ComunaPab;
use App\Labor;
use App\Medico;
use App\Api\FonasaApi;
use Illuminate\Http\Request;
use Auth;

class SolicitudPabellonController extends Controller
{
    function __construct(){
        $this->middleware('auth');
        $this->authorizeResource(SolicitudPabellon::class, 'solicitudPabellon');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->nombre)) {
            $id_pacientes_nombre = Paciente::whereRaw("CONCAT(tx_nombre,' ',tx_apellido_paterno,' ',tx_apellido_materno) LIKE ?", ['%'.$request->nombre.'%'])->pluck('id')->toArray();
        }else{
            $id_pacientes_nombre = null;
        }
        if (isset($request->rut)) {
            $rut = str_replace(".","",request()->rut);
            $rut = explode("-", $rut);
            $id_pacientes_rut = Paciente::whereRaw("nr_run LIKE ?", ['%'.$rut[0].'%'])->pluck('id')->toArray();
        }else{
            $id_pacientes_rut = null;
        }
        $solicitudesPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'protocolo', 'estado', 'pausaPre', 'pausaPabellon', 'pausaAnestesia', 'pausaSeguridad', 'pausaIntra', 'pausaRecuento', 'pausaEpicrisis', 'pausaRecuperacion')
                // ->whereHas('paciente')
                ->when($request->has('id') && !is_null($request->id), function ($collection) use ($request){
                    $collection->where('id', $request->id);
                })
                ->when($request->has('rut') && !is_null($request->rut), function ($collection) use ($id_pacientes_rut){
                    $collection->whereHas('paciente', function ($query) use ($id_pacientes_rut){
                        $query->whereIn('id_paciente', $id_pacientes_rut);
                    });
                })
                ->when($request->has('nombre') && !is_null($request->nombre), function ($collection) use ($id_pacientes_nombre){
                    $collection->whereHas('paciente', function ($query) use ($id_pacientes_nombre){
                        $query->whereIn('id_paciente', $id_pacientes_nombre);
                    });
                })
                ->orderBy('id', 'DESC')
                ->paginate(10);
        return view('solicitudPabellon.index', compact('solicitudesPabellon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tiposIdentificacion = TipoIdentificacionPaciente::where("bo_estado", 1)->get();
        $sexos = Sexo::where("bo_estado", 1)->get();
        $previsiones = Prevision::where("bo_estado", 1)->get();
        $clasificacionesFonasa = ClasificacionFonasa::where("bo_estado", 1)->get();
        $comunas = Comuna::orderBy('tx_descripcion')->get();
        $equiposMedicos = EquipoMedico::orderBy('gl_descripcion')->get();
        $especialidades = EspecialidadMedica::where('bo_activo', '1')->orderBy('gl_nombre')->get();
        $tiposAtencion = TipoAtencion::get();
        $pabellones = Pabellon::where('bo_activo', '1')->get();
        return view('solicitudPabellon.register', compact('tiposIdentificacion', 'sexos', 'previsiones', 'clasificacionesFonasa', 'comunas', 'equiposMedicos', 'especialidades', 'tiposAtencion', 'pabellones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // PACIENTE
        $comuna = Comuna::where('cd_comuna', request()->id_comuna)->first();
        $rut = null;
        $pasaporte = null;
        switch(request()->id_tipo_identificacion_paciente){
            case '1':
                $rut = str_replace(".","",request()->rut);
                $rut = explode("-", $rut);
                break;
            case '2':
                $pasaporte = request()->rut;
                break;	               
        }
        $pacienteRequest = [
            'id'=> request()->id_paciente,
            'id_tipo_dentificacion_paciente'=> request()->id_tipo_identificacion_paciente,
            'nr_run'=> $rut[0],
            'tx_digito_verificador'=> $rut[1],
            'tx_pasaporte'=> $pasaporte,
            'nr_ficha'=> request()->nr_ficha,
            'tx_apellido_paterno'=> request()->tx_apellido_paterno,
            'tx_apellido_materno'=> request()->tx_apellido_materno,
            'tx_nombre'=> request()->tx_nombre,
            'id_comuna'=> $comuna['id'],
            'id_sexo'=> request()->id_sexo,
            'fc_nacimiento'=> date("Y-m-d", strtotime(str_replace("/",".", request()->fc_nacimiento))),
            'tx_direccion'=> request()->tx_direccion,
            'tx_telefono'=> request()->tx_telefono,
            'id_prevision'=> request()->id_prevision,
            'id_clasificacion_fonasa'=> request()->id_clasificacion_fonasa
        ];
        // dd($pacienteRequest);
        $paciente = Paciente::updateOrCreate(['id' => $request->id_paciente], $pacienteRequest);

        // SolicitudPabellon
        $paciente = Paciente::find($paciente->id);
        $id_paciente = $paciente->id;
        $identificacion = str_replace(".","",$paciente->identificacion);
        $comunaPab = ComunaPab::find($comuna['id']);
        request()->cd_comuna = $comunaPab->cd_comuna;
        if($request->id_prevision == 1){
            switch ($request->id_clasificacion_fonasa) {
                case 1:
                    request()->id_prevision = 'A';
                    break;
                case 2:
                    request()->id_prevision = 'B';
                    break;
                case 3:
                    request()->id_prevision = 'C';
                    break;
                case 4:
                    request()->id_prevision = 'D';
                    break;
            }
        }
        $solicitudPabellon = SolicitudPabellon::updateOrCreate([
            'id' => request()->id
        ],[
            'id_paciente'=> $id_paciente,
            'gl_rut'=> $identificacion,
            'gl_tipo_identificacion'=> request()->id_tipo_identificacion_paciente,
            'nr_ficha'=> request()->nr_ficha,
            'gl_genero'=> request()->id_sexo,
            'gl_nombre'=> request()->tx_nombre,
            'gl_paterno'=> request()->tx_apellido_paterno,
            'gl_materno'=> request()->tx_apellido_materno,
            'gl_prevision'=> request()->id_prevision,
            'fc_nacimiento'=> date("Y-m-d", strtotime(str_replace("/",".", request()->fc_nacimiento))),
            'gl_direccion'=> request()->tx_direccion,
            'cd_comuna'=> request()->cd_comuna,
            'gl_telefono'=> request()->tx_telefono,
            'bo_re_intervencion'=> request()->bo_re_intervencion,
            'bo_neonato'=> request()->bo_neonato,
            'gl_neonato_genero'=> request()->bo_neonato,
            'bo_lista_espera'=> request()->bo_lista_espera,
            'bo_auge'=> request()->bo_auge,
            'gl_tipo_intervencion'=> request()->gl_tipo_intervencion,
            'bo_cancer'=> request()->bo_cancer,
            'bo_cama_critica'=> request()->bo_cama_critica,
            'bo_latex'=> request()->bo_latex,
            'bo_hipertermia'=> request()->bo_hipertermia,
            'id_tipo_atencion'=> request()->id_tipo_atencion,
            'gl_sala'=> request()->gl_sala,
            'gl_cama'=> request()->gl_cama,
            'fc_intervencion'=> request()->fc_intervencion,
            'tm_estimado'=> request()->tm_estimado,
            'nr_orden'=> request()->nr_orden,
            'id_pabellon'=> request()->id_pabellon,
            'fc_salida_espera'=> '0000-00-00 00:00:00',
            'id_medico'=> request()->id_medico,
            'id_diagnostico_1'=> request()->id_diagnostico_1,
            'gl_diagnostico_2'=> request()->gl_diagnostico_2,
            'gl_intevension'=> request()->gl_intervencion,
            'gl_lado'=> request()->gl_lado,
            'id_unidad'=> request()->id_unidad,
            'id_equipo'=> request()->id_equipo,
            'gl_riesgo_operatorio'=> request()->gl_riesgo_operatorio,
            'gl_tipo_anestesia'=> request()->gl_tipo_anestesia,
            'bo_sangre'=> request()->bo_sangre,
            'bo_radiologia'=> request()->bo_radiologia,
            'gl_observacion'=> request()->gl_observacion,
            'bo_estudio'=> request()->bo_estudio,
            'id_especialidad_aa'=> request()->id_especialidad_aa
        ]);

        // Prestaciones
        $solicitudPabellon->prestaciones()->sync($request->intervencion);

        //Equipo
        $equipoDelete = SolicitudPabellonEquipo::where('id_solicitud', $solicitudPabellon->id)->delete();
        foreach ($request->equipos as $key => $equipo) {
            $equipo = explode("-", $equipo);
            SolicitudPabellonEquipo::updateOrCreate([
                'id_solicitud' => $solicitudPabellon->id,
                'id_medico' => $equipo[0]
            ],[
                'id_solicitud' => $solicitudPabellon->id,
                'id_medico' => $equipo[0],
                'cd_labor' => $equipo[1]
            ]);
        }

        if($solicitudPabellon){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Solicitud de Pabellón");
        }else{
            return redirect()->route('home')->with('error', "No se ha creado la Solicitud de Pabellón");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return \Illuminate\Http\Response
     */
    public function show(SolicitudPabellon $solicitudPabellon)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'pabellon', 'equipoMedico')->find($solicitudPabellon->id);
        $pdf = \PDF::loadView('solicitudPabellon.show', compact('solicitudPabellon'));
     
        return $pdf->stream('archivo.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return \Illuminate\Http\Response
     */
    public function edit(SolicitudPabellon $solicitudPabellon)
    {
        $solicitudPabellon = SolicitudPabellon::with('prestaciones', 'equipo', 'equipo.labor', 'equipo.medico')->find($solicitudPabellon->id);
        $paciente = Paciente::find($solicitudPabellon->id_paciente);
        foreach ($solicitudPabellon->equipo as $key => $equipo) {
            $equipo->id_new = $equipo->medico->pro_cod.'-'.$equipo->labor->cod_car;
        }
        $tiposIdentificacion = TipoIdentificacionPaciente::where("bo_estado", 1)->get();
        $sexos = Sexo::where("bo_estado", 1)->get();
        $previsiones = Prevision::where("bo_estado", 1)->get();
        $clasificacionesFonasa = ClasificacionFonasa::where("bo_estado", 1)->get();
        $comunas = Comuna::orderBy('tx_descripcion')->get();
        $equiposMedicos = EquipoMedico::orderBy('gl_descripcion')->get();
        $especialidades = EspecialidadMedica::where('bo_activo', '1')->orderBy('gl_nombre')->get();
        $tiposAtencion = TipoAtencion::get();
        $pabellones = Pabellon::where('bo_activo', '1')->get();
        return view('solicitudPabellon.register', compact('solicitudPabellon', 'paciente', 'tiposIdentificacion', 'sexos', 'previsiones', 'clasificacionesFonasa', 'comunas', 'equiposMedicos', 'especialidades', 'tiposAtencion', 'pabellones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SolicitudPabellon $solicitudPabellon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return \Illuminate\Http\Response
     */
    public function destroy(SolicitudPabellon $solicitudPabellon)
    {
        //
    }

    
    public function validador(Request $request)
    {
        $error['alerta'] = '';
        $error['ok'] = 0;

        foreach($request->equipos as $key => $equipo){
            $rol = explode("-", $equipo);
            $primer_cirujano = 0;
            if($rol[1] == 6){
                $primer_cirujano = 1;
                $error['ok'] = 1;
            }
            if($primer_cirujano == 0){
                $error['alerta'] = 'Debe seleccionar al menos un primer cirujano';
            }
        }
        return $error;
    }

    public function detalle($id)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'equipo', 'asignacion', 'consentimiento', 'protocolo', 'estado', 'pausaPre', 'pausaPabellon', 'pausaAnestesia', 'pausaSeguridad', 'pausaIntra', 'pausaRecuento', 'pausaEpicrisis', 'pausaRecuperacion', 'pausaRecuperacionTransitoria', 'pausaTraslado', 'solicitudesInsumos', 'solicitudesInsumos.solicitante', 'solicitudesInsumos.estado', 'solicitudesInsumosBodega')
                ->find($id);
        if(isset($solicitudPabellon->asignacion)){
            $personal = SolicitudPabellonPersonal::with('labor', 'medico')->where('id_nr_pabellon', $solicitudPabellon->asignacion->id_nr_pabellon)->where('fc_solicutud', date('Y-m-d', strtotime(str_replace("/",".",$solicitudPabellon->asignacion->fc_inicio))))->get();
        }else{
            $personal = null;
        }
        return view('solicitudPabellon.modal-detalle', compact('solicitudPabellon', 'personal'));
    }

    public function indexPausas(Request $request)
    {
        $this->authorize('indexPausa', User::class);
        $solicitudesPabellon = SolicitudPabellon::with('paciente', 'medico', 'prestaciones', 'pabellon', 'asignacion')
                ->whereHas('asignacion', function ($query){
                    $query->whereBetween('fc_inicio', [date('Y-m-d 00:00:00', strtotime( "-1 day", strtotime( date('Y-m-d H:i:s')))), date('Y-m-d 23:59:59')]);
                })
                ->whereIn('id_pabellon', Auth::user()->pabellones->pluck('id')->toArray())
                ->whereIn('id_estado', [2,3])
                ->orderBy('id', 'DESC')
                ->paginate(10);
        return view('pausa.index', compact('solicitudesPabellon'));
    }

    public function devolverListaEspera(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('asignacion')->find($request->id_solicitud);
        // $solicitudPabellon->bo_lista_espera = 1;
        $solicitudPabellon->id_jefe_equipo = 0;
        $solicitudPabellon->id_estado = 1;
        $solicitudPabellon->save();
        $solicitudPabellon->asignacion()->delete();
        return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado el protocolo");
    }

    public function solicitudPabellonSinPaciente(Request $request)
    {
        if (isset($request->nombre)) {
            $id_pacientes_nombre = SolicitudPabellon::whereRaw("CONCAT(gl_nombre,' ',gl_paterno,' ',gl_materno) LIKE ?", ['%'.$request->nombre.'%'])->pluck('id')->toArray();
        }else{
            $id_pacientes_nombre = null;
        }
        if (isset($request->rut)) {
            $rut = str_replace(".","",request()->rut);
            $rut = explode("-", $rut);
            $id_pacientes_rut = Paciente::whereRaw("nr_run LIKE ?", ['%'.$rut[0].'%'])->pluck('id')->toArray();
        }else{
            $id_pacientes_rut = null;
        }
        $solicitudesPabellon = SolicitudPabellon::whereNull('id_paciente')
                ->orderBy('id', 'DESC')
                ->paginate(10);

        $solicitudesPabellonSinIDPaciente = SolicitudPabellon::whereNull('id_paciente')->count();
        $solicitudesPabellonConIDPaciente = SolicitudPabellon::whereNotNull('id_paciente')->count();
        return view('solicitudPabellon.sinPaciente', compact('solicitudesPabellon', 'solicitudesPabellonSinIDPaciente', 'solicitudesPabellonConIDPaciente'));
    }

    public function corregir(FonasaApi $fonasaApi)
    {
        $solicitudesPabellon = SolicitudPabellon::whereNull('id_paciente')->where('gl_tipo_identificacion', 'Rut')->whereRaw('LENGTH(gl_rut) > 5')->whereRaw('gl_rut NOT LIKE ?', ['0%'])->whereRaw('gl_rut LIKE ?', ['%-%'])->orderBy('id', 'ASC')->take(1000)->get();
        foreach ($solicitudesPabellon as $key => $solicitudPabellon) {
            $rut = explode("-", $solicitudPabellon->gl_rut);
            $paciente = Paciente::where('nr_run', $rut[0])->where('tx_digito_verificador', $rut[1])->first();
            if(isset($paciente)){
                SolicitudPabellon::where('id', $solicitudPabellon->id)->update(['id_paciente' => $paciente->id]);
                dump($paciente->nombre, $solicitudPabellon->nombre);
                dump('________________________________________');
            }else{
                $data = $fonasaApi->fetchNormalized($rut[0], $rut[1]);
                if (isset($data['nr_run'])) {
                    $comuna = Comuna::where('cd_comuna', $data['cdgComuna'])->first();
                    if (isset($comuna)) {
                        $comuna = $comuna->id;
                    } else {
                        $comuna = null;
                    }
                    if (strlen($solicitudPabellon->gl_telefono) > 7) {
                        $telefono = $solicitudPabellon->gl_telefono;
                    } else {
                        $telefono = null;
                    }
                    if ($solicitudPabellon->nr_ficha > 999) {
                        $ficha = $solicitudPabellon->nr_ficha;
                    } else {
                        $ficha = null;
                    }
                    $dataPaciente = array(
                        'nr_run' => $data['nr_run'],
                        'tx_digito_verificador'=> $data['tx_digito_verificador'],
                        'id_tipo_dentificacion_paciente'=> 1,
                        'nr_run'=> $data['nr_run'],
                        'tx_digito_verificador'=> $data['tx_digito_verificador'],
                        'nr_ficha'=> $ficha,
                        'tx_apellido_paterno'=> $data['tx_apellido_paterno'],
                        'tx_apellido_materno'=> $data['tx_apellido_materno'],
                        'tx_nombre'=> $data['tx_nombre'],
                        'id_comuna'=> $comuna,
                        'id_sexo'=> $data['id_sexo'],
                        'fc_nacimiento'=> date("Y-m-d", strtotime(str_replace("/", ".", $data['fecha_nacimiento']))) ,
                        'tx_direccion'=> $data['tx_direccion'],
                        'tx_telefono'=> $telefono,
                        'id_prevision'=> $data['id_prevision'],
                        'id_clasificacion_fonasa'=> $data['id_clasificacion_fonasa']
                    );

                    $paciente = Paciente::create($dataPaciente);
                }else{
                    dump($solicitudPabellon->id, $solicitudPabellon->gl_rut, $solicitudPabellon->nombre);
                }
            }
        }

        // Con Ficha
        $solicitudesPabellon = SolicitudPabellon::whereNull('id_paciente')->where('nr_ficha', '>', '999')->take(500)->get();
        foreach ($solicitudesPabellon as $key => $solicitudPabellon) {
            $paciente = Paciente::where('nr_ficha', $solicitudPabellon->nr_ficha)->first();
            if ($paciente) {
                SolicitudPabellon::where('id', $solicitudPabellon->id)->update(['id_paciente' => $paciente->id]);
                dump($paciente->id, $paciente->nombre, $solicitudPabellon->id, $solicitudPabellon->nombre);
                dump('________________________________________');
            }
        }

        // Con Pasaporte
        $solicitudesPabellon = SolicitudPabellon::whereNull('id_paciente')->where('gl_tipo_identificacion', 'Pasaporte')->take(500)->get();
        foreach ($solicitudesPabellon as $key => $solicitudPabellon) {
            $paciente = Paciente::where('tx_pasaporte', $solicitudPabellon->gl_rut)->first();
            if(isset($paciente)){
                SolicitudPabellon::where('id', $solicitudPabellon->id)->update(['id_paciente' => $paciente->id]);
                dump($paciente->id, $paciente->nombre, $solicitudPabellon->id, $solicitudPabellon->nombre);
                dump('________________________________________');
            }
        }

        $solicitudesPabellon = SolicitudPabellon::whereNull('id_paciente')->get();
        dd($solicitudesPabellon);
    }
}
