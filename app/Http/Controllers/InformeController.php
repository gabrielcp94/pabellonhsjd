<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolicitudPabellon;
use App\Pabellon;
use App\Exports\UserExport;
// use Maatwebsite\Excel\Excel;
use Excel;

class InformeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function vaciadoIntervenciones(Request $request){
        // dd($request->all());
        $solicitudesPabellon = SolicitudPabellon::with('paciente', 'protocolo', 'protocolo.cie10post', 'protocolo.prestaciones', 'asignacion')
                ->whereIn('id', function ($query) use ($request){
                    $query->select('id_solicitud_pabellon')
                            ->from('cb_protocolo')
                            ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                                return $collection->where('fc_entrada_pabellon', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)));
                            })
                            ->when($request->has('fc_termino') && !is_null($request->fc_termino), function ($collection) use ($request) {
                                return $collection->where('fc_entrada_pabellon', '<=', date("Y-m-d 23:59:59", strtotime($request->fc_termino)));
                            })
                            ->when(!$request->has('fc_inicio') || is_null($request->fc_inicio), function ($collection) use ($request){
                                return $collection->where('fc_entrada_pabellon', '>=', date("Y-m-d 00:00:00"));
                            })->when(!$request->has('fc_termino') || is_null($request->fc_termino), function ($collection) use ($request) {
                                return $collection->where('fc_entrada_pabellon', '<=', date("Y-m-d 23:59:59"));
                            })
                            ->where('bo_eliminado', 0)
                            ->where('bo_antiguo', 0)
                            ->whereIn('id', function ($query) use ($request){
                                $query->select('id_protocolo')
                                        ->from('cb_protocolo_presta_estadistica');
                                        // ->where('id_presta', '5653');
                                        // ->when($request->has('intervenciones') && !is_null($request->intervenciones), function ($collection) use ($request){
                                        //     return $collection->whereIn('id_presta', $request->intervenciones);
                                        // });
                            });
                })
                ->when($request->has('pabellones') && !is_null($request->pabellones), function ($collection) use ($request){
                    return $collection->whereIn('id_pabellon', $request->pabellones);
                })
                ->whereIn('id', function ($query){
                    $query->select('id_solicitud')
                            ->from('cb_solicitud_pabellon_asignacion')
                            ->where('id_estado', 2);
                })
                // ->when($request->has('intervenciones') && !is_null($request->intervenciones), function ($collection) use ($request){
                //     return $collection->where('gl_rut', '7935269-1');
                // })
                ->paginate(10);
        // dd($solicitudesPabellon);
        $pabellones = Pabellon::where('bo_activo', 1)->get();
        return view('vaciadoIntervenciones', compact('solicitudesPabellon', 'pabellones'));


        



        $solicitudesPabellon = SolicitudPabellon::with('paciente', 'protocolo', 'protocolo.cie10post', 'protocolo.prestaciones', 'asignacion')
                ->whereHas('protocolo', function($query) use ($request){
                    $query->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                                return $collection->where('fc_entrada_pabellon', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)));
                            })->when($request->has('fc_termino') && !is_null($request->fc_termino), function ($collection) use ($request) {
                                return $collection->where('fc_entrada_pabellon', '<=', date("Y-m-d 23:59:59", strtotime($request->fc_termino)));
                            })
                            ->when(!$request->has('fc_inicio') || is_null($request->fc_inicio), function ($collection) use ($request){
                                return $collection->where('fc_entrada_pabellon', '>=', date("Y-m-d 00:00:00"));
                            })->when(!$request->has('fc_termino') || is_null($request->fc_termino), function ($collection) use ($request) {
                                return $collection->where('fc_entrada_pabellon', '<=', date("Y-m-d 23:59:59"));
                            })
                            ->whereHas('prestaciones', function($query) use ($request){
                                $query->when($request->has('intervenciones') && !is_null($request->intervenciones), function ($collection) use ($request){
                                        return $collection->whereIn('prs_cod', $request->intervenciones);
                                });
                            })
                            // ->when($request->has('intervenciones') && !is_null($request->intervenciones), function ($collection) use ($request){
                            //     return $collection->whereIn('prs_cod', $request->intervenciones);
                            // })
                            ->where('bo_eliminado', 0)
                            ->where('bo_antiguo', 0);
                })
                ->when($request->has('intervenciones') && !is_null($request->intervenciones), function ($collection) use ($request){
                    return $collection->where('id', 58715);
                })
                ->when($request->has('pabellones') && !is_null($request->pabellones), function ($collection) use ($request){
                    return $collection->whereIn('id_pabellon', $request->pabellones);
                })
                ->whereHas('asignacion', function($query){
                    $query->where('id_estado', 2);
                })
                ->paginate(10);
        $pabellones = Pabellon::where('bo_activo', 1)->get();
        // dd($request->all());
        // dd($pabellones);
        // dd($solicitudesPabellon);

        return view('vaciadoIntervenciones', compact('solicitudesPabellon', 'pabellones'));
    }

    public function vaciadoIntervencionesExcel(Request $request) 
    {
        // $today = date('Y-m-d');
        // $prestaciones = Prestacion::with('protocolo_estadistica')
        //                             ->when(!$request->has('fc_inicio') || is_null($request->fc_inicio), function ($collection) {
        //                                 return $collection->whereHas('protocolo_estadistica', function($query) {
        //                                     $query->where('fc_entrada_pabellon', '>', date("Y-m-d 00:00:00"));
        //                                 });
        //                             })
        //                             ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request) {
        //                                 return $collection->whereHas('protocolo_estadistica', function($query) use($request) {
        //                                     $query->where('fc_entrada_pabellon', '>', $request->fc_inicio.' 00:00:00');
        //                                 });
        //                             })
        //                             ->when(!$request->has('fc_termino') || is_null($request->fc_termino), function ($collection) {
        //                                 return $collection->whereHas('protocolo_estadistica', function($query) {
        //                                     $query->where('fc_entrada_pabellon', '>', date("Y-m-d 00:00:00"));
        //                                 });
        //                             })
        //                             ->when($request->has('fc_termino') && !is_null($request->fc_termino), function ($collection) use ($request) {
        //                                 return $collection->whereHas('protocolo_estadistica', function($query) use($request) {
        //                                     $query->where('fc_entrada_pabellon', '<', $request->fc_termino.' 23:59:59');
        //                                 });
        //                             })
        //                             ->get();
        // dd($prestaciones);




        $today = date('Y-m-d');
        if(!isset($request)){
            $request = collect();
        }
        if(!isset($request->fc_inicio)){
            $request->fc_inicio = date('Y-m-d');
        }
        if(!isset($request->fc_termino)){
            $request->fc_termino = date('Y-m-d');
        }
        $prestaciones = Prestacion::with('protocolo_estadistica')
                                    ->whereHas('protocolo_estadistica', function($query) use($request) {
                                        $query->whereBetween('fc_entrada_pabellon', [$request->fc_inicio.' 00:00:00', $request->fc_termino.' 23:59:59']);
                                    })
                                    ->get();

        // dd($prestaciones);
        foreach ($prestaciones as $prestacion) {
            var_dump($prestacion->protocolo_estadistica);
            # code...
        }
        dd();




        // $prestaciones = Prestacion::with('protocolo_estadistica')
        //                             ->whereHas('protocolo_estadistica', function($query){
        //                                 $query->where('fc_entrada_pabellon', '>', date("Y-m-d 00:00:00"));
        //                             })
        //                             ->get();
        // dd($prestaciones);


        dd();









        $protocolos = Protocolo::with('solicitudPabellon.paciente', 'cie10post', 'prestaciones')->latest('id')
                ->when(!$request->has('fc_inicio') || is_null($request->fc_inicio), function ($collection) {
                    return $collection->where('fc_entrada_pabellon', '>', date("Y-m-d 00:00:00"));
                })
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request) {
                    return $collection->where('fc_entrada_pabellon', '>', $request->fc_inicio);
                })
                ->get();
        $data = array();
        foreach($protocolos as $protocolo){
            $primera = 100;
            foreach($protocolo->prestaciones as $prestacion){
                $cirujano1 = "";
                $cirujano2 = "";
                $cirujano3 = "";
                $valor = 0;
                foreach($protocolo->equipo as $medico){
                    if($medico->pivot->cd_labor == 6){
                        $cirujano1 = $medico->nombre;
                    }
                    if($medico->pivot->cd_labor == 10){
                        $cirujano2 = $medico->nombre;
                    }
                    if($medico->pivot->cd_labor == 11){
                        $cirujano3 = $medico->nombre;
                    }
                }
                if($prestacion->nr_valor > 0){
                    $valor = $prestacion->nr_valor;
                }elseif(isset($prestacion->mai) && $prestacion->mai->precio > 0){
                    $valor = $prestacion->mai->precio;
                }elseif(isset($prestacion->mle) && $prestacion->mle->total1 > 0){
                    $valor = $prestacion->mle->total1;
                }elseif(isset($prestacion->particular) && $prestacion->particular->precio > 0){
                    $valor = $prestacion->particular->precio;
                }
                array_push($data, [
                    'GES' => $protocolo->ges_sn,
                    'F_INTER' => $protocolo->fecha_intervencion,
                    'FICHA' => $protocolo->solicitudPabellon->paciente->nr_ficha,
                    'NOMBRE_PACIENTE' => $protocolo->solicitudPabellon->paciente->nombre,
                    'RUT' => $protocolo->solicitudPabellon->paciente->rut_sin_puntos,
                    'ED' => $protocolo->solicitudPabellon->paciente->edad_num,
                    'AD' => 'A',
                    'SEXO' => $protocolo->solicitudPabellon->paciente->sexo_mf,
                    'PREV' => $protocolo->solicitudPabellon->paciente->prevision,
                    'TIPO BENE' => $protocolo->solicitudPabellon->paciente->tipo_beneficio,
                    'BENEFICIARIO' => $protocolo->solicitudPabellon->paciente->beneficiario,
                    'NO BENE' => $protocolo->solicitudPabellon->paciente->no_beneficiario,
                    'UPS' => intval($protocolo->solicitudPabellon->asignacion->pabellon->cb_numerico),
                    'URS' => $protocolo->solicitudPabellon->unidad->gl_nombre,
                    'T_INT' => $prestacion->t_int,
                    'C_INT' => $protocolo->solicitudPabellon->c_int,
                    'D_PRE' => $protocolo->cie10pre->cd_cie10,
                    'D_POS' => $protocolo->cie10post->cd_cie10,
                    'INTERV' => $prestacion->prs_cod,
                    'PORC' => $primera,
                    'VALOR' => $valor,
                    'CANTIDAD' => '1',
                    'VALOR TOTAL' => ($valor*$primera)/100,
                    'CO1' => substr($prestacion->prs_cod, 0, 2),
                    'CO2' => substr($prestacion->prs_cod, 2, 2),
                    'CO3' => substr($prestacion->prs_cod, 4),
                    'CIRU1' => $cirujano1,
                    'CIRU2' => $cirujano2,
                    'CIRU3' => $cirujano3,
                    'GLOSA' => $prestacion->prs_desc,
                    'LISTA ESPERA' => $protocolo->lista_espera,
                    'ENTRADA PABELLON' => date("d/m/Y H:i:s", strtotime($protocolo->fc_entrada_pabellon)),
                    'SALIDA PABELLON' => date("d/m/Y H:i:s", strtotime($protocolo->fc_salida_pabellon)),
                    'EQUIPO MEDICO' => $protocolo->solicitudPabellon->equipoMedico->gl_descripcion
                ]);
                $primera = 50;
            }
        }
        $date = date('d-m-Y');
        return Excel::download(new UserExport($data), 'Vaciado '.$date.'.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
