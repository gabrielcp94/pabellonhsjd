<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Perfil;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('portal');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function portal(Request $request)
    {
        Auth::logout();
        return redirect('/loginPortal?rUser='.$request->rut.'&tUser='.$request->token.'');
    }

    public function index(Request $request)
    {
        return redirect('http://10.6.3.43/pabellon/login/bp/'.Auth::user()->gl_rut.'/'.$request->session()->get('token'));
    }

    public function inicio(Request $request)
    {
        return view('welcome');
    }
}
