<?php

namespace App\Http\Controllers;

use App\PausaTraslado;
use App\SolicitudPabellon;
use Illuminate\Http\Request;

class PausaTrasladoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PausaTraslado  $pausaTraslado
     * @return \Illuminate\Http\Response
     */
    public function show(PausaTraslado $pausaTraslado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PausaTraslado  $pausaTraslado
     * @return \Illuminate\Http\Response
     */
    public function edit(PausaTraslado $pausaTraslado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PausaTraslado  $pausaTraslado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PausaTraslado $pausaTraslado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PausaTraslado  $pausaTraslado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pausaTraslado = PausaTraslado::with('intraSonda')->find($id);
        foreach ($pausaTraslado->intraSonda as $key => $value) {
            $value->delete();
        }
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaTraslado->id_solicitud);
        if($pausaTraslado->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
