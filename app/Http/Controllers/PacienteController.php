<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Paciente;
use App\TipoIdentificacionPaciente;
use App\Sexo;
use App\Prevision;
use App\ClasificacionFonasa;
use App\Comuna;
use App\SolicitudPabellon;

use App\Http\Controllers\Helpers\InformesBiopsia;
use nusoap_client;

class PacienteController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if(isset($request->rut)){
            $rut = str_replace(".","",request()->rut);
            $rut = explode("-", $rut);
        }else{
            $rut = null;
        }
        $pacientes = Paciente::
                when($request->has('tx_nombre') && !is_null($request->tx_nombre), function ($collection) use ($request){
                    $collection->whereRaw('tx_nombre LIKE ?', '%'.$request->tx_nombre.'%');
                })
                ->when($request->has('tx_apellido_paterno') && !is_null($request->tx_apellido_paterno), function ($collection) use ($request){
                    $collection->whereRaw('tx_apellido_paterno LIKE ?', $request->tx_apellido_paterno.'%');
                })
                ->when($request->has('tx_apellido_materno') && !is_null($request->tx_apellido_materno), function ($collection) use ($request){
                    $collection->whereRaw('tx_apellido_materno LIKE ?', $request->tx_apellido_materno.'%');
                })
                ->when($request->has('rut') && !is_null($request->rut), function ($collection) use ($rut){
                    $collection->where('nr_run', $rut[0])->where('tx_digito_verificador', $rut[1]);
                })
                ->when($request->has('nr_ficha') && !is_null($request->nr_ficha), function ($collection) use ($request){
                    $collection->whereRaw('nr_ficha LIKE ?', '%'.$request->nr_ficha.'%');
                })
                ->when($request->has('tx_pasaporte') && !is_null($request->tx_pasaporte), function ($collection) use ($request){
                    $collection->whereRaw('tx_pasaporte LIKE ?', '%'.$request->tx_pasaporte.'%');
                })
                ->when($request->has('id') && !is_null($request->id), function ($collection) use ($request){
                    $collection->where('id', $request->id);
                })
                ->orderBy('id', 'desc')
                ->paginate(10);
        return view('paciente.index', compact('pacientes'));
    }

    public function create()
    {
        $tiposIdentificacion = TipoIdentificacionPaciente::where("bo_estado", 1)->get();
        $sexos = Sexo::where("bo_estado", 1)->get();
        $previsiones = Prevision::where("bo_estado", 1)->get();
        $clasificacionesFonasa = ClasificacionFonasa::where("bo_estado", 1)->get();
        $comunas = Comuna::orderBy('tx_descripcion')->get();
        return view('paciente.register', compact('tiposIdentificacion', 'sexos', 'previsiones', 'clasificacionesFonasa', 'comunas'));
    }

    public function store(Request $request)
    {
        $comuna = Comuna::where('cd_comuna', request()->id_comuna)->first();
        $rut = null;
        $pasaporte = null;
        switch(request()->id_tipo_identificacion_paciente){
            case '1':
                $rut = str_replace(".","",request()->rut);
                $rut = explode("-", $rut);
                break;
            case '2':
                $pasaporte = request()->rut;
                break;	               
        }
        $pacienteRequest = [
            'id_tipo_dentificacion_paciente'=> request()->id_tipo_identificacion_paciente,
            'nr_run'=> $rut[0],
            'tx_digito_verificador'=> $rut[1],
            'tx_pasaporte'=> $pasaporte,
            'nr_ficha'=> request()->nr_ficha,
            'tx_apellido_paterno'=> request()->tx_apellido_paterno,
            'tx_apellido_materno'=> request()->tx_apellido_materno,
            'tx_nombre'=> request()->tx_nombre,
            'id_comuna'=> $comuna['id'],
            'id_sexo'=> request()->id_sexo,
            'fc_nacimiento'=> date("Y-m-d", strtotime(str_replace("/",".", request()->fc_nacimiento))),
            'tx_direccion'=> request()->tx_direccion,
            'tx_telefono'=> request()->tx_telefono,
            'id_prevision'=> request()->id_prevision,
            'id_clasificacion_fonasa'=> request()->id_clasificacion_fonasa
        ];
        $paciente = Paciente::updateOrCreate(['id' => $request->id], $pacienteRequest);

        if($paciente){
            return redirect('/paciente/'.$paciente->id)->with('message', "Se han editado los datos del Paciente");
        }else{
            return redirect()->route('home')->with('error', "No se han editado los datos del Paciente");
        }
    }

    public function show(Request $request, Paciente $paciente, InformesBiopsia $informesBiopsia)
    {
        $token = $request->session()->get('token');
        if ($paciente->nr_run != null && $paciente->tx_digito_verificador) {
            list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
            'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($paciente->nr_run, $paciente->tx_digito_verificador);
            $rut = $paciente->nr_run.'-'.$paciente->tx_digito_verificador;
            $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$rut";
            $data = file_get_contents($url);
            $search  = array('\n\r', '\n', '\r');
            $data = str_replace($search,'',$data);
            $objson = json_decode($data, true);
            $arr_epicrises = $objson['epicrises'];
        }
        $solicitudesPabellon = SolicitudPabellon::with('prestaciones', 'equipoMedico', 'pabellon', 'estado', 'consentimiento', 'asignacion', 'protocolo')->where('id_paciente', $paciente->id)->orderBy('created_at', 'desc')->get();
        if (!isset($arr_epicrises)){
            $arr_epicrises = array();
        }
        if(!isset($informes[0]->tipoEstudio)){
            $informes = array();
        }
        return view('paciente.show', compact('paciente', 'solicitudesPabellon', 'arr_epicrises', 'informes', 'token'));
    }

    public function edit(Paciente $paciente)
    {
        $tiposIdentificacion = TipoIdentificacionPaciente::where("bo_estado", 1)->get();
        $sexos = Sexo::where("bo_estado", 1)->get();
        $previsiones = Prevision::where("bo_estado", 1)->get();
        $clasificacionesFonasa = ClasificacionFonasa::where("bo_estado", 1)->get();
        $comunas = Comuna::orderBy('tx_descripcion')->get();
        return view('paciente.register', compact('paciente', 'tiposIdentificacion', 'sexos', 'previsiones', 'clasificacionesFonasa', 'comunas'));
    }

    public function update(Request $request, Paciente $paciente)
    {
        //
    }

    public function destroy(Paciente $paciente)
    {
        if($paciente->forceDelete()){
            return redirect()->route('inicio')->with('message', "Se ha eliminado el paciente");
        }else{
            return redirect()->route('inicio')->with('error', "No se ha eliminado el Paciente");
        }
    }

    // Recibe el Rut/Pasaporte/Ficha del buscador y lo redirige al perfil
    public function buscarPaciente(Request $request)
    {
        $rut = str_replace(".", "", $request->rut);
        if (stristr($rut, '-')) {
            $rutDV = explode("-", $rut);
            $paciente = Paciente::where('nr_run', $rutDV[0])->where('tx_digito_verificador', $rutDV[1])->first();
        } else {
            $paciente = Paciente::where('tx_pasaporte', $rut)->first();
            if (!$paciente) {
                $paciente = Paciente::where('nr_ficha', $rut)->first();
            }
        }
        if(isset($paciente)){
            return redirect('/paciente/'.$paciente->id);
        }else{
            echo "<script>
                        alert('El paciente con rut/pasaporte/ficha $rut no se encuentra registrado.');
                        window.location= 'inicio'
                </script>";
        }
    }

    // Funcion para traer las biopsias del paciente
    public function getBiopsia(Request $request)
    {
        $array_rut = explode("-",$request->rut);
        $rut_sd = $array_rut[0];
        $rut_dv = $array_rut[1];
        $objSOAP;
        $url = "http://10.6.1.236/pathient/ws/wsnPath.php?wsdl";
        $type = "";
        $proxyhost = "";
        $proxyport = "";
        $proxyusername = "";
        $proxypassword = "";
        $clientError;
        $objSOAP = new nusoap_client($url, $type, $proxyhost, $proxyport, $proxyusername, $proxypassword);
        $clientError = $objSOAP->getError();
        $param = array(
            'rut' => $rut_sd,
            'dv' => $rut_dv
        );
        try{
            $resultado = $objSOAP->call('consultaInforme', $param, '', '', false, true);		
            if ($objSOAP->fault) {
                    $resulto = $resultado;
            } else {
                    $error = $objSOAP->getError();
                    if ($error) {
                        $resulto = $error;
                    } else {
                        $resulto = $resultado;
                    }
            }
        }catch(Exception $e){
            $resulto = ( $e->getMessage());
        }
            $xml = simplexml_load_string(utf8_encode($resultado));
        foreach($xml->informe as $informe){
                $arr_pdf[] = $informe->informepdf;	
            }
        for($i = 0; $i < count($arr_pdf); $i++){
            if($i == $request->fila ){
                $pdf_codificado = $arr_pdf[$i];
            }
        }
        $decoded = base64_decode($pdf_codificado);
        header('Content-Type: application/pdf');
        echo $decoded;
    }
}
