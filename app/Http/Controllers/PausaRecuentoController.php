<?php

namespace App\Http\Controllers;

use App\PausaRecuento;
use App\PausaRecuentoGc;
use App\PausaRecuentoIn;
use App\SolicitudPabellon;
use App\Medico;
use Illuminate\Http\Request;

class PausaRecuentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $anestesiologos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', [3,20]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $arsenaleras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 1);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $pabelloneras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 2);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $cirujanos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [10,11,14,15,16,6]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.recuento.register', compact('solicitudPabellon',  'anestesiologos', 'arsenaleras', 'pabelloneras', 'enfermeras', 'cirujanos'));
    }

    public function store(Request $request)
    {
        $recuentoRequest = [
            'id_solicitud'=> request()->id_solicitud,
            'id_asignacion'=> request()->id_asignacion,
            'bo_borrador'=> request()->bo_borrador,
            'id_anestesiologo'=> request()->id_anestesiologo,
            'id_arsenalera'=> request()->id_arsenalera,
            'id_pabellonera'=> request()->id_pabellonera,
            'id_enfermera'=> request()->id_enfermera,
            'id_cirujano'=> request()->id_cirujano,
            'nr_recuento_sutura'=> request()->nr_recuento_sutura,
            'nr_recuento_aguja'=> request()->nr_recuento_aguja,
            'nr_recuento_aguja_total'=> request()->nr_recuento_aguja_total,
            'id_pabellonera_aguja'=> request()->id_pabellonera_aguja,
            'gl_observacion'=> request()->gl_observacion
        ];
        $pausaRecuento = PausaRecuento::updateOrCreate(['id_solicitud' => $request->id_solicitud], $recuentoRequest);
        if($pausaRecuento){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaRecuento->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado el Recuento");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado el Recuento");
        }
    }

    public function show(PausaRecuento $pausaRecuento)
    {
        //
    }

    public function edit($id)
    {
        $pausaRecuento = PausaRecuento::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaIntra')
                ->whereHas('paciente')
                ->where('id', $pausaRecuento->id_solicitud)
                ->first();
        $anestesiologos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', [3,20]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $arsenaleras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 1);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $pabelloneras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->where('cod_car', 2);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $cirujanos = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [10,11,14,15,16,6]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.recuento.register', compact('solicitudPabellon',  'anestesiologos', 'arsenaleras', 'pabelloneras', 'enfermeras', 'cirujanos'));
    }

    public function update(Request $request, PausaRecuento $pausaRecuento)
    {
        //
    }

    public function destroy($id)
    {
        $pausaRecuento = PausaRecuento::with('gc', 'in')->find($id);
        foreach ($pausaRecuento->gc as $key => $value) {
            $value->delete();
        }
        foreach ($pausaRecuento->in as $key => $value) {
            $value->delete();
        }
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaRecuento->id_solicitud);
        if($pausaRecuento->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }

    public function agregarGasasCompresas(Request $request)
    {
        if($request->agregar == 1){
            $gasasCompresas = PausaRecuentoGc::create($request->except('agregar', 'eliminar'));
        }elseif($request->eliminar != 0){
            PausaRecuentoGc::find($request->eliminar)->delete();
        }
        $gasasCompresas = PausaRecuentoGc::with('pabellonera')->where('id_solicitud', $request->id_solicitud)->get();
        return $gasasCompresas;
    }

    public function agregarInstrumental(Request $request)
    {
        if($request->agregar == 1){
            $instrumental = PausaRecuentoIn::create($request->except('agregar', 'eliminar'));
        }elseif($request->eliminar != 0){
            PausaRecuentoIn::find($request->eliminar)->delete();
        }
        $instrumental = PausaRecuentoIn::with('pabellonera')->where('id_solicitud', $request->id_solicitud)->get();
        return $instrumental;
    }
}
