<?php

namespace App\Http\Controllers;

use App\PausaAnestesia;
use App\SolicitudPabellon;
use App\Medico;
use Illuminate\Http\Request;

class PausaAnestesiaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPabellon')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $tecnicosAnestesistas = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [4, 30, 31]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.anestesia.register', compact('solicitudPabellon', 'tecnicosAnestesistas', 'enfermeras'));
    }

    public function store(Request $request)
    {
        if(!$request->gl_bic){
            $request->request->add(['gl_bic' => array()]);
        }
        $pausaAnestesia = PausaAnestesia::updateOrCreate(['id_solicitud'=> $request->id_solicitud], $request->except('_token'));
        if($pausaAnestesia){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaAnestesia->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Anestesia");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Anestesia");
        }
    }

    public function show(PausaAnestesia $pausaAnestesia)
    {
        //
    }

    public function edit($id)
    {
        $pausaAnestesia = PausaAnestesia::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaPabellon')
                ->whereHas('paciente')
                ->where('id', $pausaAnestesia->id_solicitud)
                ->first();
        $tecnicosAnestesistas = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [4, 30, 31]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        $enfermeras = Medico::with('labores')
                ->whereHas('labores',
                    function ($query){
                        $query->whereIn('cod_car', [17,5,21,7]);
                    }
                )
                ->orderBy('pro_nombres')
                ->get();
        return view('pausa.anestesia.register', compact('solicitudPabellon', 'tecnicosAnestesistas', 'enfermeras'));
    }

    public function update(Request $request, PausaAnestesia $pausaAnestesia)
    {
        //
    }

    public function destroy($id)
    {
        $pausaAnestesia = PausaAnestesia::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaAnestesia->id_solicitud);
        if($pausaAnestesia->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
