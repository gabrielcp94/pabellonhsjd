<?php

namespace App\Http\Controllers;

use App\PausaSeguridad;
use App\SolicitudPabellon;
use App\PrevencionCaida;
use App\PrevencionUPP;
use Illuminate\Http\Request;

class PausaSeguridadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaSeguridad')
                ->whereHas('paciente')
                ->where('id', $request->id)
                ->first();
        $prevencionesCaida = PrevencionCaida::get();
        $prevencionesUPP = PrevencionUPP::get();
        return view('pausa.seguridad.register', compact('solicitudPabellon', 'prevencionesCaida', 'prevencionesUPP'));
    }

    public function store(Request $request)
    {
        $pausaSeguridad = PausaSeguridad::updateOrCreate(['id_solicitud'=> $request->id_solicitud], $request->except('_token'));
        if($pausaSeguridad){
            $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaSeguridad->id_solicitud);
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha creado la Pausa Seguridad");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha creado la Pausa Seguridad");
        }
    }

    public function show(PausaSeguridad $pausaSeguridad)
    {
        //
    }

    public function edit($id)
    {
        $pausaSeguridad = PausaSeguridad::find($id);
        $solicitudPabellon = SolicitudPabellon::with('paciente', 'asignacion', 'pausaSeguridad')
                ->whereHas('paciente')
                ->where('id', $pausaSeguridad->id_solicitud)
                ->first();
        $prevencionesCaida = PrevencionCaida::get();
        $prevencionesUPP = PrevencionUPP::get();
        return view('pausa.seguridad.register', compact('solicitudPabellon', 'prevencionesCaida', 'prevencionesUPP'));
    }

    public function update(Request $request, PausaSeguridad $pausaSeguridad)
    {
        //
    }

    public function destroy(PausaSeguridad $pausaSeguridad)
    {
        $solicitudPabellon = SolicitudPabellon::with('paciente')->find($pausaSeguridad->id_solicitud);
        if($pausaSeguridad->delete()){
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('message', "Se ha eliminado la pausa con exito");
        }else{
            return redirect('/paciente/'.$solicitudPabellon->id_paciente)->with('error', "No se ha podido eliminar la pausa");
        }
    }
}
