<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaPabellon extends Model implements Auditable
{
    protected $table = 'cb_pausa_lista_pabellon';

    public $guarded = [];

    // Audit
    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
    protected $auditThreshold = 50;

    // Boot
    public static function boot()
    {
        parent::boot();

        static::creating(function($pausaPabellon)
        {
            $pausaPabellon->id_responsable = Auth::id();
        });
          
        static::created(function($pausaPabellon)
        {
            $pausaPabellon->id_responsable = Auth::id();
        });
    }

    // Set
    public function setGlEnergiaAttribute($value) 
	{
		$this->attributes['gl_energia'] = implode(",", $value);
    }
}
