<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaPre extends Model implements Auditable
{
    protected $table = 'cb_pausa_pre';

    public $guarded = [];

    // Audit
    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
    protected $auditThreshold = 50;

    // Boot
    public static function boot()
    {
        parent::boot();

        static::creating(function($pausaPre)
        {
            $pausaPre->id_responsable = Auth::id();
        });
          
        static::created(function($pausaPre)
        {
            $pausaPre->id_responsable = Auth::id();
        });
    }

    // Set
    public function setFcIngresoServicioAttribute($value) 
	{
		$this->attributes['fc_ingreso_servicio'] = date("Y-m-d H:i:s", strtotime(str_replace("/",".", $value)));
    }

    public function setGlAislamientoAttribute($value) 
	{
		$this->attributes['gl_aislamiento'] = implode(",", $value);
    }

    public function setGlProtesisDentalAttribute($value) 
	{
        if($value == NULL){
            $this->attributes['gl_protesis_dental'] = 'NO';
        }else{
            $this->attributes['gl_protesis_dental'] = $value;
        }
    }

    public function setGlPielSanaAttribute($value) 
	{
        if($value == NULL){
            $this->attributes['gl_piel_sana'] = 'SI';
        }else{
            $this->attributes['gl_piel_sana'] = $value;
        }
    }

    // Relaciones
    public function cie10()
    {
		return $this->belongsTo('App\Cie10', 'id_diagnostico_pre');
    }
}
