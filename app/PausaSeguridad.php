<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaSeguridad extends Model implements Auditable
{
    protected $table = 'cb_pausa_seguridad';

    public $guarded = [];

    // Audit
    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
    protected $auditThreshold = 50;

    // Boot
    public static function boot()
    {
        parent::boot();

        static::creating(function($pausaSeguridad)
        {
            $pausaSeguridad->id_responsable = Auth::id();
        });
          
        static::created(function($pausaSeguridad)
        {
            $pausaSeguridad->id_responsable = Auth::id();
        });
    }

    // Set
    public function setGlPrevencionUppAttribute($value) 
	{
        if($value == NULL){
            $this->attributes['gl_prevencion_upp'] = '0';
        }else{
            $this->attributes['gl_prevencion_upp'] = implode(",", $value);
        }
    }

    public function setTmProfilaxisAntibioticaAttribute($value) 
	{
        if($value == NULL){
            $this->attributes['tm_profilaxis_antibiotica'] = '00:00';
        }else{
            $this->attributes['tm_profilaxis_antibiotica'] = $value;
        }
    }
}
