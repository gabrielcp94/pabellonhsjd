<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrevencionCaida extends Model
{
    protected $table = 'cb_prevencion_caida';
}
