<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaRecuentoGc extends Model
{
    protected $table = 'cb_pausa_recuento_gc';

    public $guarded = [];
    public $timestamps = false;

    // Relaciones
    public function pabellonera()
    {
		return $this->belongsTo('App\Medico', 'id_pabellonera');
    }
}
