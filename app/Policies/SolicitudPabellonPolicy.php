<?php

namespace App\Policies;

use App\SolicitudPabellon;
use App\User;
use Session;

use Illuminate\Auth\Access\HandlesAuthorization;

class SolicitudPabellonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any solicitud pabellons.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array(900, Session::get('opciones')) ? true : false;
    }

    /**
     * Determine whether the user can view the solicitud pabellon.
     *
     * @param  \App\User  $user
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return mixed
     */
    public function view(User $user, SolicitudPabellon $solicitudPabellon)
    {
        return true;
    }

    /**
     * Determine whether the user can create solicitud pabellons.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array(301, Session::get('opciones')) ? true : false;
    }

    /**
     * Determine whether the user can update the solicitud pabellon.
     *
     * @param  \App\User  $user
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return mixed
     */
    public function update(User $user, SolicitudPabellon $solicitudPabellon)
    {
        return in_array(900, Session::get('opciones')) ? true : false;
    }

    /**
     * Determine whether the user can delete the solicitud pabellon.
     *
     * @param  \App\User  $user
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return mixed
     */
    public function delete(User $user, SolicitudPabellon $solicitudPabellon)
    {
        //
    }

    /**
     * Determine whether the user can restore the solicitud pabellon.
     *
     * @param  \App\User  $user
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return mixed
     */
    public function restore(User $user, SolicitudPabellon $solicitudPabellon)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the solicitud pabellon.
     *
     * @param  \App\User  $user
     * @param  \App\SolicitudPabellon  $solicitudPabellon
     * @return mixed
     */
    public function forceDelete(User $user, SolicitudPabellon $solicitudPabellon)
    {
        //
    }

    public function indexPausa()
    {
        return in_array(351, Session::get('opciones')) ? true : false;
    }
}
