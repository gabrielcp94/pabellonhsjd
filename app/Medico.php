<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medico extends Model
{
    protected $table = 'cb_medico_ncr';
    protected $appends = ['id', 'nombre', 'rut'];
    protected $guarded = [];

    protected $primaryKey = 'pro_cod';

    public function getIdAttribute()
    {
        return "{$this->pro_cod}";
    }

    public function getNombreAttribute()
    {
        return ucwords(mb_strtolower("{$this->pro_nombres} {$this->pro_apepat} {$this->pro_apemat}"));
    }

    public function getRutAttribute()
    {
        return "{$this->pro_rut}-{$this->pro_digito}";
    }

    public function labores()
    {
        return $this->belongsToMany('App\Labor', 'cb_medico_cod_ncr', 'pro_cod', 'tla_cod', 'pro_cod', 'cod_car');
    }

    public function tipoProfesional()
    {
		return $this->belongsTo('App\TipoProfesional', 'pro_tipo', 'gl_codigo')->withDefault(["tx_descripcion" => "Sin Información"]);
    }
}