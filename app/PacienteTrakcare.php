<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteTrakcare extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'trakcare.his_pacien2';

    public $guarded = [];

    protected $appends = ['rut_completo'];

    public function getRutCompletoAttribute()
    {
        return "{$this->RUT}-{$this->DV}";
    }
}
