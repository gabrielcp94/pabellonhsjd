<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudInsumoProducto extends Model
{
    protected $table = 'cb_salida';

    protected $appends = ['costo_total', 'costo_total_post'];

    public function stock()
    {
		    return $this->belongsTo('App\Stock', 'id_stock');
    }
}
