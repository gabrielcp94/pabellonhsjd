<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaEpicrisis extends Model implements Auditable
{
    protected $table = 'cb_pausa_epicrisis_enfermeria';

    public $guarded = [];

    // Audit
    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
    protected $auditThreshold = 50;

    // Boot
    public static function boot()
    {
        parent::boot();

        static::creating(function($pausaAnestesia)
        {
            $pausaAnestesia->id_responsable = Auth::id();
        });
          
        static::created(function($pausaAnestesia)
        {
            $pausaAnestesia->id_responsable = Auth::id();
        });
    }

    // Set
    public function setIdDocumentoAttribute($value) 
	{
		$this->attributes['id_documento'] = implode(",", $value);
    }
}
