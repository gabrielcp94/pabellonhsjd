<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudInsumoEstado extends Model
{
    protected $table = 'cb_estado_solicitud';
}
