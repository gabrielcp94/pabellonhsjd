<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable;

    protected $table = 'cb_usuario';
    protected $guarded = [];

    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];

    protected $auditThreshold = 100;

    public static function boot()
    {
        parent::boot();

        static::creating(function($user)
        {
            $password = substr($user->gl_rut, 0, 4);
            $user->gl_clave = $password;
            $user->password = $password;
        });
          
        static::created(function($user)
        {
            $password = substr($user->gl_rut, 0, 4);
            $user->gl_clave = $password;
            $user->password = $password;
        });
    }

    public function setGlClaveAttribute($value){
        $this->attributes['gl_clave'] = sha1(intval($value));
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt(intval($value));
    }

    protected $appends = ['nombre', 'opciones'];

    //Trae el nombre solo con la primera letra en mayuscula
    public function getNombreAttribute()
    {
        return ucwords(mb_strtolower("{$this->gl_nombre}"));
    }

    //Trae todas las opciones a las que tiene acceso el usuario
    public function getOpcionesAttribute()
    {
        return array_unique(array_merge($this->Perfiles->pluck('Opciones')->collapse()->pluck('id')->toArray(), $this->Perfiles->pluck('Opciones')->collapse()->pluck('id_padre')->toArray()));
    }

    public function TokenPortal()
    {
		return $this->hasOne('App\UserPortal', 'rut', 'gl_rut');
    }

    public function Perfiles()
    {
        return $this->belongsToMany('App\Perfil', 'cb_labor', 'id_usuario', 'id_perfil');
    }

    public function Bodegas()
    {
        return $this->belongsToMany('App\Bodega', 'cb_usuario_bodega', 'id_usuario', 'id_bodega');
    }

    public function Especialidades()
    {
        return $this->belongsToMany('App\Especialidad', 'cb_usuario_especialidad', 'id_usuario', 'id_especialidad');
    }

    public function EquiposMedicos()
    {
        return $this->belongsToMany('App\EquipoMedico', 'cb_usuario_equipo', 'id_usuario', 'id_equipo_medico');
    }

    public function Pabellones()
    {
        return $this->belongsToMany('App\Pabellon', 'cb_usuario_pabellon', 'id_usuario', 'id_pabellon');
    }
}