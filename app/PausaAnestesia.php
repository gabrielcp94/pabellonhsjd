<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaAnestesia extends Model implements Auditable
{
    protected $table = 'cb_pausa_lista_anestesia';
    
    public $guarded = [];

    // Audit
    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
    protected $auditThreshold = 50;

    // Boot
    public static function boot()
    {
        parent::boot();

        static::creating(function($pausaAnestesia)
        {
            $pausaAnestesia->id_responsable = Auth::id();
        });
          
        static::created(function($pausaAnestesia)
        {
            $pausaAnestesia->id_responsable = Auth::id();
        });
    }

    // Set
    public function setGlBicAttribute($value) 
	{
		$this->attributes['gl_bic'] = implode(",", $value);
    }
}
