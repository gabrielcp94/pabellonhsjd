<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaRecuperacionControl extends Model
{
    protected $table = 'cb_pausa_recuperacion_control';

    public $guarded = [];
}
