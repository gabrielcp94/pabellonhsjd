<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipoMedico extends Model
{
    protected $table = 'cb_equipo_medico';
    public $guarded = [];

    protected $appends = ['nombre'];

    public function getNombreAttribute()
    {
        return ucwords(mb_strtolower("{$this->gl_descripcion}"));
    }
}
