<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIdentificacionPaciente extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gen_tipo_identificacion_paciente';
}
