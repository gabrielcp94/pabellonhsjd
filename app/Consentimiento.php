<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Consentimiento extends Model
{
    protected $table = 'cb_solicitud_pabellon_consentimiento';

    public $guarded = [];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function($consentimiento)
        {
            $consentimiento->id_medico = Auth::id();
        });
          
        static::created(function($consentimiento)
        {
            $consentimiento->id_medico = Auth::id();
        });
    }

    public function medico()
    {
		    return $this->belongsTo('App\User', 'id_medico');
    }
}
