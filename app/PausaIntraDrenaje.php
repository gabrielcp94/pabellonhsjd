<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaIntraDrenaje extends Model
{
    protected $table = 'cb_pausa_intra_drenaje';

    public $guarded = [];
    public $timestamps = false;
}
