<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'cb_perfil';
    public $guarded = [];

    public function Opciones()
    {
        return $this->belongsToMany('App\Opcion', 'cb_permiso', 'id_perfil', 'id_opcion');
    }
}
