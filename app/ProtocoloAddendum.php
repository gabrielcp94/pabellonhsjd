<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProtocoloAddendum extends Model implements Auditable
{
    protected $table = 'cb_protocolo_addendum';
    public $guarded = [];

    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
        'deleted',
        'deleting',
    ];
}
