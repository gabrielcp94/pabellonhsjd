<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProfesional extends Model
{
    protected $table = 'cb_tipo_profesional';
}
