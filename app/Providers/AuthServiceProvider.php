<?php

namespace App\Providers;

use App\User;
use App\Policies\UserPolicy;
use App\SolicitudPabellon;
use App\Policies\SolicitudPabellonPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        SolicitudPabellon::class => SolicitudPabellonPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::resource('user', 'App\Policies\UserPolicy');

        Gate::resource('solicitudPabellon', 'App\Policies\SolicitudPabellonPolicy');
        Gate::define('indexPausa', 'App\Policies\SolicitudPabellonPolicy@indexPausa');

        // Modulos
        Gate::define('insumos', 'App\Policies\UserPolicy@insumos');
        Gate::define('solicitudInsumo', 'App\Policies\UserPolicy@solicitudInsumo');
        Gate::define('misSolicitudes', 'App\Policies\UserPolicy@misSolicitudes');
        Gate::define('solicitudRecibida', 'App\Policies\UserPolicy@solicitudRecibida');
        Gate::define('confirmacionInsumosUtilizados', 'App\Policies\UserPolicy@confirmacionInsumosUtilizados');
        Gate::define('stockCritico', 'App\Policies\UserPolicy@stockCritico');
        Gate::define('listaPedidos', 'App\Policies\UserPolicy@listaPedidos');
        Gate::define('stock', 'App\Policies\UserPolicy@stock');
        Gate::define('visarInsumos', 'App\Policies\UserPolicy@visarInsumos');
        Gate::define('visarDevolucion', 'App\Policies\UserPolicy@visarDevolucion');
        Gate::define('usoInsumoSinSolicitud', 'App\Policies\UserPolicy@usoInsumoSinSolicitud');
        Gate::define('salidas', 'App\Policies\UserPolicy@salidas');
        Gate::define('solicitudExterna', 'App\Policies\UserPolicy@solicitudExterna');
        Gate::define('cargaHermes', 'App\Policies\UserPolicy@cargaHermes');
        Gate::define('vaciadoStockExcel', 'App\Policies\UserPolicy@vaciadoStockExcel');
        Gate::define('solicitudTranslado', 'App\Policies\UserPolicy@solicitudTranslado');
        Gate::define('solicitudPostOperacion', 'App\Policies\UserPolicy@solicitudPostOperacion');

        Gate::define('solicitudDePabellon', 'App\Policies\UserPolicy@solicitudDePabellon');
        Gate::define('ingresoSolicitud', 'App\Policies\UserPolicy@ingresoSolicitud');
        Gate::define('misSolicitudesQuirurgicas', 'App\Policies\UserPolicy@misSolicitudesQuirurgicas');
        Gate::define('asignacion', 'App\Policies\UserPolicy@asignacion');
        Gate::define('agendaPabellones', 'App\Policies\UserPolicy@agendaPabellones');
        Gate::define('ingresoSolicitudAntigua', 'App\Policies\UserPolicy@ingresoSolicitudAntigua');
        Gate::define('estadoSolicitud', 'App\Policies\UserPolicy@estadoSolicitud');
        Gate::define('suspenderIQ', 'App\Policies\UserPolicy@suspenderIQ');

        Gate::define('listaEspera', 'App\Policies\UserPolicy@listaEspera');
        Gate::define('gestionListaEspera', 'App\Policies\UserPolicy@gestionListaEspera');
        Gate::define('rnle', 'App\Policies\UserPolicy@rnle');
        Gate::define('bajaListaEspera', 'App\Policies\UserPolicy@bajaListaEspera');
        Gate::define('desvinculacion', 'App\Policies\UserPolicy@desvinculacion');
        Gate::define('intervencionesEditadas', 'App\Policies\UserPolicy@intervencionesEditadas');

        Gate::define('enfermeria', 'App\Policies\UserPolicy@enfermeria');
        Gate::define('procesoEnfermeria', 'App\Policies\UserPolicy@procesoEnfermeria');
        Gate::define('visarPausa', 'App\Policies\UserPolicy@visarPausa');
        Gate::define('historialEnfermeria', 'App\Policies\UserPolicy@historialEnfermeria');
        Gate::define('recuperacion', 'App\Policies\UserPolicy@recuperacion');
        Gate::define('hojaDeTraslado', 'App\Policies\UserPolicy@hojaDeTraslado');
        Gate::define('trasladosRealizados', 'App\Policies\UserPolicy@trasladosRealizados');

        Gate::define('preOperatorioSinSolicitud', 'App\Policies\UserPolicy@preOperatorioSinSolicitud');
        Gate::define('ingresarPreOperatorio', 'App\Policies\UserPolicy@ingresarPreOperatorio');
        Gate::define('gestionPreOperatorio', 'App\Policies\UserPolicy@gestionPreOperatorio');

        Gate::define('protocolo', 'App\Policies\UserPolicy@protocolo');
        Gate::define('ingresarProtocolo', 'App\Policies\UserPolicy@ingresarProtocolo');
        Gate::define('listaProtocolo', 'App\Policies\UserPolicy@listaProtocolo');
        Gate::define('historial', 'App\Policies\UserPolicy@historial');
        Gate::define('historialVer', 'App\Policies\UserPolicy@historialVer');
        Gate::define('misProtocolos', 'App\Policies\UserPolicy@misProtocolos');
        Gate::define('estadisticas', 'App\Policies\UserPolicy@estadisticas');
        Gate::define('misParticipaciones', 'App\Policies\UserPolicy@misParticipaciones');
        Gate::define('protocoloProcuramiento', 'App\Policies\UserPolicy@protocoloProcuramiento');
        Gate::define('listaDeAnalisisReoperaciones', 'App\Policies\UserPolicy@listaDeAnalisisReoperaciones');

        Gate::define('pabellon', 'App\Policies\UserPolicy@pabellon');
        Gate::define('quintoPiso', 'App\Policies\UserPolicy@quintoPiso');
        Gate::define('estadisticasIngreso', 'App\Policies\UserPolicy@estadisticasIngreso');

        Gate::define('informes', 'App\Policies\UserPolicy@informes');
        Gate::define('vaciadoIntervenciones', 'App\Policies\UserPolicy@vaciadoIntervenciones');
        Gate::define('revisarDiagnostico', 'App\Policies\UserPolicy@revisarDiagnostico');
        Gate::define('produccionPabellon', 'App\Policies\UserPolicy@produccionPabellon');
        Gate::define('ugsqMinsal', 'App\Policies\UserPolicy@ugsqMinsal');
        Gate::define('informePrestaciones', 'App\Policies\UserPolicy@informePrestaciones');
        Gate::define('informeCardioCirugia', 'App\Policies\UserPolicy@informeCardioCirugia');
        Gate::define('solicitudIntervencionSinProtocolo', 'App\Policies\UserPolicy@solicitudIntervencionSinProtocolo');
        Gate::define('estadoListaEsperaHistorica', 'App\Policies\UserPolicy@estadoListaEsperaHistorica');
        Gate::define('estadoInsumos', 'App\Policies\UserPolicy@estadoInsumos');
        Gate::define('usoDePabellon', 'App\Policies\UserPolicy@usoDePabellon');
        Gate::define('tiempoOperatorio', 'App\Policies\UserPolicy@tiempoOperatorio');
        Gate::define('informePrestacionesSIGGES', 'App\Policies\UserPolicy@informePrestacionesSIGGES');
        Gate::define('vaciadoCompleto', 'App\Policies\UserPolicy@vaciadoCompleto');
        Gate::define('medidaMinisterial', 'App\Policies\UserPolicy@medidaMinisterial');
        Gate::define('cmdbUGCQsegundaEtapa', 'App\Policies\UserPolicy@cmdbUGCQsegundaEtapa');
        Gate::define('estudioDeEficiencia', 'App\Policies\UserPolicy@estudioDeEficiencia');
        Gate::define('indicadoresCalidad', 'App\Policies\UserPolicy@indicadoresCalidad');
        Gate::define('produccionEquipoQuirurgico', 'App\Policies\UserPolicy@produccionEquipoQuirurgico');
        Gate::define('produccionCirujanos', 'App\Policies\UserPolicy@produccionCirujanos');
        Gate::define('pausaSeguridadProtocolo', 'App\Policies\UserPolicy@pausaSeguridadProtocolo');
        Gate::define('gastosInsumos', 'App\Policies\UserPolicy@gastosInsumos');
        Gate::define('cantidadPacientesIntervenidos', 'App\Policies\UserPolicy@cantidadPacientesIntervenidos');
        Gate::define('informeOncologia', 'App\Policies\UserPolicy@informeOncologia');
        Gate::define('pacientesIngresadosConAnticipacion', 'App\Policies\UserPolicy@pacientesIngresadosConAnticipacion');
        
        Gate::define('mantenedor', 'App\Policies\UserPolicy@mantenedor');
        Gate::define('equipoClinico', 'App\Policies\UserPolicy@equipoClinico');
        Gate::define('equipoMedico', 'App\Policies\UserPolicy@equipoMedico');
        Gate::define('packInsumos', 'App\Policies\UserPolicy@packInsumos');
        Gate::define('jerarquiaEquipoMedico', 'App\Policies\UserPolicy@jerarquiaEquipoMedico');
        Gate::define('causaDesvincular', 'App\Policies\UserPolicy@causaDesvincular');
        Gate::define('pabellonMantenedor', 'App\Policies\UserPolicy@pabellonMantenedor');
        Gate::define('salaRecuperacion', 'App\Policies\UserPolicy@salaRecuperacion');
        Gate::define('camaRecuperacion', 'App\Policies\UserPolicy@camaRecuperacion');
        Gate::define('estadoCamaSala', 'App\Policies\UserPolicy@estadoCamaSala');

        Gate::define('administracion', 'App\Policies\UserPolicy@administracion');


        //
    }
}
