<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadDemandante extends Model
{
    protected $table = 'cb_unidad';
}
