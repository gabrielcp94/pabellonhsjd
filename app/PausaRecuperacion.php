<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PausaRecuperacion extends Model implements Auditable
{
    protected $table = 'cb_pausa_recuperacion';

    public $guarded = [];

    use \OwenIt\Auditing\Auditable;
    protected $auditEvents = [
        'updated',
        'updating',
    ];

    protected $auditThreshold = 50;

    public function controles()
    {
		    return $this->hasMany('App\PausaRecuperacionControl', 'id_solicitud', 'id_solicitud');
    }
}
