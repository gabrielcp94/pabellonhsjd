<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InformationReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Mensaje de Pabellon.';
    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $distressCall;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->request->id_correo == 1){
            return $this->view('protocolo.correoEliminar');
        }
    }
}
