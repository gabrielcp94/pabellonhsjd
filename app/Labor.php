<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Labor extends Model
{
    protected $table = 'cb_cod_cirugia_ncr';
    protected $appends = ['id', 'gl_nombre'];

    public function getIdAttribute()
    {
        return "{$this->cod_car}";
    }

    public function getGlNombreAttribute()
    {
        return "{$this->cod_des}";
    }
}
