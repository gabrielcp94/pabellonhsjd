<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaTrasladoIntraSonda extends Model
{
    protected $table = 'cb_traslado_intra_sonda';

    public $guarded = [];
}
