<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SolicitudPabellonAsignacionCondicional extends Model
{
    protected $table = 'cb_solicitud_pabellon_asignacion_condicional';
}
