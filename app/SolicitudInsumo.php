<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudInsumo extends Model
{
    protected $table = 'cb_solicitud';

    protected $appends = ['fecha_solicitud'];

    public function getFechaSolicitudAttribute()
    {
        $fecha_solicitud = date('d/m/Y H:i:s', strtotime(str_replace("/", ".", $this->fc_solicitud)));
        return "{$fecha_solicitud}";
    }

    public function solicitante()
    {
		    return $this->belongsTo('App\User', 'id_solicitante');
    }

    public function visaSolicitud()
    {
		    return $this->belongsTo('App\User', 'id_visa_solicitud');
    }
    
    public function visaDevolucion()
    {
		    return $this->belongsTo('App\User', 'id_visa_devolucion');
    }

    public function solicitudPabellon()
    {
		    return $this->belongsTo('App\SolicitudPabellon', 'id_solicitud_pabellon');
    }

    public function estado()
    {
		    return $this->belongsTo('App\SolicitudInsumoEstado', 'id_estado_solicitud')->withDefault();
    }

    public function insumos()
    {
		    return $this->hasMany('App\SolicitudInsumoProducto', 'id_solicitud');
    }
}
