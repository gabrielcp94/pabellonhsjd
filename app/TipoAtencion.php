<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtencion extends Model
{
    protected $table = 'cb_tipo_atencion';
}
