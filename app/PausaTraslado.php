<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaTraslado extends Model
{
    protected $table = 'cb_traslado_solicitud';

    public $guarded = [];

    public function intraSonda()
    {
		    return $this->hasMany('App\PausaTrasladoIntraSonda', 'id_solicitud', 'id_solicitud');
    }
}
